# README #

### What is this repository for? ###

* SLOTH is an Uncertainty Quantification library written in Python 2.7

### How do I get set up? ###

* Required Python libraries: scipy, scikit-learn, scikit-fmm, matplotlib
* To install, simply clone git clone https://username@bitbucket.org/quagla/sloth.git

### Contribution guidelines ###

* The Test folder contains scripts for testing all classes
* If you write a new class Foo, please add a corresponding test_foo.py script 

### Who do I talk to? ###

* Repo owner Dr. Alessio Quaglino (alessio.quaglino at usi.ch)
* MOOSE interfaces Seif Ben Bader (seif.ben.bader at usi.ch)

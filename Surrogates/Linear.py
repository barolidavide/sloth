
# @file	Nonparametric.cxx
# @brief	This file implements a Bayesian linear regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 July 2016 

import numpy
from scipy.stats import invgamma


#####################################################################################################
# @brief Define class
#####################################################################################################
class Linear:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): pass


    #####################################################################################################
    # @brief Performs linear regression
    #####################################################################################################
    def fit(self,x,y):
					
        x = numpy.atleast_2d(x)
        y = numpy.atleast_2d(y)
        
        self.p = 2
        self.n = x.shape[0]
        
        # Assemble matrices
        X = numpy.matrix([numpy.ones(self.n),x[:,0]]).T
        A = (X.T)*X

        # Compute covariance of posterior distribution
        self.C = numpy.linalg.inv(A)

        # Compute least squares and residual
        self.b = numpy.linalg.solve(A,X.T*y)
        self.r = y - (self.b[0] + self.b[1]*x[:,0]).T
        self.r2 = self.r.T * self.r


    #####################################################################################################
    # @brief Eval linear regression
    #####################################################################################################
    def eval(self,xi,stdDev=False):
        
        # Eval regression
        yi = self.b[0,0] + self.b[1,0]*xi
        yi = numpy.atleast_2d(yi.T).T
        
        # Eval credible interval
        if stdDev:
            sigma = self.evalVariance(xi,yi)
        else:
            sigma = yi*0
        
        print 'Sigma linear = %f' % numpy.amax(sigma)
        
        return (yi,sigma)


    #####################################################################################################
    # @brief Evaluate standard deviation
    #####################################################################################################
    def evalVariance(self,xi,mu):
    
        M = 1000
        dy = numpy.zeros([xi.size,M])
        
        xi = numpy.atleast_2d(xi).T
        mu = numpy.atleast_2d(mu).T
    
        # Define inverse gamma
        s2 = self.r2 / (self.n-self.p)
        a = ( self.n - self.p ) / 2.0
        b = a * s2[0,0]

        for i in range(0,M):

            # Draw sigma
            sigma = invgamma.rvs(a, loc=0, scale=b, size=1)
            betaM = [self.b[0,0],self.b[1,0]]
        
            # Draw beta
            beta = numpy.random.multivariate_normal(betaM,sigma[0]*self.C)
    
            # Compute model output
            yi = beta[0] + beta[1]*xi
            
            # Compute error
            dy[:,i] = yi[:,0] - mu[:,0]
        
        var = numpy.square(dy);
        std = numpy.sqrt(numpy.mean(var,axis=1))
        
        return std


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


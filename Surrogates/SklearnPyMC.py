
# @file	nonparPyMC.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy as np
import pymc
from scipy import special
from numpy import matlib
from sklearn import gaussian_process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel,RBF,ConstantKernel as C


#####################################################################################################
# @brief Define class
#####################################################################################################
class NPR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self):
    
        self.kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2)) \
                    + WhiteKernel(noise_level=1e-5, noise_level_bounds=(1e-10, 1e+1))
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi,return_std=False):
        
        xdata = np.atleast_2d(xdata).T
        ydata = np.atleast_2d(ydata).T
        
        self.gp = gaussian_process.GaussianProcessRegressor(kernel=self.kernel,
                                                            alpha=1.e-2, n_restarts_optimizer=10)
        
        # Prior
        self.gp.fit(xdata,ydata)
        theta0 = self.gp.kernel_.theta
        theta = pymc.MvNormal('theta',theta0,np.diag([1e-1,1e-1,1e-1]))
        
        @pymc.stochastic(observed=False)
        def sigma(value=1):
            return -np.log(abs(value))

        # Model
        @pymc.deterministic
        def mu(xi=xdata,theta=theta,gp=self.gp):
            self.gp.kernel_.theta = theta
            return gp.predict(xi,return_std=False)
        
        # Likelihood
        y = pymc.Normal('y', mu=mu, tau=1. / sigma ** 2, observed=True, value=ydata)

        # Package model
        model1 = pymc.Model([theta,y])
			
        # Run the model
        S = pymc.MCMC(model1)
        S.sample(iter=10000, burn=8000)

        # Get output
        self.theta = S.trace('theta')[:]
        
        # Return the output
        return self.eval(xi,return_std)


    #####################################################################################################
    # @brief Evaluate only
    #####################################################################################################
    def eval(self,xi,return_std=True):
        xi = np.atleast_2d(xi).T
    
        if return_std:
            M = self.theta.shape[0]
            N =         xi.shape[0]
        
            # M predictions for N points (N x M matrix)
            fx = np.zeros((N,M))
            
            for i in range(0,M):
                self.gp.kernel_.theta = self.theta[i,:]
                fx[:,i] = self.gp.predict(xi,return_std=False)[:,0]
        
            # Mean per each point xi
            avg = np.mean(fx,axis=1)
            
            var = np.square(fx - np.matlib.repmat(avg,M,1).T);
            std = np.sqrt(np.mean(var,axis=1))
        else:
            
            self.gp.kernel_.theta = np.mean(self.theta,axis=0)
            avg = self.gp.predict(xi,return_std=False)[:,0]
            std = 0.0

        return (avg,std)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


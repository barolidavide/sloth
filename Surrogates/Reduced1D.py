# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 15:20:38 2016

@author: alessio
"""

import numpy
from numpy import matlib

#####################################################################################################
# @brief Define class
#####################################################################################################
class Reduced1D:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,model,n=1):
        self.n = n
        self.model = model
        self.path = []
        self.tang = []
        self.eigv = []
        self.prec = []


    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,x,y):
        print "Fitting 1D model.."
        if (len(y) != self.n):
            print "Wrong number of components!"
        for i in range(len(y)):
            self.path.append( self.model.initPath(y[i]) )
            self.tang.append( self.model.computeTangent(self.path[i]) )
            self.eigv.append( self.model.computeEigenvectors(self.path[i]) )
            self.prec.append( self.model.precomputeTensor(self.path[i]) )
        print "1D model fitted"
    

    #####################################################################################################
    # @brief Evaluate only
    #####################################################################################################
    def eval(self,xi):
        yi = numpy.zeros(self.n)
        for i in range(self.n):
            yi[i] = self.model.findSolution(xi,self.path[i],
                                               self.tang[i],
                                               self.eigv[i],
                                               self.prec[i])
        return numpy.min(yi)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


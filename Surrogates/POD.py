# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 15:20:38 2016

@author: alessio
"""

from GPR import *
from sklearn.decomposition import PCA
import numpy

#####################################################################################################
# @brief Define class
#####################################################################################################
class POD:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,functional,n=15):
        
        self.functional = functional
        self.gpr = GPR()
        self.n = n


    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,x,y):

        if y.ndim < 2 : print "Solution y has dimension 1, stopping POD computation"
        
        self.pcay = PCA(n_components=self.n, svd_solver='arpack').fit(y)
        #self.pcax = PCA(n_components=self.n, svd_solver='arpack').fit(x)
        
        #self.gpr.fit(self.pcax.transform(x),self.pcay.transform(y))
        self.gpr.fit(x,self.pcay.transform(y))
    

    #####################################################################################################
    # @brief Evaluate only
    #####################################################################################################
    def eval(self,xi):

        #(sol,sigma) = self.gpr.eval(self.pcax.transform(xi))
        (sol,sigma) = self.gpr.eval(xi)

        eigv = self.pcay.components_[:,self.functional.idx]
        mean = self.pcay.mean_[self.functional.idx]

        yi = numpy.dot(sol,eigv) + mean
        return (yi,sigma)


    #####################################################################################################
    # @brief Evaluate standard deviation
    #####################################################################################################
    def evalVariance(self,xi): return self.gpr.evalVariance(xi)
    

    #####################################################################################################
    # @brief Sample from posterior
    #####################################################################################################
    def samplePosterior(self,xi_input): return self.gpr.samplePosterior(xi_input)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


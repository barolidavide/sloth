
# @file	nonparPyMC.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy as np
from scipy import special
import pymc
import pysmc

#####################################################################################################
# @brief Define class
#####################################################################################################
class LR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): 
        self.ESS = 0.0
        self.w = 0.0
        self.l = 0
        self.s = 0
        self.g = 0
        self.a0 = 1.0
        self.b0 = 1.0	
        self.a = 2.0
        self.b = 1e-6
        self.a_tau = 1.0
        self.a_mu = 0.01
        self.k_max = 100
        self.s = 1.0
        self.gammaS = 1.0
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi):
        
        # Define the variables needed for the routine, with their prior distributions
        alpha = pymc.Uniform('alpha', -100, 100)

        @pymc.stochastic(observed=False)
        def beta(value=0):
            return -1.5 * np.log(1 + value ** 2)

        @pymc.stochastic(observed=False)
        def sigma(value=1):
            return -np.log(abs(value))

        # Define the form of the model and likelihood
        @pymc.deterministic
        def y_model(x=xdata, alpha=alpha, beta=beta):
            return alpha + beta * x

        #@pymc.stochastic(observed=True)
        #def y(value=ydata, mu=y_model, tau=1. / sigma ** 2):
            #return pymc.normal_like(value, mu, tau)

        y = pymc.Normal('y', mu=y_model, tau=1. / sigma ** 2, observed=True, value=ydata)

        # package the full model in a dictionary
        model1 = dict(alpha=alpha, beta=beta, sigma=sigma, y_model=y_model, y=y)

        # Construct the basic MCMC sampler
        mcmc_sampler = pymc.MCMC(model1)
        
        # Construct the SMC sampler
        #smc_sampler = pysmc.SMC(mcmc_sampler, num_particles=1000, num_mcmc=10, verbose=1)
        
        # Initialize SMC at gamma = 0.01
        #smc_sampler.initialize(0.01)
    
        # Move the particles to gamma = 1.0
        #smc_sampler.move_to(1.0)

        # Get a particle approximation
        #p = smc_sampler.get_particle_approximation()
    
        # take samples
        mcmc_sampler.sample(iter=10000, burn=5000)

        # extract the traces and plot the results
        pymc_trace = [mcmc_sampler.trace('alpha')[:],mcmc_sampler.trace('beta')[:],mcmc_sampler.trace('sigma')[:]]
		
        # Return the output
        return (0,self.evalRegression(xi,pymc_trace))


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi,trace):
        
        alpha, beta = trace[:2]
        yi = alpha[:, None] + beta[:, None] * xi

        return yi.mean(0)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


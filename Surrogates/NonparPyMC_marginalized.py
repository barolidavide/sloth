
# @file	Koutsourelakis.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 July 2016 
 
import numpy
from scipy import special

#####################################################################################################
# @brief Define class
#####################################################################################################
class NPR:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): 
        self.ESS = 0.0
        self.w = 0.0
        self.l = 0
        self.s = 0
        self.g = 0
        self.a0 = 1.0
        self.b0 = 1.0	
        self.a = 2.0
        self.b = 1e-6
        self.a_tau = 1.0
        self.a_mu = 0.01
        self.k_max = 100
        self.s = 1.0
        self.gammaS = 1.0
		
		
    #####################################################################################################
    # @brief Sample from the prior
    #####################################################################################################
    def samplePrior(self):
        a0 = self.a0
        b0 = self.b0
        a_tau = self.a_tau
        a_mu = self.a_mu 
        s = self.s

        # Draw k (number of basis functions)
        l = numpy.random.exponential(s)
        k = numpy.random.poisson(l)
        
        if k==0   : k=1
        if k>=100 : k=100
				
        # Draw tau (kernel precision parameter)	
        mu = numpy.random.exponential(a_mu)
        b_tau = mu*a_tau				
				
        tau = numpy.zeros(k)
        tau = numpy.random.gamma(a_tau,b_tau,k)
		
        # Draw aj (coefficients of basis functions)				
        mu = numpy.zeros(k)
        aj = numpy.zeros(k)
								
        sigma = b0/a0 * numpy.identity(k)

        aj = self.multivariate_t(mu,sigma,k)
								
        # Draw nu (kernel locations)
        nu = numpy.zeros(k)			
        nu = numpy.random.uniform(0.0,1.0,k)
								
        return (k,tau,aj,nu)
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,x,y,xi):

        # Sample the prior					
        (k,tau,aj,nu) = self.samplePrior()
        self.theta = (k,tau,aj,nu)
								
        # Initialize the variables
        self.w = numpy.ones(1+4*k)
        self.y = y
        self.x = x
								
        (l,self.s,self.gamma) = (0,0,0)
					
        # Run Sequential Monte Carlo
        while l<100 :
            self.s = self.s + 1
												
            self.reweigh(l)
            self.resample()
            self.rejuvenate()
						
            if self.gamma==1 : (l,self.s,self.gamma)	 = (l+1,0,0)					
		
        # Return the output
        return (0,self.evalRegression(xi))


    #####################################################################################################
    # @brief Reweigh step
    #####################################################################################################
    def reweigh(self,n): 
        gammaOld = self.gamma
        gammaNew = 1.0
								
        piOld = self.evalPosterior(gammaOld,n)
        piNew = self.evalPosterior(gammaNew,n)
								
        self.w = self.w * piNew / piOld
								
        print self.w
								
        self.gamma = gammaNew
			
			
    #####################################################################################################
    # @brief Resample step
    #####################################################################################################
    def resample(self): pass
			
			
    #####################################################################################################
    # @brief Rejuvenate step
    #####################################################################################################
    def rejuvenate(self): pass
					
					
    #####################################################################################################
    # @brief Evaluates the posterior
    #####################################################################################################
    def evalPosterior(self,gamma,n): 
        return self.evalLikelihood(gamma,n)*self.evalPrior()
								
		
    #####################################################################################################
    # @brief Evaluates the likelihood
    #####################################################################################################
    def evalLikelihood(self,gamma,n): 
					
        numerator = special.gamma(self.a+(n+gamma)/2)	
								
        Fi = self.evalRegression(self.x)
				
        error = numpy.dot(self.y-Fi,self.y-Fi);
				
        denominator = numpy.power(self.b+error , self.a+(n+gamma)/2)						
								
        return numerator/denominator
								
						
    #####################################################################################################
    # @brief Evaluates the prior
    #####################################################################################################
    def evalPrior(self): 
        (k,tau,aj,nu) = self.theta

        c11 = numpy.power(1.0/(self.s+1.0),k+1.0)

        c21 = special.gamma(self.a_tau+1)/special.gamma(self.a_tau)
        c22 = pow(self.a_tau,self.a_tau) 
        c23 = 1/self.a_mu
        c24 = 1.0
					
        for j in range(0,k):
            c22 = c22 * numpy.power(tau[j],1-self.a_tau)
            c24 = c24 * numpy.power(self.a_tau*tau[j]+1/self.a_mu,-(1+self.a_tau))
					
        c31 = numpy.power(1.0/(2.0+numpy.pi),(k+1.0)/2.0)
        c32 = special.gamma(self.a0+(k+1)/2)
        c33 = 1.0/numpy.power(self.b0 + 0.5 * numpy.sum(numpy.square(aj)),self.a0+(k+1.0)/2.0)

        term1 = c11		
        term2 = c21 * c22 * c23 * c24
        term3 = c31 * c32 * c33
					
        return term1*term2*term3


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi):
        (k,tau,aj,nu) = self.theta	
					
        # Initialize the output
        yi = numpy.zeros(xi.size)
								
        # Evaluate the basis functions at the nodes
        K = numpy.zeros((xi.size,k))
							
        for i in range(0,xi.size):
            for j in range(0,k) : 
                K[i,j] = numpy.exp(-tau[j]*(xi[i]-nu[j])*(xi[i]-nu[j]))
																
        # Evaluate the fit
        for j in range(0,k) : yi = yi + aj[j]*K[:,j]
									
        return yi


    #####################################################################################################
    # @brief Generates random variables of multivariate t distribution
    #####################################################################################################
    def multivariate_t(self,mu,Sigma,nu=numpy.inf,n=1):
        mu = numpy.asarray(mu)
        d = len(mu)
        if nu == numpy.inf:
            x = 1.
        else:
            x = numpy.random.chisquare(nu, n)/nu
        z = numpy.random.multivariate_normal(numpy.zeros(d),Sigma)
        return mu + z/numpy.sqrt(x)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


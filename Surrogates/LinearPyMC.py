
# @file	nonparPyMC.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy as np
from scipy import special
import pymc
from numpy import matlib


#####################################################################################################
# @brief Define class
#####################################################################################################
class LR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): pass
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi):
        
        # Define the variables needed for the routine, with their prior distributions
        alpha = pymc.Uniform('alpha', -100, 100)

        @pymc.stochastic(observed=False)
        def beta(value=0):
            return -1.5 * np.log(1 + value ** 2)

        @pymc.stochastic(observed=False)
        def sigma(value=1):
            return -np.log(abs(value))

        # Define the form of the model and likelihood
        @pymc.deterministic
        def y_model(x=xdata, alpha=alpha, beta=beta):
            return alpha + beta * x

        y = pymc.Normal('y', mu=y_model, tau=1. / sigma ** 2, observed=True, value=ydata)

        # package the full model in a dictionary
        model1 = pymc.Model([alpha, beta, sigma, y_model, y])

        # run the basic MCMC: we'll do 100000 iterations to match emcee above
        S = pymc.MCMC(model1)
        S.sample(iter=10000, burn=5000)

        # extract the traces
        pymc_trace = [S.trace('alpha')[:],S.trace('beta')[:],S.trace('sigma')[:]]
		
        # Compute mean prediction
        aMean = alpha.stats()['mean']
        bMean = beta.stats()['mean']
        
        # Mean per each point xi
        mu = aMean + bMean*xi

        # Compute variance of prediction
        a = np.atleast_2d(S.trace('alpha')[:])
        b = np.atleast_2d(S.trace('beta')[:])

        # M predictions for N points (N x M matrix)
        fx = a + b * np.atleast_2d(xi).T
        M = fx.shape[1]
        
        var = np.square(fx - np.matlib.repmat(mu,M,1).T);
        std = np.sqrt(np.mean(var,axis=1))
                
        # Return the output
        return (mu,std)


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi,trace):
        
        alpha, beta = trace[:2]
        yi = alpha[:, None] + beta[:, None] * xi

        return yi.mean(0)




#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 15:20:38 2016

@author: alessio
"""

from sklearn.svm import SVR
import numpy
from numpy import matlib

#####################################################################################################
# @brief Define class
#####################################################################################################
class SVM:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self):
        
        self.svr = SVR(kernel='rbf', C=1e3, gamma=0.1)


    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,x,y):

        if x.ndim < 2 : x = numpy.atleast_2d(x).T
        if y.ndim < 2 : y = numpy.atleast_2d(y).T
        
        self.svr.fit(x,y)
    

    #####################################################################################################
    # @brief Evaluate only
    #####################################################################################################
    def eval(self,xi):
    
        if xi.ndim < 2 : xi = numpy.atleast_2d(xi).T
        
        # Estimate everything using built-in functions
        yi = self.svr.predict(xi)
        out = numpy.zeros([len(xi),1])
        out[:,0] = yi

        return (out,0.0)


    #####################################################################################################
    # @brief Evaluate standard deviation
    #####################################################################################################
    def evalVariance(self,xi):
        
        print "No variance information for Support Vector Machine surrogates!"
        exit()
    

    #####################################################################################################
    # @brief Sample from posterior
    #####################################################################################################
    def samplePosterior(self,xi):
        
        print "No posterior information for Support Vector Machine surrogates!"
        exit()


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


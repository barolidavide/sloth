
# @file	nonparPyMC.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy as np
from scipy import special
import pymc
from numpy import matlib


#####################################################################################################
# @brief Define class
#####################################################################################################
class NPR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): 
        self.ESS = 0.0
        self.w = 0.0
        self.l = 0
        self.s = 0
        self.g = 0
        self.a0 = 1.0
        self.b0 = 1.0	
        self.a = 2.0
        self.b = 1e-6
        self.a_tau = 1.0
        self.a_mu = 0.01
        self.k_max = 100
        self.s = 1.0
        self.gammaS = 1.0
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi):
        
        # Prior
        @pymc.stochastic(observed=False)
        def theta(value=np.zeros(3*4)):
            
            k = 4
            tau = value[0:k]
            aj = value[k:2*k]
            nu = value[2*k:3*k]
            
            c11 = np.power(1.0/(self.s+1.0),k+1.0)
        
            c21 = special.gamma(self.a_tau+1)/special.gamma(self.a_tau)
            c22 = pow(self.a_tau,self.a_tau)
            c23 = 1/self.a_mu
            c24 = 1.0
        
            for j in range(0,k):
                c22 = c22 * np.power(tau[j],1-self.a_tau)
                c24 = c24 * np.power(self.a_tau*tau[j]+1/self.a_mu,-(1+self.a_tau))
    
    
            c31 = np.power(1.0/(2.0+np.pi),(k+1.0)/2.0)
            c32 = special.gamma(self.a0+(k+1)/2)
            c33 = 1.0/np.power(self.b0 + 0.5 * np.sum(np.square(aj)),self.a0+(k+1.0)/2.0)
        
            term1 = c11
            term2 = c21 * c22 * c23 * c24
            term3 = c31 * c32 * c33
    
            return -np.log(term1*term2*term3)

        # Model
        @pymc.deterministic
        def mu(xi=xdata,theta=theta): return self.evalRegression(xi,theta)
        
        # Likelihood
        tau = pymc.Gamma('tau', alpha=self.a, beta=self.b)
        y = pymc.Normal('y', mu, tau, observed=True, value=ydata)

        # package the full model in a dictionary
        model1 = pymc.Model([theta, y])
			
        # Run the model
        S = pymc.MCMC(model1)
        S.sample(iter=10000, burn=5000)

        # Get output
        theta = S.trace('theta')[:]

        M = theta.shape[0]
        N =    xi.shape[0]
        
        #graph = pymc.graph.graph(model1)
        #graph.write_png('univariate.png')
        
        # M predictions for N points (N x M matrix)
        fx = np.zeros((N,M))

        for i in range(0,M):
            fx[:,i] = self.evalRegression(xi,theta[i,:])
        
        # Mean per each point xi
        avg = np.mean(fx,axis=1)
        var = np.square(fx - np.matlib.repmat(avg,M,1).T);
        std = np.sqrt(np.mean(var,axis=1))
                
        # Return the output
        return (avg,std)


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi,theta):
        
        k = 4
        tau = theta[0:k]
        aj = theta[k:2*k]
        nu = theta[2*k:3*k]
    
        # Initialize the output
        yi = np.zeros(xi.size)
								
        # Evaluate the basis functions at the nodes
        K = np.zeros((xi.size,k))
    
        for i in range(0,xi.size):
            for j in range(0,k) :
                K[i,j] = np.exp(-tau[j]*(xi[i]-nu[j])*(xi[i]-nu[j]))
        
        # Evaluate the fit
        for j in range(0,k) : yi = yi + aj[j]*K[:,j]

        return yi


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


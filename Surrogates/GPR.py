# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 15:20:38 2016

@author: alessio
"""

from sklearn import gaussian_process
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel,RBF,ConstantKernel as C
import numpy
from numpy import matlib

#####################################################################################################
# @brief Define class
#####################################################################################################
class GPR:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,n=0):
        
        self.n = n
        self.kernel = C(1.0, (1e-4, 1e4)) * RBF(10, (1e-4, 1e5)) \
                    + WhiteKernel(noise_level=1e-5, noise_level_bounds=(1e-8, 1e+5))


    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,x,y):

        if x.ndim < 2 : x = numpy.atleast_2d(x).T
        if y.ndim < 2 : y = numpy.atleast_2d(y).T
        
        if self.n==0 : self.n = x.shape[1]
        
        self.gp = gaussian_process.GaussianProcessRegressor(kernel=self.kernel,
                                                            alpha=1.e-2, n_restarts_optimizer=10)
        self.gp.fit(x[:,0:self.n],y)
    

    #####################################################################################################
    # @brief Evaluate only
    #####################################################################################################
    def eval(self,xi,stdDev=False):
    
        if xi.ndim < 2 : xi = numpy.atleast_2d(xi).T
        
        # This is a hack to estimate credible intervals
        #sigma_n = self.gp.kernel_.k2.noise_level
        #self.gp.kernel_.k2.noise_level = 0.0
        
        # Estimate everything using built-in functions
        if stdDev:
            (yi,sigma) = self.gp.predict(xi,return_std=True)
        else:
            yi = self.gp.predict(xi,return_std=False)
            sigma = yi[:,0]*0
        
        # Restore noise level
        #self.gp.kernel_.k2.noise_level = sigma_n

        return (yi.flatten(),sigma)


    #####################################################################################################
    # @brief Evaluate standard deviation
    #####################################################################################################
    def evalVariance(self,xi):
        
        M = 2000
        
        yi = self.gp.sample_y(xi[:,0:self.n],M)[:,0]
        
        mu = numpy.mean(yi,axis=1)
        y2 = numpy.mean(numpy.square(yi),axis=1)
        
        var = y2 - numpy.square(mu)
        std = numpy.sqrt(var)
        
        return std


    #####################################################################################################
    # @brief Sample from posterior
    #####################################################################################################
    def samplePosterior(self,xi_input,dim=1):
        
        if xi_input.ndim < 2 : xi_input = numpy.atleast_2d(xi_input).T
        #xi = xi_input[:,0:self.n]
        
        # This is a hack to estimate credible intervals
        #sigma_n = self.gp.kernel_.k2.noise_level
        #self.gp.kernel_.k2.noise_level = 0.0

        # Sample f every 100 xi samples
        #N = 100
        #M = xi.shape[0] / N

        yi = self.gp.sample_y(xi_input,n_samples=1)

        # Allocate result
        #dim = 100
        #yi = numpy.zeros([xi.shape[0],dim])
        #yi = self.gp.sample_y(xi,dim)[:,0,:]
        
        #yi = numpy.zeros(xi.shape[0])
        #for i in range(0,M):
        #    yi[i*N:(i+1)*N] = self.gp.sample_y(xi[i*N:(i+1)*N],dim)[:,0,0]

        # Restore noise level
        #self.gp.kernel_.k2.noise_level = sigma_n

        return numpy.mean(yi[:,:,0],axis=1)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."



# @file	test_scalar.cxx
# @brief	This file contains an implementation of a 2D Laplacian
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	7 September 2016

import numpy
import scipy.sparse
import scipy.sparse.linalg

###############################################################################
# This class assumes a regular row-wise numbering of nodes:
# The node (i,j) in the domain is mapped into the vector entry u[i*Nx+j]
###############################################################################
class Model:
    
    def __init__(self,functional):
        self.functional = functional
				
    def preprocess(self,l,k): pass 
    def postprocess(self,l,k): pass
					
    def takeSamples(self,l,mu,nObs):
					
        # Generate random variables			
        k = numpy.random.normal(mu,1e-1,nObs)
					
        # Allocate output			
        data = numpy.zeros((nObs,self.functional.getNumberOutputs()))
			
        # Perform one sample at the time					
        for i in range(0,nObs): data[i,:] = self.findSolution(l,k[i])	
				
        # Return result								
        return (data,False)
	
    
    def setPotential(self,activation_potential,activation_sites):
        self.acP = activation_potential
        self.bcH = activation_sites
    

    def findSolution(self,l,k):
        
        # Compute number of elements
        # and of interior vertices
        nEx = 2 * pow(2,l)
        nEy = 2 * pow(2,l)
        nx = nEx-1
        ny = nEy-1
        nV = nx * ny
        
        # Compute domain
        h = 1.0/nx;
        xi = numpy.linspace(h, 1-h, nx);
        yi = numpy.linspace(h, 1-h, ny);
        
        # Create RHS
        b = self.acP
        
        # Calculate BCs
        nHx = nx / 4
        nHy = ny / 4
        
        # Calculate inner domain indices
        chiH = numpy.arange(0,nHx*nHy,1)
        
        for i in range(0,nHy):
            for j in range(0,nHx): chiH[i*nHx+j] = (nHy+i)*nx+(nHx+j)
        
        # Calculate diagonals
        d1 = numpy.ones(nV-nx)
        d2 = numpy.ones(nV- 1)
        d3 = numpy.ones(nV   )
        d4 = numpy.ones(nV- 1)
        d5 = numpy.ones(nV-nx)
        
        # Make heart 10x less conductive
        for i in chiH :
            d3[i] = 10.0
            if i-nx>=0    : d1[i-nx] = 10.0
            if i-1 >=0    : d2[i-1 ] = 10.0
            if i   <nV-1  : d4[i   ] = 10.0
            if i   <nV-nx : d5[i   ] = 10.0
        
        # Apply BC on the heart
        for i in self.bcH:
            d3[i] = -0.25
            if i-nx>=0    : d1[i-nx] = 0.0
            if i-1 >=0    : d2[i-1 ] = 0.0
            if i   <nV-1  : d4[i   ] = 0.0
            if i   <nV-nx : d5[i   ] = 0.0
        
        # Create Laplacian matrix on the unit square
        L = scipy.sparse.diags(d1,-nx,format='csc') + \
            scipy.sparse.diags(d2,-1 ,format='csc') - \
          4*scipy.sparse.diags(d3, 0 ,format='csc') + \
            scipy.sparse.diags(d4, 1 ,format='csc') + \
            scipy.sparse.diags(d5, nx,format='csc')

        # Perform a simulation
        LU = scipy.sparse.linalg.splu(L)
        u = LU.solve(k*b)

        # Eval functional
        F = self.functional.eval(xi,yi,u)
        
        # Return result
        return F




# @file	Quintic.py
# @brief	This file contains an implementation of the Quintic function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	10 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics 
# (or of interfacing with external solvers)
###############################################################################
class Quintic:
    
    def __init__(self,functional,level):
        self.functional = functional
        self.level = level
    
    def getDuplicate(self):
        return Quintic(self.functional,self.level)
    
    def preprocess(self,k): pass
    def postprocess(self,k): pass
    
    def findSolution(self,z): return self.functional.eval(self,z)
					
    def evalModel(self,k):
        nObs = numpy.asarray(k).shape[0]
        
        # Parameters
        a = 1.0
        b = 0.1
        
        # Allocate output
        F = numpy.zeros(nObs)

        # Loop over samples
        for i in range(0,nObs):
        
            Z = k[i,:]
        
            if self.level == 0:
                u = m.sin(Z[0]) + a*(m.sin(Z[1])**2.) +       b*(Z[2]**5.)
            elif self.level == 1:
                u = m.sin(Z[0]) + a*(m.sin(Z[1])**2.) +  20.0*b*(Z[2]**3.)
            elif self.level == 2:
                u = m.sin(Z[0]) + a*(m.sin(Z[1])**2.) + 200.0*b*(Z[2]**1.)
        
            F[i] = u
        
        # Return result
        return numpy.atleast_2d(F).T



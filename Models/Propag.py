
# @file	Propag.py
# @brief	This file contains an easy interface to propag
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	13 February 2018

import sys
sys.path.append('../Scripts')
from config import *
import os
import subprocess
import threading
import scipy.ndimage as ndimage
import scipy.interpolate as interp
from pyccmc.anatomy import create_block
from pyccmc.anatomy.angle2byte import angle2byte
from pyccmc import igb_read,igb_write
from datetime import datetime
from textwrap import dedent

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class Propag:

    def __init__(self,name,functional='full',reuse=False,dx=0.02):
        self.name = name
        self.functional = functional
        self.reuse = reuse
    
        # Geometry
        self.dx   = dx
        self.side = 1.0

        # stimulus of 1 mm^3 in the lower-left side
        self.cfun = lambda x,y,z: 1 + numpy.logical_and(x<0.1,numpy.logical_and(y<0.1,z<0.1))

        # rotating fibers
        a0,a1 = -numpy.pi/3.0,numpy.pi/3.0
        self.afun = lambda x,y,z: a0 + (a1-a0)/self.side * x
    
    
    # Set output mesh
    def setOutputMesh(self,Xout,mask):
        self.Xout = Xout
        self.mask = mask


    # Initialize from patient specific data
    def loadPatient(self,patient,anatomy):
        self.patient = patient
        self.anatomy = anatomy
        
        # Store folder paths
        self.anatomy_dir = "/scratch/eikonal/anatomy/{}/".format(self.patient)
        self.results_dir = "/scratch/eikonal/results/{}/".format(self.patient)
        
        # Computational mesh
        if self.dx == 0.02: self.prefix = patient
        else:               self.prefix = patient + "_dx{}_".format(self.dx)
        
        # Write cube geometry
        if self.patient == 'cube':
            n = int(self.side/self.dx)
            self.nelm = (n,n,n)
            self.size = (self.side,self.side,self.side)
            create_block(self.prefix,wdir=self.anatomy_dir,
                         size=self.size,nelm=self.nelm,cell=self.cfun,alpha=self.afun,
                         angles_compressed_type=False)

        # Save mean field
        self.var0 = self.getMean()
        self.Xout = self.getMesh()
        self.mask = self.getMask()
        self.mesh = self.getMesh()

    
    # Get number of DOF
    def nDof(self):
        return numpy.ma.count(numpy.ma.MaskedArray(self.Xout[0], self.mask).flatten())
    
    
    # Get solution shape
    def getSolShape(self):
        return self.var0.shape
    
    
    # Get mesh size
    def getSpacing(self):
        return self.dx
    
    
    # Get mesh
    def getMesh(self):
        (Nx,Ny,Nz) = self.var0.shape
        x = numpy.linspace(0,1,Nx)
        y = numpy.linspace(0,1,Ny)
        z = numpy.linspace(0,1,Nz)
        X = numpy.meshgrid(x,y,z,indexing='ij')
        return X
    
    
    # Get vertex mesh
    def getVert(self,function):
        (Nx,Ny,Nz) = function.shape
        x = numpy.linspace(0,1,Nx)
        y = numpy.linspace(0,1,Ny)
        z = numpy.linspace(0,1,Nz)
        X = numpy.meshgrid(x,y,z,indexing='ij')
        return X
    
    
    # Get mask
    def getMask(self):
        if self.patient == 'cube':  cell_name = "{0}-c.igb".format(self.anatomy)
        elif self.dx == 0.02:       cell_name = "{0}-c.igb".format(self.anatomy)
        else:                       cell_name = "{0}-e.igb".format(self.anatomy)
        
        cell = numpy.swapaxes(igb_read(os.path.join(self.anatomy_dir,cell_name)),0,2)
        mask = numpy.logical_not(numpy.in1d(cell.ravel(),[1,99,100,101,111,112,121,122]).reshape(cell.shape))
        
        return mask
    
    
    # Get mean
    def getMean(self):
        file = os.path.join(self.anatomy_dir,"{}-a.igb".format(self.anatomy))
        if os.path.isfile(file):
            print "Mean field {} found.".format(file)
            if self.patient == 'cube':
                tmp = numpy.swapaxes(igb_read(file),0,2)
            else:
                hdr = igb_read(file,only_header=True)
                tmp = numpy.swapaxes(igb_read(file),0,2)
                tmp = hdr['facteur']*tmp.astype(numpy.float) + hdr['zero']
            return tmp
        else:
            print "Mean field {} not found!".format(file)
            exit()


    # Writes var to a file
    def writeVar(self,nfile):
        file = os.path.join(self.anatomy_dir,"{}{:03d}-a.igb".format(self.prefix, nfile))
        if os.path.isfile(file):
            pass
        else:
            print "Anatomy {} not present, writing it to file.".format(file)
            #self.var.set_fill_value(numpy.nan)
            ang_byte,fact_a,zero_a = angle2byte(numpy.swapaxes(self.var,0,2))
            igb_write(ang_byte,file,facteur=fact_a,zero=zero_a)


    # Sets random field
    def setRandomField(self, field):
        self.field = field


    # Compute new fiber field
    def computeVar(self,Yi,nfile):
        file = os.path.join(self.anatomy_dir,"{}{:03d}-a.igb".format(self.prefix, nfile))
        if os.path.isfile(file):
            pass
        else:
            print "Anatomy {} not present, computing it.".format(file)
            stddev = numpy.pi/6.0
            #if (phi<-3.14).any() or (phi>3.14).any(): print "Perturbation above pi!!"
            self.var = self.var0 + stddev*self.resample(self.field.sample(Yi),'none',self.mesh)


    # Takes sample
    def findSolution(self,Y):
        
        # Allocate memory
        self.samples = numpy.zeros((len(Y),self.nDof()))
        
        # Write all random variables to file
        for i,Yi in enumerate(Y):
            self.computeVar(Yi,i)
            self.writeVar(i)

        # Distribute work
        if Y.shape[0] == 1:
            self.runEikonal(Y,0,1)
        else:
            P = min(300,Y.shape[0])
            N = (numpy.linspace(0,Y.shape[0],P+1)).astype(int)
            print "Sample allocation to threads: {}".format(N)
            thread_list = []
            for i in range(P):
                t = threading.Thread(target=self.runEikonal,args=(Y,N[i],N[i+1]))
                thread_list.append(t)
            for thread in thread_list: thread.start()
            for thread in thread_list: thread.join()
    
        print "All simulations of {} performed".format(self.name)
    
        # Read out solutions
        if Y.shape[0] == 1:
            self.loadSolution(0,1)
        else:
            P = min(1,Y.shape[0]) # FIXME: igb_read fails in multithreading
            N = (numpy.linspace(0,Y.shape[0],P+1)).astype(int)
            thread_list = []
            for i in range(P):
                t = threading.Thread(target=self.loadSolution,args=(N[i],N[i+1]))
                thread_list.append(t)
            for thread in thread_list: thread.start()
            for thread in thread_list: thread.join()

        # Evaluate the sample statistics
        if self.functional == 'full':   return self.samples
        else:                           return self.functional.eval(self.samples)
        
        
    # Load solution from file
    def loadSolution(self,start,end):
        for i in range(start,end):
            if self.name == 'propeiko':
                fname_dtime = os.path.join(self.results_dir,"{}{:03d}e_dtime_reduced.igb".format(self.prefix,i))
                fname_full = os.path.join(self.results_dir,"{}{:03d}e_dtime.igb".format(self.prefix,i))
            if self.name == 'propag':
                fname_dtime = os.path.join(self.results_dir,"{}{:03d}_dtime_reduced.igb".format(self.prefix,i))
                fname_full = os.path.join(self.results_dir,"{}{:03d}_dtime_full.igb".format(self.prefix,i))
        
            # Try reading full file if compressed not available
            if not os.path.isfile(fname_dtime):
                dtime = numpy.swapaxes(igb_read(fname_full),0,2)
                dtime = numpy.array(dtime,dtype=numpy.float64)
                
                print "Compressed file {} not available, downsampling full solution".format(fname_dtime)
                
                # Resample to output mesh
                sol = self.resample(dtime,'vert',self.Xout)
                
                print "Full solution shape {}, total elements {}, outside of domain {}".format(dtime.shape,dtime.size,numpy.count_nonzero(numpy.isnan(dtime)))
                print "Reduced solution shape {}, total elements {}, outside of domain {}".format(sol.shape,sol.size,numpy.count_nonzero(numpy.isnan(sol)))
                
                # Save solution to file
                igb_write(numpy.swapaxes(sol,0,2),fname_dtime)
        
            # Try reading the compressed file
            if os.path.isfile(fname_dtime):
                dtime = numpy.swapaxes(igb_read(fname_dtime),0,2)
                dtime = numpy.array(dtime,dtype=numpy.float64)
                sys.stdout.flush()
    
                # insert the sample
                self.samples[i,:] = numpy.ma.MaskedArray(dtime,self.mask).compressed()
            else:
                print "Solution file not found, quit!"
                exit()


    # Resample function on a different mesh
    def resample(self,input,Xin='none',Xout='none'):
    
        # Compute dimensions
        (nx,ny,nz) = input.shape
        (Nx,Ny,Nz) = Xout[0].shape
        ds = (float(Nx)/float(nx),float(Ny)/float(ny),float(Nz)/float(nz))
        
        # Return input if ds==1
        if ds[0]==1. and ds[1]==1. and ds[2]==1. :
            print "Same resolution. Resampling bypassed."
            return input
        elif ds[0]>1. and ds[1]>1. and ds[2]>1. :
            print "Upsampling requested, factor ds=({},{},{})".format(ds[0],ds[1],ds[2])
        elif ds[0]<1. and ds[1]<1. and ds[2]<1. :
            print "Downsampling requested, factor ds=({},{},{})".format(ds[0],ds[1],ds[2])
        else:
            print "Mixed up- and downsampling, unexpected - quit."
            exit()
        
        # Compute new mesh
        X = Xout[0][:,0,0]
        Y = Xout[1][0,:,0]
        Z = Xout[2][0,0,:]
        
        if Xin == 'none':
            x = ndimage.zoom(X,1./ds[0],X.dtype,1)
            y = ndimage.zoom(Y,1./ds[1],Y.dtype,1)
            z = ndimage.zoom(Z,1./ds[2],Z.dtype,1)
        elif Xin == 'vert':
            Xin = self.getVert(input)
            x = Xin[0][:,0,0]
            y = Xin[1][0,:,0]
            z = Xin[2][0,0,:]
        else:
            x = Xin[0][:,0,0]
            y = Xin[1][0,:,0]
            z = Xin[2][0,0,:]
        
        # Compute interpolation
        f = interp.interp1d(x,input,kind='linear',axis=0)
        tempX = f(X)
        f = interp.interp1d(y,tempX,kind='linear',axis=1)
        tempY = f(Y)
        f = interp.interp1d(z,tempY,kind='linear',axis=2)
        tempZ = f(Z)
    
        return tempZ


    # Call GPU solver
    def runEikonal(self,Y,start,end):
        for i in range(start,end):
            reuse = self.reuse
            if reuse==True:
                if self.name == 'propeiko':
                    fname_dtime = os.path.join(self.results_dir,"{}{:03d}e_dtime.igb".format(self.prefix,i))
                if self.name == 'propag':
                    fname_dtime = os.path.join(self.results_dir,"{}{:03d}_dtime_full.igb".format(self.prefix,i))
                if os.path.isfile(fname_dtime):
                    pass
                else:
                    print "Load failed for {0} model at sample {1}".format(self.name,i)
                    reuse = False
        
            # Prepare system call
            p = "{}{:03d}".format(self.prefix,i)
            args  = ["-fname_alpha","{}-a".format(p)]
            args += ["-logfile","logfile_{}".format(p)]
            args = " ".join(args)
            
            if self.name == 'propag':
                header_run = """\
                #!/bin/sh
                #SBATCH --job-name=CRT001P{0}
                #SBATCH --time=00:30:00
                #SBATCH --nodes=8
                #SBATCH --ntasks-per-core=1
                #SBATCH --ntasks-per-node=20
                #SBATCH --cpus-per-task=1
                #SBATCH --signal=TERM@5
                \n""".format(i)
                
                header_merge = """\
                #!/bin/sh
                #SBATCH --job-name=CRT001P{0}
                #SBATCH --time=00:30:00
                #SBATCH --nodes=1
                #SBATCH --ntasks-per-core=1
                #SBATCH --ntasks-per-node=1
                #SBATCH --cpus-per-task=20
                #SBATCH --signal=TERM@5
                #SBATCH --dependency=singleton
                \n""".format(i)
                
                common = """\
                # manage the host name on the cluster
                host=$(hostname)
                if [[ $host =~ ^icsnode[0-9]{{2}}$ ]]; then
                  host="ics"
                elif [[ $host =~ ^daint[0-9]{{3}}$ ]]; then
                  host="daint"
                fi
                echo "Hostname: ${{host}}"
                
                export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
                export MPICH_MAX_THREAD_SAFETY=funneled
                export TRAP_FPE='ALL=TRACE(1),ABORT'
                ulimit -c 20000000
                ulimit -s 81920
                trap 'echo got SIGTERM' TERM
                trap 'echo got SIGINT' INT
                trap 'echo got SIGPWR' PWR

                module load netcdf/4.4.1.1-gcc-7.1.0 petsc/3.7.6-gcc-7.1.0
                module list
                \n""".format(self.patient)
                
                run_cmd = """\
                mpirun -n $SLURM_NTASKS propag5 +F ../Common.par +F ../{0}_propag.par -prefix {1} {2} || exit 1
                \n""".format(self.patient,p,args)
                
                merge_cmd = """\
                mpirun -n $SLURM_NTASKS prmerge5 {1}_dtime_full.x -dir_input /scratch/eikonal/results/{0} -dir_output /scratch/eikonal/results/{0} -logfile merge_log_{1} || exit 1
                mpirun -n $SLURM_NTASKS prmerge5 {1}_dtime_heart.x -dir_input /scratch/eikonal/results/{0} -dir_output /scratch/eikonal/results/{0} -logfile merge_log_{1} || exit 1
                mpirun -n $SLURM_NTASKS prmerge5 {1}_tcc_full.x -dir_input /scratch/eikonal/results/{0} -dir_output /scratch/eikonal/results/{0} -logfile merge_log_{1} || exit 1
                \n""".format(self.patient,p)
                
                file_run = "../{0}/batch/run_propag_{1}.sh".format(self.patient,i)
                with open(file_run,"w") as f:
                    f.write(dedent(header_run))
                    f.write(dedent(common))
                    f.write(dedent(run_cmd))
                
                file_merge = "../{0}/batch/merge_propag_{1}.sh".format(self.patient,i)
                with open(file_merge,"w") as f:
                    f.write(dedent(header_merge))
                    f.write(dedent(common))
                    f.write(dedent(merge_cmd))

            # Compute new solution
            if reuse==False:
                if self.name == 'propeiko':
                    print "Run Propeiko sample {0}".format(i)
                    subprocess.call("srun -p gpu propeiko +F ../{0}/Common.par +F ../{0}/{0}_propeiko_dx{1}.par -prefix {2}e {3}".format(self.patient,self.dx,p,args),shell=True)
                    print "Propeiko done"
                if self.name == 'propag':
                    print "Run Propag sample {0}".format(i)
                    subprocess.call("cd ../{0}/batch && sbatch run_propag_{1}.sh && sbatch merge_propag_{1}.sh && cd ../../Examples".format(self.patient,i),shell=True)
                    print "Propag done"












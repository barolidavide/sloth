
# @file	Ishigami.py
# @brief	This file contains an implementation of a generic Surrogate model
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	22 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics 
# (or of interfacing with external solvers)
###############################################################################
class Surrogate:
    
    def __init__(self,modelX,modelY,regression,sampler,samples):
        self.modelX = modelX
        self.modelY = modelY
        self.sampler = sampler
        self.z0 = numpy.zeros((samples,sampler.n))
        for i in range(samples): self.z0[i,:] = sampler.sample()
        self.x0 = numpy.atleast_2d(self.modelX.findSolution(self.z0))
        self.y0 = numpy.atleast_2d(self.modelY.findSolution(self.z0))
        self.n = samples
        self.regression = regression
        for ri,r in enumerate(self.regression): r.fit(self.x0[:,ri],self.y0[:,ri])
    
    def getDuplicate(self):
        return Surrogate(self.functional,self.regression)
    
    def preprocess(self,k): pass
    def postprocess(self,k): pass
					
    def findSolution(self,k,doPlots=False):
        nObs = numpy.asarray(k).shape[0]
        
        self.y = numpy.zeros((nObs,self.modelX.functional.getNumberOutputs()))
        self.f = numpy.zeros((nObs,self.modelX.functional.getNumberOutputs()))
        
        # Compute input
        self.x = numpy.atleast_2d(self.modelX.findSolution(k))
        
        # Compute output
        for ri,r in enumerate(self.regression):
            (self.f[:,ri],self.sigma) = r.eval(self.x[:,ri],doPlots)
                        
        # Do plots only for debug runs
        if doPlots:
        
            # Compute full posterior
            self.y = numpy.zeros(self.f.shape)
            
            for ri,r in enumerate(self.regression):
                self.y[:,ri] = r.samplePosterior(self.x[:,ri])
            
            # Plot result
            nModels = self.x.shape[1]
            f,ax = plt.subplots(1,nModels)
            
            xPlot = numpy.linspace(numpy.amin(self.x0), numpy.amax(self.x0), 100)
            
            for ri,r in enumerate(self.regression):
                (yPlot,sPlot) = r.eval(xPlot,doPlots)
            
            axv = l1 = l2 = l3 = l4 = []
            labels = ('y=x','Observations','Prediction','95%')
            if nModels == 1:    axv.append(ax)
            else:               axv = ax
            
            for i in range(nModels):
                l1, = axv[i].plot(self.f,  self.f,  'r:', label=u'$y = x$')
                l2, = axv[i].plot(self.x0, self.y0, 'b.', markersize=10, label=u'Observations')
                l3, = axv[i].plot(xPlot,  yPlot,  'k.', label=u'Prediction')
                l4, = axv[i].fill(numpy.concatenate([xPlot[:], xPlot[::-1]]),
                                  numpy.concatenate([yPlot[:] - 1.9600 * sPlot,
                                                    (yPlot[:] + 1.9600 * sPlot)[::-1]]),
                           alpha=.5, fc='0.75', ec='None', label='95%')
                axv[i].set_xlabel('Low-fidelity',fontsize=16)
                axv[i].set_ylabel('High-fidelity',fontsize=16)
                axv[i].grid()
                axv[i].legend(loc='upper left')

            axes = plt.gca()
            #axes.set_xlim([numpy.amin(self.x0),numpy.amax(self.x0)])
            #axes.set_ylim([numpy.amin(self.y0),numpy.amax(self.y0)])
            mu = numpy.mean(self.f,0)
            plt.suptitle('mean = %f, std_dev = %f' % (mu,numpy.mean(self.sigma)/mu),fontsize=16)
            plt.savefig('results/regression_'+str(self.n)+'.png')
            plt.close()

        # Return result
        return self.f





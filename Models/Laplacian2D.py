
# @file	test_scalar.cxx
# @brief	This file contains an implementation of a 2D Laplacian
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	7 September 2016

import numpy

###############################################################################
# This class assumes a regular row-wise numbering of nodes:
# The node (i,j) in the domain is mapped into the vector entry u[i*Nx+j]
###############################################################################
class Laplacian2D:
    
    def __init__(self,functional,level):
        self.functional = functional
        self.level = level
				
    def preprocess(self,l,k): pass 
    def postprocess(self,l,k): pass

    def findSolution(self,k):
    
        nObs = numpy.asarray(k).shape[0]
        
        # Compute number of elements
        # and of total vertices
        nEx = 2 * pow(2,self.level)
        nEy = 2 * pow(2,self.level)
        nx = nEx+1
        ny = nEy+1
        nV = nx * ny
        
        # Compute domain
        h = 1.0/nEx;
        xi = numpy.linspace(0., 1., nx);
        yi = numpy.linspace(0., 1., ny);
        
        # Create Laplacian matrix on the unit square
        L = numpy.diag(numpy.ones(nV-nx),-nx) + \
            numpy.diag(numpy.ones(nV- 1),-1 ) - \
          4*numpy.diag(numpy.ones(nV   ), 0 ) + \
		    numpy.diag(numpy.ones(nV- 1), 1 ) + \
            numpy.diag(numpy.ones(nV-nx), nx)
        
        # Create RHS
        b = 100*pow(h,2)*numpy.ones(nV)
        
        # Allocate output
        F = numpy.zeros((nObs,self.functional.getNumberOutputs()))
        
        # Apply BC
        bc = numpy.arange(0,2*(nx+ny),1)
        V1 = 0
        V2 = nx-1
        V3 = nx*ny-1
        V4 = ny*(nx-1)
        
        bc[0*nx+0*ny:1*nx+0*ny] = numpy.arange(V1,V2+1,  1)
        bc[1*nx+0*ny:1*ny+1*nx] = numpy.arange(V1,V3+1, nx)
        bc[1*nx+1*ny:2*nx+1*ny] = numpy.arange(V4,V3+1,  1)
        bc[2*nx+1*ny:2*nx+2*ny] = numpy.arange(V2,V3+1, nx)
        
        for i in bc:
            b[i] = 0
            L[i,:] = numpy.zeros(nV)
            L[i,i] = 1.0

        for i in range(0,nObs):
            # Perform a simulation
            u = numpy.linalg.solve(L,k[i]*b)

            # Eval functional
            F[i,:] = self.functional.eval(xi,yi,u)
        
        # Return result
        return F



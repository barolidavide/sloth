
# @file	EikonalEAS.py
# @brief	This file contains the model implementation to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	12 October 2016

import sys
sys.path.append('../Distributions')
from pyccmc import *
from collections import namedtuple
import pickle
import numpy


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class Model:

    def __init__(self,functional): self.functional = functional
    def preprocess(self,l,k): pass
    def postprocess(self,l,k): pass


    def findSolution(self,Y):
        out = numpy.zeros(Y.shape[0])
        
        for i,Yi in enumerate(Y):
        
            EAS = Yi.astype(int)
            EAS = numpy.append(EAS,0.0)
            pacings = [tuple(EAS)]

            print pacings

            exp = 'crt001s01'
            r1 = self.run_experiment(exp,
                            pacings,
                            alpharvfast=10.0,
                            alphalvfast=10.0,
                            sitratio=1.0)

            #print "{}: QRSd = {}, TST = {}, TAT = {}, TeQRSd = {}".format(exp,*r1)
            
            out[i] = self.functional.eval(r1)
        
        return out
    

    def run_experiment(self,prefix,pacings,
            alphalvfast=10.0,alpharvfast=10.0,sitratio=0.1,
            leadfun=None):
        ExpResult = namedtuple('ExpResult','qrsd,tst,tat,teqrsd,w,ecg')
        CT = PyPropeiko.CellType
        # initialize parameters
        with open('../../input/CRT001j01.par.pickle',"r") as f:
            params = pickle.load(f)
        datadir = '/scratch/eikonal/anatomy/crt001'
        params['dir_input'] = datadir
        params['dir_output'] = '/scratch/eikonal/exp/mf_eas/'
        # pacing-sites from external list
        params['pacingsites'] = [PacingSite(*p) for p in pacings]
        # ecg setup
        celltypes = [CT.LVFASTENDO,CT.LVFASTENDOSEPT,
                     CT.RVFASTENDO,CT.RVFASTENDOSEPT,
                     CT.LVENDOSEPT,CT.LVMIDSEPT,CT.LVEPISEPT,
                     CT.RVENDOSEPT,CT.RVMIDSEPT,CT.RVEPISEPT,
                     CT.LVENDO,CT.LVMID,CT.LVEPI,
                     CT.RVENDO,CT.RVMID,CT.RVEPI]
        leads = ['E1','E2','E3','aVR','aVF','aVL','V1','V2','V3','V4','V5','V6']
        if leadfun is None:
            params['leadfields'] = [LeadField(
                lead=lead,
                fname_lead="crt001LFa-{}_vef_heart".format(lead),
                fname_w="{}-LF{}_w".format(prefix,lead),
                fname_ecg="{}-LF{}_ecg".format(prefix,lead))
                for lead in leads]
        elif isinstance(leadfun,str):
            params['leadfields'] = [LeadField(
                lead=lead,
                fname_lead="{}LFa-{}_vef_heart".format(leadfun,lead),
                fname_w="{}-LF{}_w".format(prefix,lead),
                fname_ecg="{}-LF{}_ecg".format(prefix,lead))
                for lead in leads]
        else:
            # lead-field is Z(x,y,z)
            fcell = "{dir_input}/{fname_cell}.igb".format(**params)
            chdr = igb_read(fcell,only_header=True)
            nz,ny,nx = chdr['z'],chdr['y'],chdr['x']
            x = np.linspace(0.0,nx*0.1,nx+1)
            y = np.linspace(0.0,ny*0.1,ny+1)
            z = np.linspace(0.0,nz*0.1,nz+1)
            zz,yy,xx = np.meshgrid(z,y,x,indexing='ij',sparse=False)
            lead = leadfun(xx,yy,zz).astype(np.float32)
            igb_write(lead,'{}-LFV0.igb'.format(prefix))
            params['leadfields'] = [LeadField(
                lead="V0",
                fname_lead="{}-LFV0".format(prefix),
                fname_w="{}-LFV0_w".format(prefix),
                fname_ecg="{}-LFV0_ecg".format(prefix))]
        # AP
        ap_fname = "../vm_tp06_endo"
        ap_dt = 0.5
        params['aps'] = [ActionPotential(
            name="tp06endo",
            cells=celltypes,
            fname=ap_fname,
            dt=ap_dt,
            t0=0.0)]
        params['ecg_length'] = 600.0
        params['ecg_dt'] = 0.5

        #params['aps'] = []
        #params['leadfields'] = []
        params['propeiko_exe'] = '/scratch/eikonal/repo/ccmc-inverse/propeiko/propeiko_cuda'
        params['propeiko_ecg_exe'] = '/scratch/eikonal/repo/ccmc-inverse/propeiko/propeiko_ecg_mc_cuda'

        params['logfile'] = 'logfile_{}'.format(prefix)
        propeiko = PyPropeiko(params)
        propeiko.solve(dryrun=False,ecg=False)

        sol = propeiko.get_solution()
        w = propeiko.get_ecgs_w()
        ecg = propeiko.get_ecgs()
        print sol.ravel().max()

        return sol.ravel().max()#ExpResult(*propeiko.dtime_markers(),w=w,ecg=ecg)


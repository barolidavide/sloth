
# @file	EikonalAngle.py
# @brief	This file contains an easy interface to propeiko to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 February 2017

import numpy
import os
import sys
import subprocess
import threading
import pickle
from prepareslab import prepare_slab
from pyccmc import igb_read,igb_write, PyPropeiko
from pyccmc.pypropeiko import PacingSite
from angle2byte import angle2byte

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class EikonalCUDA:

    def __init__(self,name,functional='full',reuse=False):
        self.name = name
        self.functional = functional
        self.reuse = reuse
    

    # Initialize from patient specific data
    def loadPatient(self,filename):
        with open(filename,'rb') as f:
            params = pickle.load(f)
            #self.propeiko = PyPropeiko()
            self.propeiko = PyPropeiko(params=params)
        self.propeiko.params['dir_input'] = '../../input'
        self.propeiko.params['dir_output'] = '../../output'
        self.fname = self.propeiko.params['fname_acttimes']


    # Set activation site
    def setActivationSite(self,ei,ej,ek):
        self.propeiko.params['pacingsites'] = [PacingSite(ei,ej,ek,0.0)]
    
    
    # Get number of DOF
    def nDof(self):
        return numpy.ma.count(numpy.ma.MaskedArray(self.var0, self.getMask()).flatten())
    
    
    # Get solution shape
    def getSolShape(self):
        return self.var0.shape
    
    
    # Get mesh size
    def getSpacing(self):
        return self.propeiko.params['hx']
    
    
    # Get mesh
    def getMesh(self):
        (Nx,Ny,Nz) = self.var0.shape
        x = numpy.linspace(0,1,Nx)
        y = numpy.linspace(0,1,Ny)
        z = numpy.linspace(0,1,Nz)
        X = numpy.meshgrid(x,y,z,indexing='ij')
        return X
    
    
    # Get mask
    def getMask(self):
        mask = self.propeiko.get_cellmask()
        mask = numpy.logical_not(mask)
        return mask
    
    
    # Get mean
    def getMean(self):
        if self.name=='alpha':
            filename = "{dir_input}/{fname_alpha}.igb".format(**self.propeiko.params)
            hdr = igb_read(filename,only_header=True)
            fact_a,zero_a = hdr['facteur'],hdr['zero']
            self.var0 = fact_a*igb_read(filename) + zero_a
        if self.name=='theta':
            filename = "{dir_input}/{fname_theta}.igb".format(**self.propeiko.params)
            self.var0 = igb_read(filename)
        return self.var0
    
    
    # Writes var to a file
    def writeVar(self,nfile):
        filename = self.fname + '{}'.format(nfile)
        name32 = '../../input/slab-{0}randf_{1}.igb'.format(self.name,nfile)
        name8 = '../../input/slab-{0}_{1}.igb'.format(self.name,nfile)
        self.propeiko.params['fname_acttimes'] = filename
        igb_write(self.var.astype(numpy.float32),name32)
        ang_byte,fact_a,zero_a = angle2byte(self.var)
        igb_write(ang_byte.astype(numpy.uint8),name8,facteur=fact_a,zero=zero_a)
        self.propeiko.params['fname_'+self.name] = 'slab-{0}_{1}'.format(self.name,nfile)
    
    
    # Computes average angle
    def preprocess(self,angleMinMax):
        if self.name=='alpha':
            self.propeiko,_,_,_ = prepare_slab(nsub=1,nfact=10,
                                               prefix='slab',
                                               ang_alpha=angleMinMax,
                                               sigma_il=2.0,sigma_it=2.0,sigma_el=2.0,sigma_et=2.0,
                                               leadfun=None)
        if self.name=='theta':
            self.propeiko,_,_,_ = prepare_slab(nsub=1,nfact=10,
                                               prefix='slab',
                                               ang_theta=angleMinMax,
                                               sigma_il=2.0,sigma_it=2.0,sigma_el=2.0,sigma_et=2.0,
                                               leadfun=None)
        
        self.propeiko.params['dir_input'] = '../../input'
        self.propeiko.params['dir_output'] = '../../output'
        filename = 'slab-'+self.name+'.igb'
        os.rename(filename,self.propeiko.params['dir_input']+'/'+filename)
        os.rename('slab-cell.igb',self.propeiko.params['dir_input']+'/slab-cell.igb')
        os.rename('slab-gamma.igb',self.propeiko.params['dir_input']+'/slab-gamma.igb')
        os.rename('slab-phi.igb',self.propeiko.params['dir_input']+'/slab-phi.igb')
        self.var0 = self.getMean()
        self.fname = self.propeiko.params['fname_acttimes']
        return self.var0
    
    
    # Postprocess if needed (possibly remove)
    def postprocess(self,l,k): pass


    # Sets random field
    def setRandomField(self, field):
        self.field = field


    # Compute new fiber field
    def computeVar(self,Yi):
        if self.name=='alpha':
            #if (phi<-3.14).any() or (phi>3.14).any():
            #print "Perturbation above pi!!"
            self.var = self.var0 + 0.5*self.field.sample(Yi)
        if self.name=='theta':
            self.var = self.var0 + 0.5*self.field.sample(Yi)
            if (self.var<0).any(): print "Var below zero!!"


    # Takes sample
    def findSolution(self,Y,type='functional'):
        
        # Allocate memory
        if type == 'full': self.out = []
        else: self.out = numpy.zeros(len(Y))
        
        # Write all random variables to file
        for i,Yi in enumerate(Y):
            self.computeVar(Yi)
            self.writeVar(i+1)
            pickle_name = '../../input/propeiko_{0}.pkl'.format(i)
            with open(pickle_name,'wb') as output: pickle.dump(self.propeiko,output,pickle.HIGHEST_PROTOCOL)

        self.mask = self.getMask()

        # Distribute work
        if Y.shape[0] == 1:
            self.runEikonal(Y,0,1)
        else:
            P = 8
            N = (numpy.linspace(0,Y.shape[0],P+1)).astype(int)
            print N
            thread_list = []
            for i in range(P):
                t = threading.Thread(target=self.runEikonal,args=(Y,N[i],N[i+1]))
                thread_list.append(t)
            for thread in thread_list: thread.start()
            for thread in thread_list: thread.join()
        
        # Read out solutions
        if Y.shape[0] == 1:
            self.loadPickle(0,1,type)
        else:
            P = 80
            N = (numpy.linspace(0,Y.shape[0],P+1)).astype(int)
            print N
            thread_list = []
            for i in range(P):
                t = threading.Thread(target=self.loadPickle,args=(N[i],N[i+1],type))
                thread_list.append(t)
            for thread in thread_list: thread.start()
            for thread in thread_list: thread.join()

        return self.out
        
        
    # Load solution from file
    def loadPickle(self,start,end,type):
        for i in range(start,end):
            pickle_name = '../../output/propeiko_{0}.pkl'.format(i)
            if start==0 : print pickle_name
            with open (pickle_name,'rb') as input:
                dtime = pickle.load(input)
                sys.stdout.flush()
        
                # Convert solution to cell based
                Nx = dtime.shape[0]
                Ny = dtime.shape[1]
                Nz = dtime.shape[2]
            
                st = [0,1]
                fiX = [Nx-1,Nx]
                fiY = [Ny-1,Nz]
                fiZ = [Nz-1,Nz]
                sol = numpy.zeros((Nx-1,Ny-1,Nz-1))
                for x in range(2):
                    for y in range(2):
                        for z in range(2):
                            sol = sol + 0.125*dtime[st[x]:fiX[x],st[y]:fiY[y],st[z]:fiZ[z]]
    
                if type == 'full' :
                    print sol.shape
                    self.out.append( numpy.ma.MaskedArray(sol,self.mask) )
                else :
                    self.out[i] = self.functional.eval(numpy.ma.MaskedArray(sol,self.mask))


    # Call GPU solver
    def runEikonal(self,Y,start,end):
        for i in range(start,end):
        
            # Inform user that we are trying to load the file
            if self.reuse==True:
                filename = "{dir_output}/{fname_acttimes}.igb".format(**self.propeiko.params)
                print "\nAttempting to load " + filename
                try:
                    dtime = igb_read(filename)
                except:
                    print "Load failed"
                    self.reuse = False
            
            # Compute new solution
            if self.reuse==False:
                pickle_name = 'propeiko_{0}.pkl'.format(i)
                subprocess.call("srun -p gpu python ../Models/EikonalCUDA.py {0}".format(pickle_name),shell=True)


    # Load files with parameters info
    def loadFiles(self):
        filenameA = "{dir_input}/{fname_alpha}.igb".format(**self.propeiko.params)
        filenameC = "{dir_input}/{fname_cell}.igb".format(**self.propeiko.params)
        filenameP = "{dir_input}/{fname_phi}.igb".format(**self.propeiko.params)
        filenameG = "{dir_input}/{fname_gamma}.igb".format(**self.propeiko.params)
        hdrP = igb_read(filenameP,only_header=True)
        hdrG = igb_read(filenameG,only_header=True)
        hdrA = igb_read(filenameA,only_header=True)
        self.cells  = igb_read(filenameC)
        self.alpha  = hdrA["facteur"]*igb_read(filenameA) + hdrA["zero"]
        self.phis   = hdrP["facteur"]*igb_read(filenameP) + hdrP["zero"]
        self.gammas = hdrG["facteur"]*igb_read(filenameG) + hdrG["zero"]


    # Precompute quantities that don't depend on alpha
    def precomputeLocalTensor(self,path):
        sc = numpy.zeros(path.shape[0])
        st = numpy.zeros(path.shape[0])
        sl = numpy.zeros(path.shape[0])
        alpha = numpy.zeros(path.shape[0])
        rho   = numpy.zeros(path.shape[0])
        phi   = numpy.zeros(path.shape[0])
        gamma = numpy.zeros(path.shape[0])
        beta  = numpy.zeros(path.shape[0])
        for i in range(path.shape[0]):
            alpha[i] = self.alpha[ tuple(path[i,:])]
            phi[i]   = self.phis[  tuple(path[i,:])]
            gamma[i] = self.gammas[tuple(path[i,:])]
            cell     =  self.cells[tuple(path[i,:])]
            for s in self.propeiko.params['substances']:
                for cellType in s.cells:
                    if cell==cellType :
                        t = s.theta
                        b = s.beta
                        sic = s.sigma_ic
                        sit = s.sigma_it
                        sil = s.sigma_il
                        sec = s.sigma_ec
                        set = s.sigma_et
                        sel = s.sigma_el
                        rho2 = s.rho2
            if rho2<0 : rho2 = t*t/b
            if sic<0 : sic = sit
            if sec<0 : sec = set
            # harmonic mean of extracellular and intracellular conductivities,
            # then 1/sqrt(sigma)
            rho[i]=numpy.sqrt(rho2)
            sc[i]=numpy.sqrt((sic+sec)/(sic*sec))
            st[i]=numpy.sqrt((sit+set)/(sit*set))
            sl[i]=numpy.sqrt((sil+sel)/(sil*sel))
            beta[i]=b
        # Precompute vectorized quantities
        cosp=numpy.cos(phi);  sinp=numpy.sin(phi)
        cosg=numpy.cos(gamma);sing=numpy.sin(gamma)
        cosa=numpy.cos(alpha);sina=numpy.sin(alpha)
        return (beta,sc,st,sl,cosp,cosg,cosa,sinp,sing,sina,rho)


    # Computes the local velocity tensor from the fibers
    def computeTensor(self,prec,path):
        (beta,sc,st,sl,cosp,cosg,cosa,sinp,sing,sina,rho) = prec
        
        if self.name == 'theta':
            theta = numpy.zeros(path.shape[0])
            for i in range(path.shape[0]):
                theta[i] = self.var[tuple(path[i,:])]
            rho = theta/numpy.sqrt(beta)
    
        if self.name == 'alpha':
            alpha = numpy.zeros(path.shape[0])
            for i in range(path.shape[0]):
                alpha[i] = self.var[tuple(path[i,:])]
            cosa=numpy.cos(alpha);sina=numpy.sin(alpha)
    
        sc = sc/rho
        st = st/rho
        sl = sl/rho
        
        # set the velocity tensor
        self.gxx = sc*numpy.square(cosp*sing)+\
                   sl*numpy.square(cosg*cosp*sina+cosa*sinp)+\
                   st*numpy.square(cosa*cosg*cosp-sina*sinp)
        self.gyy = sc*numpy.square(sing*sinp)+\
                   sl*numpy.square(cosa*cosp-cosg*sina*sinp)+\
                   st*numpy.square(cosp*sina+cosa*cosg*sinp)
        self.gzz = sc*numpy.square(cosg)+numpy.square(sing)*(sl*numpy.square(sina)+st*numpy.square(cosa))
        self.gxy = sc*cosp*numpy.square(sing)*sinp+\
                   sl*(cosg*cosp*sina+cosa*sinp)*(cosg*sina*sinp-cosa*cosp)+\
                   st*(cosp*sina+cosa*cosg*sinp)*(cosa*cosg*cosp-sina*sinp)
        self.gxz = sing*(sc*cosg*cosp-\
                   sl*sina*(cosg*cosp*sina+cosa*sinp)+\
                   st*cosa*(sina*sinp-cosa*cosg*cosp));
        self.gyz = sing*(sc*cosg*sinp+\
                   sl*sina*(cosa*cosp-cosg*sina*sinp)-\
                   st*cosa*(sina*cosp+cosa*cosg*sinp));


    # Computes the local velocity tensor from the fibers
    def computeLocalTensor(self,pos,i):
        g = numpy.zeros((3,3))
        g[2,2] = self.gxx[i]
        g[1,1] = self.gyy[i]
        g[0,0] = self.gzz[i]
        g[2,1] = self.gxy[i]
        g[2,0] = self.gxz[i]
        g[1,0] = self.gyz[i]
        g[1,2] = g[2,1]
        g[0,2] = g[2,0]
        g[0,1] = g[1,0]
        return g


###############################################################################
# This is a script launching Propeiko by loading a pickle with all options
###############################################################################

if __name__ == "__main__" :
    pickle_in = '../../input/{0}'.format(sys.argv[1])
    pickle_out = '../../output/{0}'.format(sys.argv[1])
    with open (pickle_in,'rb') as input:
        propeiko = pickle.load(input)
        print "Computing solution {0}".format(pickle_in)
        propeiko.solve()
        dtime = propeiko.get_solution()
        with open(pickle_out,'wb') as output:
            pickle.dump(dtime,output,pickle.HIGHEST_PROTOCOL)












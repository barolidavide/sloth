
# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import numpy
import skfmm


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class FastMarching:
    
    def __init__(self): pass
    
    def loadFiles(self): pass

    def findSolution(self,phi,h):
        f = skfmm.distance(phi,dx=h)
        return f




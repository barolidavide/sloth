import numpy

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class Dummy:
    
    def __init__(self,functional): self.functional = functional
    
    def preprocess(self,l,k): pass
    def postprocess(self,l,k): pass
    
    def findSolution(self,k):
                
        # Return a 2d array
        return numpy.transpose(numpy.array([k]))


# @file	test_scalar.cxx
# @brief	This file contains an implementation of a 1D Laplacian
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import numpy

###############################################################################
# This is the class taking care of the physics 
# (or of interfacing with external solvers)
###############################################################################
class Laplacian1D:
    
    def __init__(self,functional,level,type='deterministic',noise=1.0):
        self.functional = functional
        self.level = level
        self.type = type
        self.noise = noise
    
    def getDuplicate(self):
        return Laplacian1D(self.functional,self.level)
    
    def preprocess(self,k): pass
    def postprocess(self,k): pass
					
    def findSolution(self,k):
        
        nObs = numpy.asarray(k).shape[0]
        
        # Compute number of points
        nx = 2 * pow(2,self.level)
        
        # nx is the number of elements
        # therefore the number of interior vertices is nx-1
        
        # Create Laplacian matrix on [0,1]
        L = numpy.diag(numpy.ones(nx-2),-1) - \
          2*numpy.diag(numpy.ones(nx-1), 0) + \
		    numpy.diag(numpy.ones(nx-2), 1)
        
        # Create RHS
        h = 1.0/nx;
        b = 100*pow(h,2)*numpy.ones(nx-1)
        
        # Allocate output
        F = numpy.zeros((nObs,self.functional.getNumberOutputs()))
        
        for i in range(0,nObs):
            # Perform a simulation
            u = numpy.linalg.solve(L,k[i]*b)

            # Eval functional
            F[i,:] = self.functional.eval(u,nx)
        
            # Add noise
            if self.type == 'noisy': F[i,:] += numpy.random.normal(0.,self.noise)
        
        # Return result
        return F



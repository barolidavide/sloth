
# @file	ShortColumn.py
# @brief	This file contains an implementation of the ShortColumn function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	23 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics 
# (or of interfacing with external solvers)
###############################################################################
class ShortColumn:
    
    def __init__(self,functional,level):
        self.functional = functional
        self.level = level
    
    def getDuplicate(self):
        return ShortColumn(self.functional,self.level)
    
    def preprocess(self,k): pass
    def postprocess(self,k): pass
					
    def findSolution(self,k):
        nObs = numpy.asarray(k).shape[0]
        
        # Allocate output
        F = numpy.zeros((nObs,self.functional.getNumberOutputs()))
        
        # Loop over samples
        for i in range(0,nObs):
        
            Z = k[i,:]
        
            if self.level == 0:
                u = 1.0 - 4.0*Z[3]/(Z[0]*(Z[1]**2.)*Z[2]) - (           Z[4]/(Z[0]*Z[1]*Z[2]))**2.
            elif self.level == 1:
                u = 1.0 -     Z[3]/(Z[0]*(Z[1]**2.)*Z[2]) - (           Z[4]/(Z[0]*Z[1]*Z[2]))**2.
            elif self.level == 2:
                u = 1.0 -     Z[3]/(Z[0]*(Z[1]**2.)*Z[2]) - ((1.0+Z[3])*Z[4]/(Z[0]*Z[1]*Z[2]))**2.
            elif self.level == 3:
                u = 1.0 -     Z[3]/(Z[0]*(Z[1]**2.)*Z[2]) - ((1.0+Z[3])*Z[4]/(     Z[1]*Z[2]))**2.

            F[i,:] = u
        
        # Return result
        return self.functional.eval(F)



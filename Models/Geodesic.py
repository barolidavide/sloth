
# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import numpy
from numpy.linalg import norm


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class Geodesic:

    def __init__(self,h,dim=2,xF=[90,60],model=0):
        self.h = h
        self.G = 200
        self.dim = dim
        self.xF = xF
        self.model = model
        if model!=0: self.model.loadFiles()

    
    # Initialize path
    def initPath(self,f):
        print "Computing geodesic.."
        f = self.model.findSolution(numpy.zeros([1,self.model.field.m]),'full')[0]
        f[self.model.getMask()] = 100000
        if      self.dim==2 : return self.computePath(f)
        elif    self.dim==3 : return self.computePath3D(f)
        else           : print "Dimension not supported!"
    
    
    # Useful for plotting
    def getPath(self): return self.path
    

    # Computes the 2D geodesic using a greedy algorithm
    def computePath(self,f):
        self.path = numpy.zeros((self.G,2)).astype(int)
        self.path[0,:] = numpy.asarray(self.xF).astype(int)
        self.L = numpy.zeros(self.G-1)
        pNew = numpy.zeros((8,2)).astype(int)
        lNew = numpy.zeros(8)
        fNew = numpy.zeros(8)
        move = [-1,+1,0]
        for i in range(self.G-1):
            p = self.path[i,:]
            k = 0
            for x in move:
                for y in move:
                    if k<8:
                        pNew[k,:] = [p[0]+x,p[1]+y]
                        lNew[k] = numpy.sqrt(abs(x)+abs(y))
                        fNew[k] = (f[tuple(pNew[k,:])]-f[tuple(p)])/lNew[k]
                    k = k+1
            j = numpy.argmin(fNew)
            if fNew[j]>=0 : break
            self.path[i+1,:] = pNew[j]
            self.L[i] = lNew[j]
        self.path = numpy.delete(self.path,range(i+1,self.G),axis=0)
        return self.path


    # Computes the 3D geodesic using a greedy algorithm
    def computePath3D(self,f):
        self.path = numpy.zeros((self.G,3)).astype(int)
        self.path[0,:] = numpy.asarray(self.xF).astype(int)
        self.L = numpy.zeros(self.G-1)
        pNew = numpy.zeros((26,3)).astype(int)
        lNew = numpy.zeros(26)
        fNew = numpy.zeros(26)
        move = [-1,+1,0]
        for i in range(self.G-1):
            p = self.path[i,:]
            k = 0
            for x in move:
                for y in move:
                    for z in move:
                        if k<26:
                            pNew[k,:] = [p[0]+x,p[1]+y,p[2]+z]
                            lNew[k] = numpy.sqrt(abs(x)+abs(y)+abs(z))
                            fNew[k] = (f[tuple(pNew[k,:])]-f[tuple(p)])/lNew[k]
                        k = k+1
            j = numpy.argmin(fNew)
            if fNew[j]>=0 : break
            self.path[i+1,:] = pNew[j,:]
            self.L[i] = lNew[j]
        self.path = numpy.delete(self.path,range(i+1,self.G),axis=0)
        return self.path


    # Computes the 2D geodesic using a greedy algorithm based on the gradient
    def computePathFromGradient(self,g):
        self.path = numpy.zeros((self.G,2)).astype(int)
        self.path[0,:] = numpy.asarray(self.xF).astype(int)
        self.L = numpy.zeros(self.G-1)
        pNew = numpy.zeros((8,2)).astype(int)
        for i in range(self.G-1):
            p = self.path[i,:]
            pNew[0,:] = [p[0]  ,p[1]+1]
            pNew[1,:] = [p[0]  ,p[1]-1]
            pNew[2,:] = [p[0]-1,p[1]  ]
            pNew[3,:] = [p[0]+1,p[1]  ]
            pNew[4,:] = [p[0]+1,p[1]+1]
            pNew[5,:] = [p[0]+1,p[1]-1]
            pNew[6,:] = [p[0]-1,p[1]-1]
            pNew[7,:] = [p[0]-1,p[1]+1]
            l = [1.,1.,1.,1.,numpy.sqrt(2.),numpy.sqrt(2.),numpy.sqrt(2.),numpy.sqrt(2.)]
            j = numpy.argmin([+g[1][tuple(p)]*numpy.sqrt(2.),
                              -g[1][tuple(p)]*numpy.sqrt(2.),
                              -g[0][tuple(p)]*numpy.sqrt(2.),
                              +g[0][tuple(p)]*numpy.sqrt(2.),
                              +g[0][tuple(p)] + g[1][tuple(p)],
                              +g[0][tuple(p)] - g[1][tuple(p)],
                              -g[0][tuple(p)] - g[1][tuple(p)],
                              -g[0][tuple(p)] + g[1][tuple(p)]])
            self.path[i+1,:] = pNew[j]
            self.L[i] = l[j]
            if fNew[j]>=0 : break
        return self.path


    # Computes the eigenvectors components that belong to the path
    def computeEigenvectors(self,path):
        v = numpy.zeros((path.shape[0],self.model.field.m))
        idx = numpy.zeros(tuple(self.model.field.phi.shape)).astype(int)
        for i in range(self.model.field.xi.shape[1]):
            idx[tuple(self.model.field.xi[:,i])] = i
        for i in range(path.shape[0]):
            v[i] = self.model.field.v[idx[tuple(path[i,:])],:]
        return v


    # Precompute some quantities of M
    def precomputeTensor(self,path): return self.model.precomputeLocalTensor(path)


    # Computes the 1D integral from fibers and geodesic
    def findSolution(self,Yi=0,path=0,tang=0,eigv=0,prec=0):
        if self.model==0:
            return numpy.sum(self.L)*self.h
        else:
            Yi = numpy.atleast_2d(Yi).T
            S = numpy.dot(eigv,Yi)
            self.model.var[ tuple(path.T[:,:])] =\
            self.model.var0[tuple(path.T[:,:])] + 0.5*S[:,0]
            self.model.computeTensor(prec,path)
            L = numpy.zeros(path.shape[0]-1)
            for i in range(path.shape[0]-1):
                M = self.model.computeLocalTensor(path[i,:],i)
                Mt = numpy.dot(M,tang[i,:])
                L[i] = numpy.sqrt(numpy.dot(Mt,Mt))
            return numpy.sum(L)*self.h


    # Computes the tangent to the geodesic
    def computeTangent(self,path):
        nVert = path.shape[0]
        nSegm = nVert - 1
        tNodes = numpy.zeros((nVert,3))
        tang   = numpy.zeros((nSegm,3))
        # Compute tangent at inner nodes
        for i in range(1,nVert-1):
            tNodes[i,:] = (path[i+1,:]-path[i-1,:])
            tNodes[i,:] = tNodes[i,:] / 2. #(1./(self.L[i]+self.L[i-1]))
        # Transfer to cells and scale by length
        for i in range(nSegm-1):
            tang[i,:] = tNodes[i+1,:]#*self.L[i]
        # Add final tangent
        tang[nSegm-1,:] = tNodes[nVert-2,:]#/norm(tNodes[nNodes-2,:] )
        tang[nSegm-1,:] = tang[nSegm-1,:]#*self.L[nCells-1]
        return tang





# @file	Ishigami.py
# @brief	This file contains an implementation of the Ishigami function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	10 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics 
# (or of interfacing with external solvers)
###############################################################################
class Ishigami:
    
    def __init__(self,functional,level):
        self.functional = functional
        self.level = level
    
    def getDuplicate(self):
        return Ishigami(self.functional,self.level)
    
    def preprocess(self,k): pass
    def postprocess(self,k): pass
    
    def findSolution(self,z): return self.functional.eval(self,z)
					
    def evalModel(self,k):
        nObs = numpy.asarray(k).shape[0]
        
        # Parameters
        a = 5.0
        b = 0.1
        
        # Allocate output
        F = numpy.zeros(nObs)
        
        # Loop over samples
        for i in range(0,nObs):
        
            Z = k[i,:]
        
            if self.level == 0:
                u = m.sin(Z[0]) + a*(m.sin(Z[1])**2.) + b*(Z[2]**4.)*m.sin(Z[0])
            elif self.level == 1:
                u = m.sin(Z[0]) + 0.95*a*(m.sin(Z[1])**2.) + b*(Z[2]**4.)*m.sin(Z[0])
            elif self.level == 2:
                u = m.sin(Z[0]) + 0.60*a*(m.sin(Z[1])**2.) + b*(Z[2]**2.)*m.sin(Z[0])*9.0
        
            F[i] = u
        
        # Return result
        return numpy.atleast_2d(F).T




# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
import csv
import scipy.io
import netCDF4


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class MOOSE:
    
    def __init__(self,functional,level,kernel_type='none'):
        self.functional = functional
        self.moose = whale_path
        self.level = level
        self.fine_mesh = "random_test"
        self.FTS = 0
        
        self.kernel_type = kernel_type
        self.batchSize = 16
    
        if level==0: self.num_tsteps = self.batchSize
        if level==1: self.num_tsteps = self.batchSize/4
        if level>=2: self.num_tsteps = self.batchSize/16
    
    
    def preprocess(self,k): pass
    
    
    def setFakeTimeStepping(self): self.FTS = 1
    
    
    def getNumberOfNodes(self):
        return int((self.N[0]+1)*(self.N[1]+1)*(self.N[2]+1))
    
    
    def getMassMatrix(self):
        i_file = "../Scripts/MassMatrix.i"
        cmd = "mpirun -n 1 {0} -i {1} Mesh/file=results/{2}.e".format(self.moose,i_file,self.fine_mesh)
        print cmd
        os.system(cmd)
        os.system("mv log_hessian.m results/log_hessian.txt")
        MassMatrix = scipy.io.mmread('results/log_hessian.txt').tocsr()
        print MassMatrix
        return (MassMatrix)


    def setRandomField(self, field):
        self.field = field


    def createMesh(self,N0,domain,name):
        self.name = name
        self.dim = N0.shape[0]
        self.N = numpy.zeros(3)
        self.N[0] = N0[0] * 2.0**self.level
        self.domain = numpy.zeros((3,2))
        self.domain[0,:] = domain[0,:]
        if self.dim>1 :
            self.N[1] = N0[1] * 2.0**self.level
            self.domain[1,:] = domain[1,:]
        if self.dim>2 :
            self.N[2] = N0[2] * 2.0**self.level
            self.domain[2,:] = domain[2,:]
        i_file = "../Scripts/getMesh.i"
        cmd = "mpirun -n 1 {0} -i {1} Mesh/dim={13} Mesh/xmin={6} Mesh/xmax={7} Mesh/ymin={8} Mesh/ymax={9} Mesh/zmin={10} Mesh/zmax={11} Mesh/nx={4} Mesh/ny={5} Mesh/nz={12} Outputs/file_base=results/{2} Executioner/end_time={3}".format(self.moose,i_file,name,self.batchSize,self.N[0],self.N[1],self.domain[0,0],self.domain[0,1],self.domain[1,0],self.domain[1,1],self.domain[2,0],self.domain[2,1],self.N[2],self.dim)
        print cmd
        os.system(cmd)
            
        
    def getCSV(self) : #def getCSV(self,name="random_field"):
        i_file = "../Scripts/getCSV.i"
        cmd = "mpirun -n 1 {0} -i {1} Mesh/file=results/{2}.e Outputs/file_base=results/{2}".format(self.moose,i_file,self.name)
        print cmd
        os.system(cmd)
        csvfile = open("results/{0}_nodal_0001.csv".format(self.name),'rb')
        reader = csv.reader(csvfile)
        row_count = sum(1 for row in reader)
        f = numpy.zeros([row_count-2,1])
        X = numpy.zeros([row_count-2,3])
        csvfile.seek(0)
        i = 0
        for row in reader:
            if len(row)>0 and i>0:
                X[i-1,0] = float(row[0])
                X[i-1,1] = float(row[1])
                X[i-1,2] = float(row[2])
                f[i-1] = float(row[4])
            i+=1
        csvfile.close()
        return (f,X)


    def findSolution(self,Y):
        num_samp = Y.shape[0]
        num_runs = num_samp / self.num_tsteps
        mod_runs = num_samp % self.num_tsteps
        
        if mod_runs>0 : num_runs = num_runs + 1

        mean_on_fine = numpy.zeros((num_samp,self.field.N))
        output = numpy.zeros((num_samp,self.functional.nP))
        
        print "Number of samples is {0} on level {1}".format(num_samp,self.level)

        for i in range(num_runs):
            i_start = self.num_tsteps*i

            if i<num_runs-1 or mod_runs==0:
                i_end = i_start + self.num_tsteps
            else:
                i_end = i_start + mod_runs

            print "Samples from {0} to {1}".format(i_start,i_end)

            y = Y[i_start:i_end,:]
            mean_on_fine[i_start:i_end,:] = self.runBatch(y)
        
        for i in range(mean_on_fine.shape[0]):
            output[i] = self.functional.eval(mean_on_fine[i,:],self.field.N)
        
        return output
    
    
    def runBatch(self,Y):



        num_samp = Y.shape[0]
        path_to_file = "results/" + self.fine_mesh + ".e"  # 'results/random_field.e'
        nc = netCDF4.Dataset(path_to_file,'r+')

        # Take one sample at the time
        for k in range(num_samp):
            (X,phi) = self.field.sample(Y[k,:])
            phi = 0.01*phi # for now, just to avoid negative diffusion values
            
            nc.variables["vals_nod_var1"][k]=phi
            k += 1
        
        nc.close()
        
        i_file = self.kernel_type + ".i"
        
        if (self.FTS) :
            cmd = "mpirun -n 4 {0} -i {1} Mesh/file=results/{3}.e UserObjects/KL/mesh=results/{4}.e AuxKernels/MonteCarloMean/num_samples={2} AuxKernels/Variance/num_samples={2} Executioner/end_time={2}".format(self.moose,i_file,num_samp,self.name,self.fine_mesh)
        
        if (self.FTS==0) :
            cmd = "mpirun -n 4 {0} -i {1} Mesh/file=results/{2}.e UserObjects/KL/mesh=results/{3}_field.e".format(self.moose,i_file,self.name,self.fine_mesh)

        print cmd
        os.system(cmd)
        os.system("mv ../Scripts/{0}_out.e results/MC_out.e".format(self.kernel_type))

        i_file = "../Scripts/getSolution.i"

        if (self.FTS) :
            cmd = "mpirun -n 1 {0} -i {1} Mesh/file=results/{3}.e UserObjects/MC_mean_on_coarse_mesh/mesh=results/MC_out.e Executioner/end_time={2}".format(self.moose,i_file,num_samp,self.fine_mesh)

        if (self.FTS==0) :
            cmd = "mpirun -n 1 {0} -i {1} Mesh/file=results/{2}.e UserObjects/MC_mean_on_coarse_mesh/mesh=results/MC_out.e ".format(self.moose,i_file,self.fine_mesh)

        print cmd
        os.system(cmd)
        nc = netCDF4.Dataset('../Scripts/getSolution_out.e','r+')
        #mean_on_fine[run_number,:] = nc.variables["vals_nod_var2"][num_samp - 1]
        
        return numpy.asarray(nc.variables["vals_nod_var2"])



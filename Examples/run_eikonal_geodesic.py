
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import argparse
import sys
sys.path.append('../Scripts')
from config import *
from ActivationSite import *
from Geodesic import *
from Reduced1D import *
from FastMarching import *
from EikonalCUDA import *


###############################################################################
# Check what type of uncertainty we need to create
###############################################################################

parser = argparse.ArgumentParser(description='Run with one of the following options: angle, cond')
parser.add_argument("type")
args = parser.parse_args()

if args.type=="alpha":
    patientFile = '../../input/CRT001j01.par.pickle'
elif args.type=="theta":
    patientFile = '../../input/CRT001j02.par.pickle'
else:
    print "Argument unknown!"
    exit()

###############################################################################
# Constructors
###############################################################################

# Create models
modelHi = EikonalCUDA(args.type)
regression = GPR()
sigma = 20

###############################################################################
# Prepare hi-fidelity
###############################################################################

# Initialize hi-fidelity model
modelHi.loadPatient(patientFile)
modelHi.setActivationSite(21,22,106)
alpha0Hi = modelHi.getMean()

# Create hi-fidelity geometry
XHi = modelHi.getMesh()
h = modelHi.getSpacing()

print "95%% correlation area is %d cells^2" % round(4*numpy.sqrt(sigma)/h)**2

# Create hi-fidelity mask
holeHi = True
maskHi = modelHi.getMask()

# Create hi-fidelity random field
dist = FastMarching()
fieldHi = SampleFieldVoxel(sigma,dist,holeHi,XHi,h,maskHi)
modelHi.setRandomField(fieldHi)

###############################################################################
# Prepare lo-fidelity
###############################################################################

modelLo = Geodesic(h,3,[104,35,103],modelHi)
nGeo = 1
redModel = Reduced1D(modelLo,nGeo)

###############################################################################
# Main routine running MLMC
###############################################################################

sampler = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)
samplerLe = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)
#samplerLe = SampleHalton(fieldHi.m,150)

# Create solver
solver = BMFMC(nMC=20000,nRE=50,nLE=nGeo)

# Set random variables
solver.addLoDistribution(sampler)
solver.addHiDistribution(samplerLe)

# Add models
solver.addHiFidelityModel('fine',modelHi)
solver.addLoFidelityModel('1d',redModel)

# Add regressions
solver.addRegression(regression)

# Add QoI
functional = ActivationSite(maskHi,104,35,103)
modelHi.functional = functional

# Run solver
(mu,sigma) = solver.run(sys.argv)

# Plot solution
functional.plotSolution()

print "The computed mean is %f" % mu
print "The estimated error is %f" % sigma


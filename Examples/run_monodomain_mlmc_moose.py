
# @file	run_monodomain_mlmc_moose.py
# @brief	This file performs adaptive MLMC with moose
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	16 November 2017

import sys
sys.path.append('../Scripts')
from config import *
from SampleFieldMesh import *
from MOOSE import *


###############################################################################
# Create random field
###############################################################################

sigma = 0.15 # correlation
NxHi = 20
NyHi = 120
lHi = 3
N = (NxHi+1)*(NyHi+1)

# Create high-fidelity model
functional = Pointwise(N)
modelHi = MOOSE(functional,lHi,NxHi,NyHi)
(c,X) = modelHi.getMesh(0,NxHi,NyHi)

# Get the mass matrix
MassMatrix = modelHi.getMassMatrix(NxHi,NyHi)

# Create random field
field = SampleFieldMesh(sigma,c,X,MassMatrix)
modelHi.setRandomField(field)


###############################################################################
# Execute MC with one sample
###############################################################################

(alpha,beta,gamma) = (2,4,2)
tol = 1.0e-2
solver = MLMC(tol)

# Add models
for l in range(3):
    modelLo = MOOSE(functional,l,NxHi,NyHi)
    modelLo.setRandomField(field)
    solver.addLoFidelityModel('coarse',modelLo)

# Add random number generation
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
solver.addDistribution(sampler)

# Set exponents
solver.setExponents(alpha,beta,gamma)

# Run MLMC
N0 = numpy.array([64,16,4]);
solver.run(sys.argv,N0)

print "MC executed successfully on Moose !! "

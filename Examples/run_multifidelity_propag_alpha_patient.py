
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import argparse
import sys
sys.path.append('../Scripts')
from config import *
from FastMarching import *
from Propag import *
from mpl_toolkits.mplot3d import axes3d
from pyccmc import igb_read,igb_write


###############################################################################
# Check what type of uncertainty we need to create
###############################################################################

parser = argparse.ArgumentParser(description='Run with one of the following options: alpha')
parser.add_argument("type")
args = parser.parse_args()

if args.type=="alpha":
    patientFile = 'CRT001'
    anatomyFile = 'crt001-19-heart'
else:
    print "Argument unknown!"
    exit()

###############################################################################
# Function that saves IGB files
###############################################################################
def save_igb(X,mask,input,name):
    fun = numpy.ma.MaskedArray(numpy.ones_like(X[0]),mask)
    idx = numpy.asarray(numpy.where(fun.mask==False))
    fun[tuple(idx[:,:])] = input
    fun.set_fill_value(numpy.nan)
    igb_write(numpy.swapaxes(fun,0,2),'/scratch/eikonal/results/CRT001/mfmc_patient_{}.igb'.format(name))

def save_slice(X,mask,input,name,fmin=-numpy.inf,fmax=numpy.inf):
    if fmin==-numpy.inf : fmin = numpy.nanmin(input)
    if fmax==+numpy.inf : fmax = numpy.nanmax(input)
    if abs(fmin-fmax)<1.0 : fmax = fmin+1.0
    fun = numpy.ma.MaskedArray(numpy.ones_like(X[0]),mask)
    idx = numpy.asarray(numpy.where(fun.mask==False))
    fun[tuple(idx[:,:])] = input
    fig,ax = plt.subplots(2,2)
    (Nx,Ny,Nz) = fun.shape
    x = numpy.linspace(0,1,Nx)
    y = numpy.linspace(0,1,Ny)
    z = numpy.linspace(0,1,Nz)
    X = numpy.meshgrid(x,y,z,indexing='xy')
    slice = numpy.linspace(0.45*Nz,0.85*Nz,4).reshape(2,2)
    for i in range(2):
        for j in range(2):
            f = numpy.ma.masked_invalid(fun[:,:,slice[i,j]].transpose()) # Mask mismatch??
            #p = ax[i,j].pcolormesh(X[0][:,:,slice[i,j]],X[1][:,:,slice[i,j]],f, vmin=fmin, vmax=fmax)
            p = ax[i,j].imshow(f, vmin=fmin, vmax=fmax)
            ax[i,j].axis('off')
    fig.colorbar(p, ax=ax.ravel().tolist())
    #plt.tight_layout()
    plt.savefig('results/mfmc_patient_{}.png'.format(name))
    plt.close()

###############################################################################
# Constructors
###############################################################################

# Create models
modelHi = Propag('propag',functional='full',reuse=True,dx=0.02)
modelLo = Propag('propeiko',functional='full',reuse=True,dx=0.02)
model1mm = Propag('propeiko',functional='full',reuse=True,dx=0.1)

###############################################################################
# Prepare hi-fidelity
###############################################################################

sigma = 10

# Initialize models
modelHi.loadPatient(patientFile,anatomyFile)
modelLo.loadPatient(patientFile,anatomyFile)
model1mm.loadPatient(patientFile,anatomyFile+'1mm')

# Set ouput meshes
modelHi.setOutputMesh(model1mm.getMesh(),model1mm.getMask())
modelLo.setOutputMesh(model1mm.getMesh(),model1mm.getMask())

# Create hi-fidelity geometry
X = model1mm.getMesh()
h = model1mm.getSpacing()

# Create hi-fidelity mask
hole = True
mask = model1mm.getMask()

# Create hi-fidelity random field
dist = FastMarching()

if os.path.isfile('pickles/field.pkl'):
    with open('pickles/field.pkl','rb') as f: field = pickle.load(f)
else:
    field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)
    with open('pickles/field.pkl','wb') as f: pickle.dump(field,f,pickle.HIGHEST_PROTOCOL)

# Create sampler
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)

# Set random fields
modelHi.setRandomField(field)
modelLo.setRandomField(field)
model1mm.setRandomField(field)

###############################################################################
# Main routine running MLMC
###############################################################################

# Add QoI
functional = Pointwise(modelHi.nDof())
modelHi.functional = functional
modelLo.functional = functional
model1mm.functional = functional

# Create solver
solver = MFMC(tol=0.75,nSigma=20)

# Set random variables
solver.addDistribution(sampler)

# Add models
solver.addHiFidelityModel('fine'  ,modelHi)
solver.addLoFidelityModel('coarse',model1mm)
solver.addLoFidelityModel('coarse',modelLo)

# Get model evaluations
solver.estimateSigma()
solver.omega[0,0] = 1350*8
solver.omega[0,1] = 140
solver.omega[0,2] = 1.0
(a,m,e) = solver.parametersGivenTolerance(1)

# Save IGB files
for i in range(solver.m.shape[1]):
    save_igb(X,model1mm.getMask(),solver.rho[  :,i],'r{}'.format(i))
    save_igb(X,model1mm.getMask(),solver.sigma[:,i],'s{}'.format(i))
    save_igb(X,model1mm.getMask(),solver.m[    :,i],'m{}'.format(i))
    print "Maximum model calls for model {} is {}".format(i,numpy.nanmax(solver.m[:,i]))
    save_slice(X,model1mm.getMask(),solver.rho[  :,i],'r{}'.format(i),-1,1)
    save_slice(X,model1mm.getMask(),solver.sigma[:,i],'s{}'.format(i))
    save_slice(X,model1mm.getMask(),solver.m[    :,i],'m{}'.format(i))

print "\033[92m"
print "Estimated RMSE is {}".format(numpy.sqrt(numpy.nanmean(solver.e[:,0])))
print "\033[0m"

# Re-use all samples
#solver.m[:,0] = 1.5*solver.m[:,0]
#solver.m[:,1] = 1.5*solver.m[:,1]
#solver.m[:,2] = 1.5*solver.m[:,2]

# Run solver
(P,V,Pl) = solver.run(sys.argv,1)

print "\033[92m"
print "Computed RMSE with 1 model is {}".format(numpy.sqrt(numpy.nanmean(V[0,:])))
print "Computed RMSE with 2 models is {}".format(numpy.sqrt(numpy.nanmean(V[1,:])))
print "Computed RMSE with 3 models is {}".format(numpy.sqrt(numpy.nanmean(V[2,:])))
print "\033[0m"

# Save IGB files
save_igb(X,model1mm.getMask(),P,'mean')
save_igb(X,model1mm.getMask(),Pl[0,:],'mean_propag')
save_igb(X,model1mm.getMask(),Pl[1,:],'mean_eikonal')
save_igb(X,model1mm.getMask(),Pl[2,:],'mean_eikonal_1mm')
save_igb(X,model1mm.getMask(),numpy.sqrt(solver.e[:,0]),'mean_error')

save_slice(X,model1mm.getMask(),P,'mean')
save_slice(X,model1mm.getMask(),Pl[0,:],'mean_propag')
save_slice(X,model1mm.getMask(),Pl[1,:],'mean_eikonal')
save_slice(X,model1mm.getMask(),Pl[2,:],'mean_eikonal_1mm')
save_slice(X,model1mm.getMask(),numpy.sqrt(solver.e[:,0]),'mean_error',0.0,6)

for i in range(solver.m.shape[1]):
    save_igb(  X,model1mm.getMask(),numpy.sqrt(V[i,:]),'e{}'.format(i))
    save_slice(X,model1mm.getMask(),numpy.sqrt(V[i,:]),'e{}'.format(i),0.0,6)

exit()

###############################################################################
# Estimate the second moment re-using the same samples
###############################################################################

# Add QoI
functional = Variance(modelHi.nDof())
modelHi.functional = functional
modelLo.functional = functional
model1mm.functional = functional

# Reset solver
solver.tol = 3.5**2
solver.estimated = False
solver.parameters = False

# Get model evaluations
solver.estimateSigma()
solver.omega[0,0] = 1350*8
solver.omega[0,1] = 140
solver.omega[0,2] = 1.5
(a2,m2,e2) = solver.parametersGivenTolerance(1)

# Save IGB files
for i in range(solver.m.shape[1]):
    save_igb(X,model1mm.getMask(),solver.rho[  :,i],'variance_r{}'.format(i))
    save_igb(X,model1mm.getMask(),solver.sigma[:,i],'variance_s{}'.format(i))
    save_igb(X,model1mm.getMask(),solver.m[    :,i],'variance_m{}'.format(i))
    print "Maximum model calls for model {} is {}".format(i,numpy.nanmax(solver.m[:,i]))
    save_slice(X,model1mm.getMask(),solver.rho[  :,i],'variance_r{}'.format(i),-1,1)
    save_slice(X,model1mm.getMask(),solver.sigma[:,i],'variance_s{}'.format(i))
    save_slice(X,model1mm.getMask(),solver.m[    :,i],'variance_m{}'.format(i))

print "\033[92m"
print "Estimated RMSE is {}".format(numpy.nanmean(numpy.sqrt(numpy.sqrt(solver.e[:,0]))))
print "\033[0m"

# Run solver
(P2,V2,Pl2) = solver.run(sys.argv,1)

print "\033[92m"
print "Computed RMSE with 1 model is {}".format(numpy.nanmean(numpy.sqrt(numpy.sqrt(V2[0,:]))))
print "Computed RMSE with 2 models is {}".format(numpy.nanmean(numpy.sqrt(numpy.sqrt(V2[1,:]))))
print "Computed RMSE with 2 models is {}".format(numpy.nanmean(numpy.sqrt(numpy.sqrt(V2[2,:]))))
print "\033[0m"

# Save IGB files
save_igb(X,model1mm.getMask(),numpy.sqrt(P2),'variance')
save_igb(X,model1mm.getMask(),numpy.sqrt(Pl2[0,:]),'variance_propag')
save_igb(X,model1mm.getMask(),numpy.sqrt(Pl2[1,:]),'variance_eikonal')
save_igb(X,model1mm.getMask(),numpy.sqrt(Pl2[2,:]),'variance_eikonal_1mm')
save_igb(X,model1mm.getMask(),numpy.sqrt(numpy.sqrt(solver.e[:,0])),'variance_error')

save_slice(X,model1mm.getMask(),numpy.sqrt(P2),'variance')
save_slice(X,model1mm.getMask(),numpy.sqrt(Pl2[0,:]),'variance_propag')
save_slice(X,model1mm.getMask(),numpy.sqrt(Pl2[1,:]),'variance_eikonal')
save_slice(X,model1mm.getMask(),numpy.sqrt(Pl2[2,:]),'variance_eikonal_1mm')
save_slice(X,model1mm.getMask(),numpy.sqrt(numpy.sqrt(solver.e[:,0])),'variance_error')

for i in range(solver.m.shape[1]):
    save_igb(  X,model1mm.getMask(),numpy.sqrt(numpy.sqrt(V2[i,:])),'variance_e{}'.format(i))
    save_slice(X,model1mm.getMask(),numpy.sqrt(numpy.sqrt(V2[i,:])),'variance_e{}'.format(i))




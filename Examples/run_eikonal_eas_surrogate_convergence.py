
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *
from EikonalEAS_SP import Model
from GPR import GPR
from SampleNormal import Distribution as SamplerNormal
from SampleBoundary import Distribution as SamplerBoundary
from Scalar import Functional
import matplotlib
import matplotlib.pyplot as plt


###############################################################################
# Main routine
###############################################################################

# Parameters
nLE = [25,50,100,200]
err = []
mu_vec = []

# Create model
functional = Functional()
model = Model(functional)
regression = GPR()
surrogate = GPR()
samplerBoundary = SamplerBoundary()
samplerNormal = SamplerNormal([24.,46.,104.],numpy.diag([2.,2.,2.]),3)

# Create solver
solver = BMFMC()

# Set random variables
solver.addLoDistribution(samplerNormal)
solver.addHiDistribution(samplerBoundary)

# Add models
solver.addHiFidelityModel('fine',model)
solver.addLoFidelityModel('surrogate',surrogate)

# Add regressions
solver.addRegression(regression)

# Main loop
for nS in nLE :
    solver.nLE = nS
    (mu,sigma) = solver.run(sys.argv)
    err = numpy.append(err,sigma)
    mu_vec = numpy.append(mu_vec,mu)
    print "The computed mean is %f" % mu
    print "The computed sigma is %f" % sigma
    del solver.modelLo[0]['Y']

# Convergence rate
for i in range(1,err.size):
    rate = numpy.log(err[i-1]/err[i])/numpy.log(2.0)
    print 'Convergence rate: %f' % rate

# Plot
fig = plt.figure()
plt.plot(nLE,err,'-o')
plt.grid()
plt.xlabel('N')
plt.title('MSE')
plt.savefig('convergence_eikonal.png')

fig = plt.figure()
plt.plot(nLE,mu_vec,'-o')
plt.grid()
plt.xlabel('N')
plt.title('Mean')
plt.savefig('mean_eikonal.png')




# @file	test_scalar.cxx
# @brief	This file runs MC with only high-fidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import argparse
import sys
sys.path.append('../Scripts')
from config import *
from ActivationSite import *
from Geodesic import *
from Reduced1D import *
from FastMarching import *
from EikonalCUDA import *


###############################################################################
# Check what type of uncertainty we need to create
###############################################################################

parser = argparse.ArgumentParser(description='Run with one of the following options: angle, cond')
parser.add_argument("type")
args = parser.parse_args()

if args.type=="alpha":
    patientFile = '../../input/CRT001j01.par.pickle'
elif args.type=="theta":
    patientFile = '../../input/CRT001j02.par.pickle'
else:
    print "Argument unknown!"
    exit()

###############################################################################
# Constructors
###############################################################################

# Create models
modelHi = EikonalCUDA(args.type)
regression = GPR()
sigma = 40

###############################################################################
# Prepare hi-fidelity
###############################################################################

# Initialize hi-fidelity model
modelHi.loadPatient(patientFile)
modelHi.setActivationSite(21,22,106)
alpha0Hi = modelHi.getMean()

# Create hi-fidelity geometry
XHi = modelHi.getMesh()
h = modelHi.getSpacing()

# Create hi-fidelity mask
holeHi = True
maskHi = modelHi.getMask()

# Create hi-fidelity random field
dist = FastMarching()
fieldHi = SampleFieldVoxel(sigma,dist,holeHi,XHi,h,maskHi)
modelHi.setRandomField(fieldHi)

###############################################################################
# Prepare lo-fidelity
###############################################################################

###############################################################################
# Main routine running MLMC
###############################################################################

samplerHi = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)
samplerLo = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)

# Create solver
solver = BMFMC(nMC=10000,nRE=75,nLE=5)

# Set random variables
solver.addLoDistribution(samplerLo)
solver.addHiDistribution(samplerHi)

# Add models
solver.addHiFidelityModel('fine',modelHi)
solver.addLoFidelityModel('coarse',modelHi)

# Add regressions
solver.addRegression(regression)

# Add QoI
functional = ActivationSite(maskHi,104,35,103)
solver.addFunctional(functional)

# Run solver
(mu,sigma) = solver.run(sys.argv)

# Plot solution
functional.plotSolution()

print "The computed mean is %f" % mu
print "The estimated error is %f" % sigma


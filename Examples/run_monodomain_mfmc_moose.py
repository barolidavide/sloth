
# @file	run_monodomain_mlmc_moose.py
# @brief	This file performs adaptive MLMC with moose
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	16 November 2017

import sys
sys.path.append('../Scripts')
from config import *
from SampleFieldMesh import *
from MOOSE import *


###############################################################################
# Create random field
###############################################################################

sigma = 0.15 # correlation
NxHi = 20
NyHi = 120
lHi = 3
N = (NxHi+1)*(NyHi+1)

# Create high-fidelity model
functional = Energy()
modelHi = MOOSE(functional,lHi,NxHi,NyHi)
(c,X) = modelHi.getMesh(0,NxHi,NyHi)

# Get the mass matrix
MassMatrix = modelHi.getMassMatrix(NxHi,NyHi)

# Create random field
field = SampleFieldMesh(sigma,c,X,MassMatrix)
modelHi.setRandomField(field)


###############################################################################
# Execute MC with one sample
###############################################################################

# Create solver
solver = MFMC(5)

# Set high-fidelity model
solver.addHiFidelityModel('fine',modelHi)

# Add random number generation
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
solver.addDistribution(sampler)

# Add models
for l in range(lHi):
    modelLo = MOOSE(functional,l,NxHi,NyHi)
    modelLo.setRandomField(field)
    solver.addLoFidelityModel('coarse',modelLo)

# Run MFMC
solver.run(sys.argv,100)

print "MC executed successfully on Moose !! "

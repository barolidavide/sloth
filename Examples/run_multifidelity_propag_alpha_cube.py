
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import argparse
import sys
sys.path.append('../Scripts')
from config import *
from FastMarching import *
from Propag import *
from mpl_toolkits.mplot3d import axes3d


###############################################################################
# Check what type of uncertainty we need to create
###############################################################################

parser = argparse.ArgumentParser(description='Run with one of the following options: alpha')
parser.add_argument("type")
args = parser.parse_args()

if args.type=="alpha":
    patientFile = 'cube'
    anatomyFile = 'cube'
else:
    print "Argument unknown!"
    exit()

###############################################################################
# Constructors
###############################################################################

# Create models
modelHi = Propag('propag',functional='full',reuse=True,dx=0.02)
modelLo = Propag('propeiko',functional='full',reuse=True,dx=0.02)
model1mm = Propag('propeiko',functional='full',reuse=True,dx=0.1)

###############################################################################
# Prepare hi-fidelity
###############################################################################

sigma = 0.25

# Initialize models
modelHi.loadPatient(patientFile,anatomyFile)
modelLo.loadPatient(patientFile,anatomyFile)
model1mm.loadPatient(patientFile,anatomyFile+'_dx0.1_')

# Set output meshes
modelHi.setOutputMesh(model1mm.getMesh(),model1mm.getMask())
modelLo.setOutputMesh(model1mm.getMesh(),model1mm.getMask())

# Create hi-fidelity geometry
X = modelHi.getMesh()
h = modelHi.getSpacing()

print "95%% correlation area is %d cells^2" % round(4*numpy.sqrt(sigma)/h)**2

# Create hi-fidelity mask
hole = False
mask = modelHi.getMask()

# Create hi-fidelity random field
dist = FastMarching()
field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)

# Create sampler
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)

# Set random fields
modelHi.setRandomField(field)
modelLo.setRandomField(field)
model1mm.setRandomField(field)

###############################################################################
# Main routine running MLMC
###############################################################################

# Add QoI
functional = Pointwise(modelHi.nDof())
modelHi.functional = functional
modelLo.functional = functional
model1mm.functional = functional

# Create solver
solver = MFMC(tol=3e-1,nSigma=600)

# Set random variables
solver.addDistribution(sampler)

# Add models
solver.addHiFidelityModel('fine'  ,modelHi)
solver.addLoFidelityModel('coarse',model1mm)
solver.addLoFidelityModel('coarse',modelLo)

# Get model evaluations
solver.estimateSigma()
solver.omega[0,0] = 40
solver.omega[0,1] = 1.0
solver.omega[0,2] = 0.1
(a,m,e) = solver.parametersGivenTolerance(1)

# Run solver
(P,V,P1,P2) = solver.run(sys.argv,1)

s0 = solver.sigma[:,0]
s1 = solver.sigma[:,-1]

###############################################################################
# Do some plots
###############################################################################

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=solver.rho[:,1], depthshade=False, alpha=0.5, vmin=-1, vmax=1)
fig.colorbar(p)
plt.savefig('results/mfmc_rho1_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=solver.rho[:,2], depthshade=False, alpha=0.5, vmin=-1, vmax=1)
fig.colorbar(p)
plt.savefig('results/mfmc_rho2_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=P, depthshade=False, alpha=0.5)
fig.colorbar(p)
plt.savefig('results/mfmc_mean_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=P1, depthshade=False, alpha=0.5)
fig.colorbar(p)
plt.savefig('results/mfmc_mean_eikonal_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=P2, depthshade=False, alpha=0.5)
fig.colorbar(p)
plt.savefig('results/mfmc_mean_eikonal2_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=numpy.sqrt(V[-1,:]), depthshade=False, alpha=0.5)
fig.colorbar(p)
plt.savefig('results/mfmc_mean_error_600.png')
plt.close()

exit()

###############################################################################
# Estimate the second moment re-using the same samples
###############################################################################

# Add QoI
functional = Variance(modelHi.nDof())
modelHi.functional = functional
modelLo.functional = functional
model1mm.functional = functional

# Reset solver
solver.tol = 1.2
solver.estimated = False
solver.parameters = False

# Get model evaluations
solver.estimateSigma()
solver.omega[0,0] = 40
solver.omega[0,1] = 1.0
#solver.omega[0,2] = 0.1
(a2,m2,e2) = solver.parametersGivenTolerance(1)

# Check if we have the samples
if numpy.nanmax(m2[:,0])>600 or numpy.nanmax(m2[:,1])>4116:
    print "Too many evaluations (m={0},{1}) requested!".format(numpy.nanmax(m2[:,0]),numpy.nanmax(m2[:,1]))
    exit()

# Run solver
(P2,V2) = solver.run(sys.argv,1)

###############################################################################
# Do some plots
###############################################################################

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=m2[:,0], depthshade=False, alpha=0.5)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_evaluations_variance_propag_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=m2[:,1], depthshade=False, alpha=0.5)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_evaluations_variance_eikonal_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=solver.rho[:,1], depthshade=False, alpha=0.5, vmin=-1, vmax=1)
fig.colorbar(p)
plt.savefig('results/mfmc_variance_rho_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=(s0), depthshade=False, alpha=0.5, vmax=6)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_sigma_propag_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=(s1), depthshade=False, alpha=0.5, vmax=6)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_sigma_eikonal_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=numpy.sqrt(P2), depthshade=False, alpha=0.5, vmax=6)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_variance_600.png')
plt.close()

fig = plt.figure()
ax = fig.gca(projection='3d')
p = ax.scatter(X[0],X[1],X[2],c=numpy.sqrt(numpy.sqrt(V2[1,:])), depthshade=False, alpha=0.5)
ax.view_init(30, 45)
fig.colorbar(p)
plt.savefig('results/mfmc_variance_error_600.png')
plt.close()





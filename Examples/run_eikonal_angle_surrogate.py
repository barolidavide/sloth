
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import sys
sys.path.append('../Scripts')
from config import *
from EikonalCUDA import *
from POD import *
from ActivationSite import *
from FastMarching import *


###############################################################################
# Constructors
###############################################################################

# Create models
modelLo = GPR()
modelHi = EikonalCUDA('alpha')
regression = GPR()
sigma = 20

###############################################################################
# Prepare hi-fidelity
###############################################################################

# Initialize hi-fidelity model
modelHi.loadPatient('../../input/CRT001j01.par.pickle')
modelHi.setActivationSite(21,22,106)
alpha0Hi = modelHi.getMean()

# Create hi-fidelity geometry
XHi = modelHi.getMesh()
h = modelHi.getSpacing()

print "95%% correlation area is %d cells^2" % round(4*numpy.sqrt(sigma)/h)**2

# Create hi-fidelity mask
holeHi = True
maskHi = modelHi.getMask()

# Create hi-fidelity random field
dist = FastMarching()
fieldHi = SampleFieldVoxel(sigma,dist,holeHi,XHi,h,maskHi)
modelHi.setRandomField(fieldHi)

###############################################################################
# Prepare lo-fidelity
###############################################################################

###############################################################################
# Main routine running MLMC
###############################################################################

samplerHi = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)
samplerLo = SampleNormal(numpy.zeros(fieldHi.m),numpy.identity(fieldHi.m),fieldHi.m)
#samplerHi = SampleHalton(29,150)

# Create solver
solver = BMFMC(nMC=50000,nRE=100,nLE=150)

# Set random variables
solver.addLoDistribution(samplerLo)
solver.addHiDistribution(samplerHi)

# Add models
#modelLo2 = POD(functional)
solver.addHiFidelityModel('fine',modelHi)
solver.addLoFidelityModel('surrogate',modelLo)
#solver.addLoFidelityModel('pod',modelLo2)

# Add regressions
solver.addRegression(regression)

# Add QoI
functional = ActivationSite(maskHi,104,35,103)
solver.addFunctional(functional)

# Run solver
(mu,sigma) = solver.run(sys.argv)

# Plot solution
functional.plotSolution()

print "The computed mean is %f" % mu
print "The estimated error is %f" % sigma


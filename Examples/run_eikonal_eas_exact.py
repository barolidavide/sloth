
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import sys
sys.path.append('../Scripts')
from config import *
from EikonalEAS import Model
from GPR import *
from SampleBoundary import Distribution as SamplerBoundary
from Scalar import *

###############################################################################
# Main routine running MLMC
###############################################################################
'''Runs models for debugging.'''

# Create model
model = Model()
regression = GPR()
samplerBoundary = SamplerBoundary()
samplerNormal = SampleNormal([24.,46.,104.],numpy.diag([2.,2.,2.]),3)

# Create solver
solver = BMFMC(nMC=100,nRE=25,nLE=5)

# Set random variables
solver.addLoDistribution(samplerNormal)
solver.addHiDistribution(samplerBoundary)

# Add models
solver.addHiFidelityModel('fine',model)
solver.addLoFidelityModel('coarse',model)

# Add regressions
solver.addRegression(regression)

# Add QoI
functional = Scalar()
solver.addFunctional(functional)

# Run solver
(mu,sigma) = solver.run(sys.argv)

# Plot solution
functional.plotSolution()

print "The computed mean is %f" % mu
print "The estimated error is %f" % sigma



# @file	mlmc_eikonal_cube
# @brief	This file contains the model implementation to perform MLMC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	11 April 2016

import sys
sys.path.append('/home/quaglino/repo/ccmc-inverse/pypropeiko')
sys.path.append('/home/quaglino/repo/ccmc-inverse/uq')
sys.path.append('../Inverse')
import pydoc
import numpy
import scipy
from ABC import ABC
from EikonalCube import Model


###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Functional:
    
    def getNumberOutputs(self): return 1
    def evalFunctional(self,u,nx): return u



#####################################################################################################
# @brief Main routine setting up ABC
#####################################################################################################
'''Applies multilevel Monte Carlo to 1D Laplacian with a field output.\n\
    Input parameters include:\n\
    -method: mlmc or leave empty for debug'''

# Starting value
startValue = numpy.array([0.0]);

# Parameters
it = 100
burnIn = 50
nObs = 20
refinement = 1

# Create model
functional = Functional()
model = Model(functional)

# Create solver
solver = ABC(model,startValue)

# Create synthetic data
(data,dummy) = model.takeSamples(refinement,1.0,nObs)
solver.setData(data)

# Run solver
chain = solver.run(sys.argv,it)

# Plot histogram
plt.hist(chain[burnIn:it,0],bins=10)
plt.show()


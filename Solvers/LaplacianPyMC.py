
# @file	nonparPyMC.cxx
# @brief	This file implements a non parametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy
from scipy import special
import pymc

#####################################################################################################
# @brief Define class
#####################################################################################################
class NPR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): 
        self.ESS = 0.0
        self.w = 0.0
        self.l = 0
        self.s = 0
        self.g = 0
        self.a0 = 1.0
        self.b0 = 1.0	
        self.a = 2.0
        self.b = 1e-6
        self.a_tau = 1.0
        self.a_mu = 0.01
        self.k_max = 100
        self.s = 1.0
        self.gammaS = 1.0
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi):
        
        # Prior
        @pymc.stochastic(observed=False)
        def theta(value=numpy.zeros(3*4)):
            
            k = 4
            tau = value[0:k]
            aj = value[k:2*k]
            nu = value[2*k:3*k]
            
            c11 = numpy.power(1.0/(self.s+1.0),k+1.0)
        
            c21 = special.gamma(self.a_tau+1)/special.gamma(self.a_tau)
            c22 = pow(self.a_tau,self.a_tau)
            c23 = 1/self.a_mu
            c24 = 1.0
        
            for j in range(0,k):
                c22 = c22 * numpy.power(tau[j],1-self.a_tau)
                c24 = c24 * numpy.power(self.a_tau*tau[j]+1/self.a_mu,-(1+self.a_tau))
    
    
            c31 = numpy.power(1.0/(2.0+numpy.pi),(k+1.0)/2.0)
            c32 = special.gamma(self.a0+(k+1)/2)
            c33 = 1.0/numpy.power(self.b0 + 0.5 * numpy.sum(numpy.square(aj)),self.a0+(k+1.0)/2.0)
        
            term1 = c11
            term2 = c21 * c22 * c23 * c24
            term3 = c31 * c32 * c33
    
            return -numpy.log(term1*term2*term3)

        # Model
        @pymc.deterministic
        def mu(xi=xdata,theta=theta):
            yi = self.evalRegression(xi,theta)
            yOut = self.evalResidual(xi,theta)
            yOut[0]  = yi[0]
            yOut[-1] = yi[-1]
            return yOut
        
        # Likelihood
        tau = pymc.Gamma('tau', alpha=self.a, beta=self.b)
        y = pymc.Normal('y', mu, tau, observed=True, value=ydata)

        # package the full model in a dictionary
        model1 = dict(theta=theta, y=y)
			
        # Run the model
        (iter,burn) = (100000,50000)
        N = iter - burn
        S = pymc.MCMC(model1)
        S.sample(iter=iter, burn=burn)

        # Get output
        theta = S.trace('theta')[:]
        yi = numpy.zeros((N,xi.size))
        ri = numpy.zeros((N,xi.size))
        
        for i in range(0,N):
            ri[i,:] = self.evalResidual(xi,theta[i,:])
            yi[i,:] = self.evalRegression(xi,theta[i,:])
        
        ri[:, 0] = yi[:, 0]
        ri[:,-1] = yi[:,-1]
        
        # Return the output
        return (ri.mean(0),yi.mean(0))


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalResidual(self,xi,theta):
        k = 4
        tau = theta[0:k]
        aj = theta[k:2*k]
        nu = theta[2*k:3*k]
        
        # Initialize the output
        yi = numpy.zeros(xi.size)
								
        # Evaluate the basis functions at the nodes
        K = numpy.zeros((xi.size,k))
        
        for i in range(0,xi.size):
            for j in range(0,k) :
                dx  = -2*tau[j]*nu[j]*(xi[i]-nu[j])
                ddx = -2*tau[j]*nu[j]
                K[i,j] = (dx*dx+ddx)*numpy.exp(-tau[j]*(xi[i]-nu[j])*(xi[i]-nu[j]))
    
        # Evaluate the fit
        for j in range(0,k) : yi = yi + aj[j]*K[:,j]
        
        return yi-1


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi,theta):
        k = 4
        tau = theta[0:k]
        aj = theta[k:2*k]
        nu = theta[2*k:3*k]
    
        # Initialize the output
        yi = numpy.zeros(xi.size)
								
        # Evaluate the basis functions at the nodes
        K = numpy.zeros((xi.size,k))
    
        for i in range(0,xi.size):
            for j in range(0,k) :
                K[i,j] = numpy.exp(-tau[j]*(xi[i]-nu[j])*(xi[i]-nu[j]))
        
        # Evaluate the fit
        for j in range(0,k) : yi = yi + aj[j]*K[:,j]

        return yi


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."



# @file	nonparPyMC.cxx
# @brief	This file implements a solver for the 1D Laplacian using a nonparametric regression
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	4 August 2016
 
import numpy
from scipy import special
import pymc

#####################################################################################################
# @brief Define class
#####################################################################################################
class NPR:
    

    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self): 
        self.ESS = 0.0
        self.w = 0.0
        self.l = 0
        self.s = 0
        self.g = 0
        self.a0 = 1.0
        self.b0 = 1.0	
        self.a = 2.0
        self.b = 1e-6
        self.a_tau = 1.0
        self.a_mu = 0.01
        self.k_max = 100
        self.s = 1.0
        self.gammaS = 1.0
        self.k = 3
			

    #####################################################################################################
    # @brief Performs nonparametric regression
    #####################################################################################################
    def fit(self,xdata,ydata,xi):
        
        # Prior
        k = self.k
        
        lamdda = pymc.Exponential('lamdaa', self.s)
        mu_tau = pymc.Exponential('mu_tau', self.a_mu, size=k)
        sigma2 = pymc.InverseGamma('sigma', alpha=self.a0, beta=self.b0)
        
        #k  = pymc.Poisson('k', lamdda)
        tj = pymc.Gamma('tj', alpha=self.a_tau, beta=mu_tau*self.a_tau, size=k)
        aj = pymc.Normal('aj', 0.0, sigma2, size=k) # why not 1.0/sigma2?
        nu = pymc.Uniform('nu', 0.0, 1.0, size=k)

        # Model
        @pymc.deterministic
        def mu(xi=xdata,tj=tj,aj=aj,nu=nu):
            self.tj = tj
            self.aj = aj
            self.nu = nu
            yOut = self.evalResidual(xi)
            bc = numpy.zeros(2)
            for j in range(0,self.k) : bc[0] = bc[0] + aj[j]*self.K[ 0,j]
            for j in range(0,self.k) : bc[1] = bc[1] + aj[j]*self.K[-1,j]
            yOut[0]  = bc[0]
            yOut[-1] = bc[-1]
            return yOut
        
        # Likelihood
        sigma2Inv = pymc.Gamma('sigma2Inv', alpha=self.a, beta=self.b)
        y = pymc.Normal('y', mu, sigma2Inv, observed=True, value=ydata)

        # package the full model in a dictionary
        model1 = dict(tj=tj, aj=aj, nu=nu, y=y)
			
        # Run the model
        (iter,burn) = (15000,10000)
        N = iter - burn
        S = pymc.MCMC(model1)
        S.sample(iter=iter, burn=burn)

        # Get output
        tj = S.trace('tj')[:]
        aj = S.trace('aj')[:]
        nu = S.trace('nu')[:]
        yi = numpy.zeros((N,xi.size))
        ri = numpy.zeros((N,xi.size))
        
        for i in range(0,N):
            self.tj = tj[i,:]
            self.aj = aj[i,:]
            self.nu = nu[i,:]
            ri[i,:] = self.evalResidual(xi)
            yi[i,:] = self.evalRegression(xi)
        
        ri[:, 0] = yi[:, 0]
        ri[:,-1] = yi[:,-1]
        
        # Return the output
        return (ri.mean(0),yi.mean(0))


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalResidual(self,xi):

        k = self.k
        tau = self.tj
        aj = self.aj
        nu = self.nu
        
        # Initialize the output
        yi = numpy.zeros(xi.size)
        dK = numpy.zeros((xi.size,k))
        cj = numpy.zeros((xi.size,k))
        dx = numpy.zeros(k)
        
        # Evaluate basis
        self.evalBasis(xi)
								
        # Evaluate the basis functions at the nodes
        for j in range(0,k): dx[j] = -2*tau[j]*nu[j]
        for j in range(0,k): cj[:,j] = (dx[j]*self.R2[:,j]+1.0)*dx[j]
        for j in range(0,k): dK[:,j] = cj[:,j]*self.K[:,j]
    
        # Evaluate the fit
        for j in range(0,k): yi = yi + aj[j]*dK[:,j]
        
        return yi-0.5


    #####################################################################################################
    # @brief Evaluates the regression
    #####################################################################################################
    def evalRegression(self,xi):
        aj = self.aj

        # Initialize the output
        yi = numpy.zeros(xi.size)
								
        # Evaluate basis
        self.evalBasis(xi)
        
        # Evaluate the fit
        for j in range(0,self.k) : yi = yi + aj[j]*self.K[:,j]

        return yi


    #####################################################################################################
    # @brief Evaluates the basis functions
    #####################################################################################################
    def evalBasis(self,xi):
        self.evalR2(xi)
        self.K = numpy.zeros((xi.size,self.k))
        for j in range(0,self.k): self.K[:,j] = numpy.exp(-self.tj[j]*self.R2[:,j])


    #####################################################################################################
    # @brief Evaluates distance
    #####################################################################################################
    def evalR2(self,xi):
        self.R2 = numpy.zeros((xi.size,self.k))
        for j in range(0,self.k): self.R2[:,j] = numpy.square(xi-self.nu[j])


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


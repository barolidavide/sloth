
# @file	MCMC.cxx
# @brief	This file contains an implementation of MCMC rejection
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	11 April 2016
 
 
import numpy
import time

#####################################################################################################
# @brief Define class
#####################################################################################################
class MCMC:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,model,startValue):
        self.model = model
        self.startValue = startValue
        self.nPar = startValue.size
    
				
    #####################################################################################################
    # @brief Set the data
    #####################################################################################################
    def setData(self,data):
        self.nObs = data.shape[0]
        self.nOut = data.shape[1]
        self.meandata = numpy.mean(data,0)	
        self.standarddeviationdata = numpy.std(data,0)
    
    
    #####################################################################################################
    # @brief Set random variables
    #####################################################################################################
    def addDistribution(self,type,pars):
        try:
            self.Y
        except AttributeError:
            self.Y = []
        self.Y.append( {'type':type,'pars':pars} )
   

    #####################################################################################################
    # @brief Runs Metropolis MCMC
    #####################################################################################################
    def acceptance(self,par,nSamples):
			
	  # Compute random parameters
      for y in self.Y:
          yi = y['pars'];
          if( y['type']=='normal' ):
              if len(par)<2:
                  k = numpy.random.normal(par,1e-1,nSamples)
              else:
                  if par[1]<=0 : return False
                  k = numpy.random.normal(par[0],par[1],nSamples)
          else:
              print "ERROR: Distribution not supported!"
              exit()
        
      # Run simulations
      samples = self.model.findSolution(k)
     					
	  # If something did not make sense
      if( numpy.isnan(samples).any() ): return False
					
	  # comparison with the observed summary statistics
      diffmean = numpy.linalg.norm(numpy.mean(samples,0) - self.meandata)
      diffsd = numpy.linalg.norm(numpy.std(samples,0) - self.standarddeviationdata)
			
	  # Normalization
      diffmean = diffmean / numpy.linalg.norm(self.meandata)
      diffsd = diffsd / numpy.linalg.norm(self.standarddeviationdata)
					
	  # If accepted return True
      if( (diffmean < 0.1) and (diffsd < 0.2) ): return True
      else: return False


    #####################################################################################################
    # @brief Runs Metropolis MCMC
    #####################################################################################################
    def MCMC(self,iterations,nSamples):
					
        # Initialize chain
        chain = numpy.zeros((iterations+1,self.nPar))
        chain[0,:] = self.startValue
        stdDev = 0.7
 
        for i in range(0,iterations):
									
            # New proposal
            proposal = numpy.random.normal(0.0,stdDev,self.nPar)
            proposal = proposal +	 chain[i,:]
            
            # Check whether to accept
            if( self.acceptance(proposal,nSamples) ): chain[i+1,] = proposal
            else:                                     chain[i+1,] = chain[i,]
        
        return chain 

    
    #####################################################################################################
    # @brief Parse the input and run
    #####################################################################################################
    def run(self,input,iterations,nSamples=1000):
					
        #	Read in method to use
        if len(input)>1 :
            method_name = str(input[1])
        else:
            method_name = "mcmc"

        #	Call solver directly for debug
        if method_name == "run_fun_l":
            print("Doing a single run for debug")
            u = self.model.findSolution(1)
            print u

        #	Call MCMC
        else:
            print("Performing Metropolis MCMC")
            chain = self.MCMC(iterations,nSamples)
            return chain


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


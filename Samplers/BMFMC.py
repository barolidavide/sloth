
# @file	MFMC.cxx
# @brief	This file contains an implementation of multifidelity Monte Carlo
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 July 2016 

from Sampler import *

#####################################################################################################
# @brief Define class
#####################################################################################################
class BMFMC(Sampler):
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,nMC=50000,nRE=100,nLE=100):
        Sampler.__init__(self)
        self.nLE = nLE
        self.nRE = nRE
        self.nMC = nMC
        self.y = numpy.empty(0)


    #####################################################################################################
    # @brief Learn low-fidelity models
    #####################################################################################################
    def learnModels(self):
        for j,m in enumerate(self.modelLo) :
            if( m['type'] == 'surrogate' or
                m['type'] == 'pod' or
                m['type'] == '1d' ):
                
                try: m['Y']
                except KeyError:
                    print "Learning surrogate model.."
                
                    # Compute random parameters
                    m['Z'] = self.evalHiDistribution(self.nLE)
                    
                    # Generate high-fidelity samples
                    if( m['type'] == 'surrogate' ):
                        m['Y'] = self.evalHiModel(m['Z'])
                    elif( m['type'] == 'pod'):
                        m['Y'] = self.evalHiModel(m['Z'],solType='compressed')
                    elif( m['type'] == '1d' ):
                        m['Y'] = self.evalHiModel(m['Z'],solType='full')

                    # Fit model once
                    m['model'].fit(m['Z'],m['Y'])
            
    
    #####################################################################################################
    # @brief Computes the posterior
    #####################################################################################################
    def computePosterior(self,xi):
        
        print "\033[92m",
        print "Correlation using %d samples." % (self.nRE)
        print "Variance of low-fidelity is %e" % numpy.var(xi)
        
        # Train low-high correlation model
        yi = self.modelLo[0]['model'].findSolution(xi.T)
        sigma = self.modelLo[0]['model'].sigma
        #ySample = self.modelLo[0]['model'].regression.samplePosterior(xi.T)
        
        # Save data to file for postprocessing
        #file = open('results/output.pckl', 'wb')
        #pickle.dump(xi, file)
        #pickle.dump(yi, file)
        #pickle.dump(self.x, file)
        #pickle.dump(self.y, file)
        #pickle.dump(xPlot, file)
        #pickle.dump(yPlot, file)
        #pickle.dump(sigma,file)
        #pickle.dump(ySample, file)
        #file.close()

        #plt.figure()
        #plt.hist(ySample,100,histtype='stepfilled',color='0.75')
        #axes = plt.gca()
        #axes.set_xlim([100,150])
        #plt.title('mean = %f, std_dev = %f' % (numpy.mean(yi,0),numpy.mean(sigma)/numpy.mean(yi,0)),fontsize=16)
        #plt.savefig('results/histogram_'+str(self.nRE)+'.png')

        return (yi,sigma)


    #####################################################################################################
    # @brief Performs tests to plot correlation
    #####################################################################################################
    def mfmc(self):
        
        # Total number of samples
        nS = self.nMC

        # Learn models
        if( self.nLo > 0 ) : self.learnModels()

        # Compute random parameters
        zi = self.evalDistribution(nS,self.Y,self.nY)

        # Compute likelihood
        if( self.modelHi['type'] == 'none' ):   (yi,sigma) = (zi,0.0)
        else:                                   (yi,sigma) = self.computePosterior(zi.T)
        
        # Compute posterior
        mu = numpy.mean(yi,0)
        
        return (mu,numpy.mean(sigma)/mu)


    #####################################################################################################
    # @brief Parse the input and run
    #####################################################################################################
    def run(self,input):

        # Samples already taken
        try:
            self.k
        except AttributeError:
            self.k = numpy.zeros((0,self.nY))

        print("Performing multifidelity Monte Carlo.")

        exectime = time.time()					
        out = self.mfmc()
        exectime = time.time() - exectime
								
        print "\033[92m"
        print "Execution time %f seconds." % exectime 
        print "\033[0m"

        return out											

#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


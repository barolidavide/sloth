
# @file	Sampler.cxx
# @brief	This file contains the Sampler base clarr
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	15 November 2017

import matplotlib
import matplotlib.pyplot as plt
import pickle
import time
import numpy
from numpy import linalg

#####################################################################################################
# @brief Define class
#####################################################################################################
class Sampler:
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self):
        self.Y = []
        self.nY = 0
        self.modelHi = {'type':'none','model':'none'}
        self.modelLo = []
        self.nLo = 0

    #####################################################################################################
    # @brief Add random distribution
    #####################################################################################################
    def addDistribution(self,sampler,Y=0,nY=0):
        if Y==0: # called externally
            self.Y.append(sampler)
            self.nY = self.nY + sampler.n
        else: # called internally
            Y.append( sampler )
            nY = nY + sampler.n
            return (Y,nY)

    #####################################################################################################
    # @brief Sample from random distribution
    #####################################################################################################
    def evalDistribution(self,nS,Y,nY):
        zi = numpy.zeros((nS,nY))
        for p in Y:
            for n in range(nS):
                zi[n,:] = p.sample()
        return zi
    
    #####################################################################################################
    # @brief Computes the solution for the mean parameter
    #####################################################################################################
    def meanSolution(self):
        zi = numpy.zeros((1,self.nY))
        zi[0,:] = self.Y[0].mean()
        return self.evalHiModel(zi)
    
    #####################################################################################################
    # @brief Add high fidelity model
    #####################################################################################################
    def addHiFidelityModel(self,type,model):
        self.modelHi = {'type':type,'model':model}

    #####################################################################################################
    # @brief Add low fidelity model
    #####################################################################################################
    def addLoFidelityModel(self,type,model):
        self.modelLo.append( {'type':type,'model':model} )
        self.nLo = self.nLo + 1

    #####################################################################################################
    # @brief Sample the high fidelity model
    #####################################################################################################
    def evalHiModel(self,k,solType='other'):
        if solType=='compressed':
            yi = numpy.zeros((len(k),self.modelHi['model'].nDof()))
            for n,ki in enumerate(k): yi[n,:] = self.modelHi['model'].findSolution(ki).flatten().compressed()
        elif solType=='full':
            yi = self.modelHi['model'].findSolution(k,'full')
        else:
            yi = self.modelHi['model'].findSolution(k)
        return yi

    #####################################################################################################
    # @brief Eval all low fidelity models
    #####################################################################################################
    def evalLoModel(self,zi):
        x = numpy.zeros((len(zi),len(self.modelLo)))
        
        for i,m in enumerate(self.modelLo):

            if( m['type'] == 'surrogate' ):
                (sol,sigma) = m['model'].eval(zi)
                x[:,i] = sol[:,0]
            
            elif( m['type'] == 'pod' ):
                (sol,sigma) = m['model'].eval(zi)
                x[:,i] = sol
            
            elif( m['type'] == 'coarse' ):
                for n,ki in enumerate(zi): x[n,i] = m['model'].findSolution(ki)
            
            elif( m['type'] == '1d' ):
                for n,ki in enumerate(zi): x[n,i] = m['model'].eval(ki)
    
            elif( m['type'] == 'noisy' ):
                for n,ki in enumerate(zi): x[n,i] = m['model'].findSolution([ki]) + numpy.random.normal(0.,3.e-1)
            
            else: print "MODEL NOT SUPPORTED!!!"
        
        return x

#####################################################################################################
# End of class definition
#####################################################################################################
if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


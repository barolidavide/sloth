
# @file	SMC.cxx
# @brief	This file contains an implementation of SMC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	11 April 2016
 
 
import numpy
import time

#####################################################################################################
# @brief Define class
#####################################################################################################
class SMC:
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,model,startValue):
        self.model = model
    
				
    #####################################################################################################
    # @brief Set the data
    #####################################################################################################
    def setData(self,data):
        self.nObs = data.shape[0]
        self.nOut = data.shape[1]
        self.meandata = numpy.mean(data,0)	
        self.standarddeviationdata = numpy.std(data,0)
    
    
    #####################################################################################################
    # @brief Set random variables
    #####################################################################################################
    def addDistribution(self,sampler):
        try:
            self.Y
        except AttributeError:
            self.Y = []
            self.nY = 0
        self.Y.append(sampler)
        self.nY = self.nY + sampler.n


    #####################################################################################################
    # @brief Compute random variables
    #####################################################################################################
    def evalDistribution(self,nS):
    
        zi = numpy.zeros((nS,self.nY))
    
        # Take one sample at the time
        for n in range(0,nS):
        
            # Loop over all random variables
            for p in self.Y:
                zi[n,:] = p.sample()
        
        return zi


    #####################################################################################################
    # @brief Eval the likelihood
    #####################################################################################################
    def evalLikelihood(self,u):
        x = self.model.findSolution(u)
        L = 1
        for i in range(u.shape[0]):
            err = self.meandata - x[i,:]
            L = L*numpy.exp(-numpy.dot(err.T,err)/0.1)
            print err
        return L


    #####################################################################################################
    # @brief Runs Metropolis SMC
    #####################################################################################################
    def acceptance(self,par,nSamples):
			
	  # Random walk
      for y in self.Y:
          k = numpy.random.normal(par,1e-1,nSamples)
        
      # Run simulations
      samples = self.model.findSolution(k)
     					
	  # If something did not make sense
      if( numpy.isnan(samples).any() ): return False
					
	  # comparison with the observed summary statistics
      diffmean = numpy.linalg.norm(numpy.mean(samples,0) - self.meandata)
      diffsd = numpy.linalg.norm(numpy.std(samples,0) - self.standarddeviationdata)
			
	  # Normalization
      diffmean = diffmean / numpy.linalg.norm(self.meandata)
      diffsd = diffsd / numpy.linalg.norm(self.standarddeviationdata)
					
	  # If accepted return True
      if( (diffmean < 0.1) and (diffsd < 0.2) ): return True
      else: return False


    #####################################################################################################
    # @brief Runs MCMC kernel
    #####################################################################################################
    def kernel(self,startValue,iterations,nSamples):
					
        # Initialize chain
        chain = numpy.zeros((iterations+1,1))
        chain[0,:] = startValue
        stdDev = 0.7
 
        for i in range(0,iterations):
									
            # New proposal
            proposal = numpy.random.normal(0.0,stdDev,1)
            proposal = proposal +	 chain[i,:]
            
            # Check whether to accept
            if( self.acceptance(proposal,nSamples) ): chain[i+1,] = proposal
            else:                                     chain[i+1,] = chain[i,]
        
        # Check acceptance
        uniqueChain = numpy.unique(chain)
        acceptanceRatio = numpy.true_divide(uniqueChain.size-1,iterations)*100
        print "Acceptance ratio: %f %%" % acceptanceRatio
        
        # Return only final value
        return chain[iterations,:]


    #####################################################################################################
    # @brief Runs SMC
    #####################################################################################################
    def SMC(self,N,T):

        # Resampling tolerance
        tol = 1e-2

        # MCMC steps
        nMCMC = 5000/(N*T)

        # Initialization
        u = self.evalDistribution(N)
        w = numpy.ones([N,1])*1.0/N
        
        # Main loop
        for t in range(T):
            
            # Importance sampling
            w = w * self.evalLikelihood(u)

            # Compute ESS
            ESS = 1.0 / sum( w**2 )
            print "ESS is {0}".format(ESS[0])

            # Resampling
            if ESS < tol :
                print "Resampling"
                p = numpy.random.multinomial(1,w)
                u = u[p]
                w = numpy.ones([N,1])*1.0/N

            # Mutation
            for i in range(N): u[i] = self.kernel(u[i],nMCMC,5)

        # Final estimate
        x = self.model.findSolution(u)
        s = numpy.mean(x,0)
    
        return (u,s)


    #####################################################################################################
    # @brief Parse the input and run
    #####################################################################################################
    def run(self,input,T,N=1000):
					
        #	Read in method to use
        if len(input)>1 :
            method_name = str(input[1])
        else:
            method_name = "smc"

        #	Call solver directly for debug
        if method_name == "debug":
            print("Doing a single run for debug")
            (u,s) = self.model.findSolution(1)
            print (u,s)

        #	Call SMC
        else:
            print("Performing Sequential MC")
            (u,s) = self.SMC(N,T)
            return (u,s)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


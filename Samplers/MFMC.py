
# @file	MLMC.cxx
# @brief	This file contains an implementation of multifidelity Monte Carlo
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	15 November 2017

import sys
sys.path.append('../Scripts')
from config import *

from scipy.stats import spearmanr
from itertools import chain,combinations

#####################################################################################################
# @brief Define class
#####################################################################################################
class MFMC(MLMC):

    def __init__(self,tol=1e-3,nSigma=100):
        Sampler.__init__(self)
        self.nSigma = nSigma
        self.estimated = False
        self.ordered = False
        self.parameters = False
        self.tol = tol


    #####################################################################################################
    # @brief Model list ordered with decreasing fidelity
    #####################################################################################################
    def reorderModels(self):
        # Do model selection
        #M = list(self.modelLo)
        #P = chain.from_iterable(combinations(M,r) for r in range(len(M)+1))
        #for model in P: print model
    
        self.modelLo.append(self.modelHi)
        self.modelLo.reverse()
        self.nLo = len(self.modelLo)
        self.ordered = True


    #####################################################################################################
    # @brief Estimate correlations by sampling
    #####################################################################################################
    def estimateSigma(self):
        if self.ordered == False: self.reorderModels()
        N = self.nSigma
        self.nP = self.modelHi['model'].functional.getNumberOutputs()
        self.sigma = numpy.zeros((self.nP,self.nLo))
        self.omega = numpy.zeros((      1,self.nLo))
        self.rho   = numpy.zeros((self.nP,self.nLo+1))
        self.spe   = numpy.zeros((self.nP,self.nLo+1))
        
        z = self.evalDistribution(N,self.Y,self.nY)
        h = self.modelHi['model'].findSolution(z)
        
        self.h = h
        
        for l in range(self.nLo):
            exectime = time.time()
                        
            # Compute solution
            f = self.modelLo[l]['model'].findSolution(z)
            exectime = time.time() - exectime
            self.omega[0,l] = exectime/N
            
            # Compute mean
            mu1 = numpy.mean(h,axis=0)
            mu2 = numpy.mean(f,axis=0)
            
            # Compute variance
            self.sigma[:,l] = numpy.std(f,axis=0,ddof=0)
            
            # Compute second moment
            X2 = numpy.multiply(h,f);
            
            # Compute correlation coefficient
            covariance = (numpy.mean(X2,axis=0) - numpy.multiply(mu1,mu2));
            self.rho[:,l] = covariance / numpy.multiply(self.sigma[:,l],self.sigma[:,0])
            #self.spe[:,l],p = spearmanr(h,f)

        # Do not repeat this
        #print self.rho
        #print self.spe
        self.estimated = True
        
        # Erase if necessary
        self.order = numpy.array([range(self.nLo)])
        threshold = 0.01
#        for i in reversed(self.order[0]):
#            if (self.rho[0,i+1]+0.005 > self.rho[0,i] or self.rho[0,i] <= threshold) and i > 0 :
#                print "Deleting {}".format(i)
#                self.rho   = numpy.delete(self.rho  ,i,axis=1)
#                self.spe   = numpy.delete(self.spe  ,i,axis=1)
#                self.sigma = numpy.delete(self.sigma,i,axis=1)
#                self.omega = numpy.delete(self.omega,i,axis=1)
#                self.order = numpy.delete(self.order,i,axis=1)
#                self.nLo = self.nLo - 1
#                if self.rho[0,i] > threshold:
#                    print "Swapping components"
#                    self.modelLo[i+1],self.modelLo[i] = self.modelLo[i],self.modelLo[i+1]

        # Reorder if necessary
        #self.order = numpy.argsort(-self.rho,axis=1)
        #for i in range(self.nP):
        #    self.sigma[i,:] = self.sigma[i,self.order[0,:-1]]
        #    self.omega[i,:] = self.omega[i,self.order[0,:-1]]
        #    self.rho[i,:]   = self.rho[  i,self.order[0,:]]
        #    self.spe[i,:]   = self.spe[  i,self.order[0,:]]


    #####################################################################################################
    # @brief Compute optimal model evaluations given budget and correlations
    #####################################################################################################
    def parametersGivenBudget(self,p):
        if self.ordered == False: self.reorderModels()
    
        a = numpy.zeros((self.nP,self.nLo))
        m = numpy.zeros((self.nP,self.nLo))
        e = numpy.zeros((self.nP,1))
        
        # Rename for shortness
        w     = self.omega
        s     = self.sigma
        rho2  = self.rho**2
        drho2 = numpy.maximum(-numpy.diff(rho2),0) # We simply set to 0 the #samples if the models are not ordered
        
        # Compute weights
        for i in range(self.nP):
            a[i, :] = (self.rho[i,0:self.nLo]*s[i,0]) / s[i,:]
            a[i, 0] = 1.0
                
        # Compute averaged quantities
        sigma2bar = numpy.nansum(self.sigma**2,axis=0)
        rho2bar = numpy.nansum((self.sigma**2)*rho2[:,0:self.nLo],axis=0) / sigma2bar
        drho2bar = -numpy.diff(numpy.append(rho2bar,0.0))
        
        # Make sure r is not NaN (drho2bar<0) nor Inf (rho2bar=1)
        drho2bar = numpy.maximum(drho2bar,0.0)
        rho2bar  = numpy.minimum( rho2bar,1.0-1e-8)
        
        r = numpy.sqrt(w[0,0]/w)*numpy.sqrt(drho2bar/(1.-rho2bar[1]))
        
        # Compute model evaluations
        for i in range(self.nP):
            m[i, 0] = p[i]/numpy.dot(w,r.T)
            m[i,1:] = m[i,0]*r[0,1:]
    
        m = numpy.maximum(m,1e-8)
    
        # Compute error
        for i in range(0,self.nLo): e[:,0] += numpy.sqrt(w[0,i]*drho2[:,i])
        
        e[:,0] = (s[:,0]**2) * (e[:,0]**2) / p
    
        # Assign output
        self.a,self.m,self.e = (a,m,e)
        

    #####################################################################################################
    # @brief Compute optimal budget given error tolerance
    #####################################################################################################
    def parametersGivenTolerance(self,pMax):
        self.parameters = True
        if self.ordered == False: self.reorderModels()
        if self.estimated == False: self.estimateSigma()

        # Compute optimal evaluations with dummy budget
        self.parametersGivenBudget(numpy.ones(self.nP)*pMax)
        
        # Compute optimal budget (L2 norm)
        p = pMax * numpy.nanmean(self.e[:,0])/self.tol**2
                
        print "Optimal budget {}".format(pMax * numpy.nanmean(self.e[:,0])/self.tol**2)
        print "Max budget {}".format(pMax * numpy.nanmax(self.e[:,0]/self.tol**2))
        
        # Compute optimal evaluations with new budget
        self.parametersGivenBudget(numpy.ones(self.nP)*p)
    
        return (self.a,self.m,self.e)


    #####################################################################################################
    # @brief Run multifidelity Monte Carlo
    #####################################################################################################
    def mlmc(self,pMax=1):
        self.nP = self.modelHi['model'].functional.getNumberOutputs()
    
        # Estimate budget
        if self.parameters == False : (a,m,e) = self.parametersGivenTolerance(pMax)
        else : (a,m,e) = (self.a,self.m,self.e)
        
        # Component for output
        idx = [numpy.nanargmax(e[:,0])]
        indices = idx
        
        # Start timer
        exectime = time.time()
        
        print("K\tL\tN0\tN1\t\tAlpha\t\tY0\t\tY1\t\tP\n")
        print("-------------------------------------------------------------------------------------------------\n")
        
        # Evaluate estimator
        P = numpy.zeros(self.nP)
        Pl = numpy.zeros((self.nLo,self.nP))
        Yl = numpy.zeros((self.nLo,self.nP))
        ml = numpy.zeros((self.nLo,self.nP))
        V = numpy.zeros((self.nLo,self.nP))
        
        # Loop over levels
        for k in range(self.nLo):
            if k>0 :
                n0 = numpy.nanmax(numpy.ceil(m[:,k-1])).astype(int)
                coeff = (1./numpy.nanmax(numpy.ceil(m[:,k-1]))-1./numpy.nanmax(numpy.ceil(m[:,k])))
                V[k,:] = V[k-1,:] + coeff*(a[:,k]**2)*(self.sigma[:,k]**2)
                V[k,:] -= coeff*2.*a[:,k]*self.rho[:,k]*self.sigma[:,0]*self.sigma[:,k]
            else:
                n0 = 0
                z0 = numpy.zeros((0,self.nY))
                V[0,:] = self.sigma[:,0]**2 / numpy.nanmax(numpy.ceil(m[:,k]))
            
            n1 = numpy.nanmax(numpy.ceil(m[:,k])).astype(int)
            print "Computing {0} samples at level l={1}".format(n1,k)
            
            z1 = self.evalDistribution(n1-n0,self.Y,self.nY)
            z0 = numpy.concatenate((z0,z1),axis=0)
            
            f0 = self.modelLo[k]['model'].findSolution(z0[0:n0,:])
            f1 = self.modelLo[k]['model'].findSolution(z0[0:n1,:])
            
            #if k==1:
                #plt.scatter(f0,yold,c='b')
                #plt.savefig('results/model_surrogate_correlation_{}.png'.format(self.sample))
                #plt.close()
            
            yold = f1
            
            y0 = numpy.mean(f0,axis=0)
            y1 = numpy.mean(f1,axis=0)
            
            if k>0: ml[k,:] = y1-y0
            else:   ml[k,:] = y1
            
            Pl[k,:] = y1
            Yl[k,:] = P + a[:,k]*ml[k,:]
            
            # Compute estimator
            P = Yl[k,:]
            
            print "%i:\t%i\t%i\t%i\t\t%.4e\t%.4e\t%.4e\t%.4e\n" % (k,self.nLo-k-1,n0,n1,a[idx,k],y0[idx],y1[idx],P[idx])
    
        # End timer
        exectime = time.time() - exectime
        
        print "\033[92m"
        print "Multifidelity Monte Carlo complete."
        print "Execution time %f seconds." % exectime
        print "\033[0m"
        
        # Plot figures
#        for i in indices:
#            f,ax = plt.subplots(1,3,figsize=(10,5))
#            levels = numpy.linspace(0,self.nLo-1,self.nLo).astype(int)
#
#            ax[0].plot(levels,abs(Pl[:,i]),'k--*',label='E[$P_l$]')
#            ax[0].plot(levels,abs(Yl[:,i]),'b-o',label='E[$Y$]')
#            ax[0].set_xlabel('level l')
#            ax[0].set_title('Mean')
#            ax[0].legend(loc=3)
#
#            ax[1].semilogy(levels,V[:,i],'k--*',label='V[$Y_l$]')
#            ax[1].semilogy(levels,e[:,i],'k-o',label='V[$Y^*_l$]')
#            ax[1].semilogy(levels,numpy.ones(levels.shape)*self.tol**2,'r--',label='$\epsilon^2$')
#            ax[1].set_xlabel('level l')
#            ax[1].set_title('Errors')
#            ax[1].legend(loc=1)
#
#            ax[2].semilogy(levels,self.omega[0,:],'k-*',label='$\omega_l$')
#            ax[2].semilogy(levels,m[i,:],'k-*',label='$m_l$')
#            ax[2].set_xlabel('level l')
#            ax[2].set_title('Models')
#            ax[2].legend(loc=0)
#
#            plt.savefig('results/mfmc_convergence'+str(i)+'.png')
#            plt.close()

        return (P,V,Pl)


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


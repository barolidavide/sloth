
# @file	MLMC.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo
# @details TBD
# @author	Alessio Quaglino
# @version	2
# @date	21 July 2016 

from Sampler import *

#####################################################################################################
# @brief Define class
#####################################################################################################
class MLMC(Sampler):
    
    
    #####################################################################################################
    # @brief Constructor
    #####################################################################################################
    def __init__(self,tol):
        Sampler.__init__(self)
        self.alpha = -2
        self.beta = -4
        self.gamma = 1
        self.tol = tol
    
				
    #####################################################################################################
    # @brief Set convergence exponents
    #####################################################################################################
    def setExponents(self,alpha,beta,gamma):
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
								
 
    #####################################################################################################
    # @brief Runs N simulations at level l and l-1
    #####################################################################################################
    def run_fun_l(self,l,N):
    
        # Area element for norm computation
        dX = (1.0/self.nP)
    
        # Initialize output
        sumM = numpy.zeros((2,self.nP))
        sumV = numpy.zeros(2)
        k = self.evalDistribution(N,self.Y,self.nY)
    
        # Preprocess
        self.modelLo[l  ]['model'].preprocess(k)
        self.modelLo[l-1]['model'].preprocess(k)
        
        # Compute solution on fine mesh
        Pf = self.modelLo[l]['model'].findSolution(k)
            
        # Compute solution on coarse mesh
        if( l>0 )   : Pc = self.modelLo[l-1]['model'].findSolution(k)
        else        : Pc = numpy.zeros([N,self.nP])
    
        # Compute statistics
        for n in range(0,N):
            sumM[0,:] = sumM[0,:] + Pf[n,:]-Pc[n,:]
            sumM[1,:] = sumM[1,:] + Pf[n,:]
            sumV[0] = sumV[0] + linalg.norm(dX*(Pf[n,:]-Pc[n,:]))**2
            sumV[1] = sumV[1] + linalg.norm(dX*(Pf[n,:]        ))**2
  
        return (sumM,sumV)


    #####################################################################################################
    # @brief Computes slope of linear regression
    #####################################################################################################
    def slope(self,s):

        n = len(s) 
        
        x = numpy.zeros(n)
        y = numpy.zeros(n)
            
        # Populate x and y
        for i in range(0,n):
            x[i] = i 
            y[i] = numpy.log2(s[i]) 

        # Compute averages
        avgX = sum(x) / n
        avgY = sum(y) / n 
        
        # Compute deviations
        numerator = 0.0 
        denominator = 0.0 
        
        # Skip coarsest level (i.e. i>0)
        for i in range(0,n):
            numerator += (x[i] - avgX) * (y[i] - avgY) 
            denominator += (x[i] - avgX) * (x[i] - avgX) 
        
        # Return slope
        if(denominator == 0):
            print("Zero denominator in linear regression!")
            return 1 
        else:
            slope = - numerator / denominator 
            return slope
           

    #####################################################################################################
    # @brief Performs tests to compute coefficients
    #####################################################################################################
    def estimateExponents(self,Ns):

        dX = 1.0/self.nP
        L = 2
        Nl = numpy.ones(L+1,int)*Ns
        ml =  numpy.zeros(L+1)
        Vl =  numpy.zeros(L+1)
        
        print "\033[92m",
        print "Estimating alpha and beta using %d samples on 3 levels." % Ns

        # Update sample sums
        for l in range(0,L+1):
            
            # Take the remaining samples
            (sumM,sumV) = self.run_fun_l(l,Nl[l])
            
            # compute absolute average and variance
            # TODO: if negative set it to zero
            ml[l] = dX*linalg.norm(sumM[0,:]) / Nl[l]
            Vl[l] = sumV[0]/Nl[l] - ml[l]**2
        
        a = 0.0;
        b = 0.0;
        
        # Estimate alpha
        a = self.slope(ml)
        print "\033[92m",
        print "Estimated alpha = %e." % (a),
        print "\033[0m"
        
        # Estimate beta
        b = self.slope(Vl)
        print "\033[92m",
        print "Estimated beta = %e." % (b),
        print "\033[0m \n"

        return (a,b)


    #####################################################################################################
    # @brief Runs multilevel Monte Carlo
    #####################################################################################################
    def mlmc(self,N0):
        
        self.nP = self.modelLo[0]['model'].functional.getNumberOutputs()
        alpha = self.alpha
        beta = self.beta
        gamma = self.gamma
        eps = self.tol
        nP = self.nP
        
        L0 = N0.size;
        Nl = numpy.zeros(L0,int);
        
        sumVC = 0.0 
        rem = 0.0
        
        if( alpha<0.0 or beta<0.0 ):
            print "\033[93m"
            print "*" * 104
            print "**********************************",
            print "WARNING: alpha and beta not given!",
            print "**********************************"
            print "*" * 104
            print "\033[0m"
            (alpha,beta) = self.estimateExponents(160)
        
        L = len(Nl)-1
        Ns = numpy.zeros(L+1,int)
        sumM = numpy.zeros((L+1,nP))
        sumP = numpy.zeros((L+1,nP))
        sumF = numpy.zeros(L+1)
        sumV = numpy.zeros(L+1)
        ml =  numpy.zeros(L+1) 
        Vl =  numpy.zeros(L+1)
        Cl =  numpy.zeros(L+1)
        it = 0
        dX = 1.0/self.nP
        
        exectime = time.time()
        
        print("It\tL\tdN\tNopt\t\tAlpha\t\tBeta\t\tMean\t\tVariance\n") 
        print("-------------------------------------------------------------------------------------------------\n") 
        
        # Initial number of samples per level
        dNl = N0; 
           
        # Keep looping until no more samples are needed
        while( numpy.sum(dNl)>0 ):
                
            # update sample sums
            for l in range(0,L+1):
                if( dNl[l] > 0 ):
                
                    # Time to take a single sample on the finest level
                    if l==L : exectimeL = time.time()
                
                    # Check if we are asking too much
                    if( dNl[l]>80000 ):
                        print "Too many samples ({0}) requested at level {1}!".format(dNl[l],l)
                        exit() 
                        
                    # Take the remaining samples
                    (sum1,sum2) = self.run_fun_l(l,dNl[l])
                    Nl[l] = Nl[l] + dNl[l]
                    sumM[l,:] = sumM[l,:] + sum1[0,:]
                    sumP[l,:] = sumP[l,:] + sum1[1,:]
                    sumV[l] = sumV[l] + sum2[0]
                    sumF[l] = sumF[l] + sum2[1]
                    
                    # Time to take a single sample on the finest level
                    if l==L : exectimeL = (time.time() - exectimeL) / dNl[l]
                
                # compute absolute average and variance
                ml[l] = dX*linalg.norm(sumM[l,:]) / Nl[l]
                Vl[l] = max(0.0,sumV[l]/Nl[l] - ml[l]**2)
                Cl[l] = pow(2.0, gamma*l)
                
                # fix to cope with possible zero values for ml and Vl
                # (can happen in some applications when there are few samples)
                if( l>2 ):
                    if( ml[l] < 0.1*ml[l-1]/pow(2.0,alpha) and alpha > 0 ):
                        print "Warning: computed mean is lower than 10% of the predicted one."
                        print "This could happen if very few samples were taken on this level."
                        print "Increasing it using alpha to be on the safe side."
                        ml[l] = 0.1*ml[l-1]/pow(2.0,alpha)
                        
                    if( Vl[l] < 0.1*Vl[l-1]/pow(2.0,beta) and beta > 0 ):  
                        print "Warning: computed variance is lower than 10% of the predicted one."
                        print "This could happen if very few samples were taken on this level."
                        print "Increasing it using beta to be on the safe side."
                        Vl[l] = 0.1*Vl[l-1]/pow(2.0,beta)
                
                print "%i:\t%i\t%i\t%i\t\t%.4e\t%.4e\t%.4e\t%.4e\n" % (it,l,dNl[l],Ns[l],alpha,beta,ml[l],Vl[l])
            
            # Set optimal number of additional samples
            sumVC = 0.0 
            
            for l in range(0,L+1):
                sumVC += numpy.sqrt(Vl[l]*Cl[l]) 
            
            for l in range(0,L+1):
                Ns[l] = numpy.ceil(2.0 * numpy.sqrt(Vl[l]/Cl[l]) * sumVC / (eps*eps)) 
                dNl[l] = max(0, Ns[l]-Nl[l]) 
            
            # if (almost) converged, estimate remaining error and decide
            # whether a new level is required
            almostConverged = True 
            
            for l in range(0,L+1):
                if( dNl[l] > 0.01*Nl[l] ): # More than 1% of samples left
                    almostConverged = False 
            
            # Compute error
            sumVC = 0.0 
            rem = 0.0 
            
            for r in range(-2,1): 
                rem = max(rem,ml[L+r]*pow(2,alpha*r)) 
                
            rem *= 1.0/(pow(2.0,alpha) - 1) 
            
            # Decide whether to increase levels
            if( almostConverged and rem>eps/numpy.sqrt(2) ):
                
                Vl = numpy.append(Vl, Vl[L]/pow(2.0,beta) )
                Nl = numpy.append(Nl,  0) 
                Ns = numpy.append(Ns,  0) 
                dNl = numpy.append(dNl, 0) 
                ml = numpy.append(ml, 0)
                Cl = numpy.append(Cl, 0.0)
                sumM = numpy.concatenate((sumM, numpy.zeros((1,nP))))
                sumP = numpy.concatenate((sumP, numpy.zeros((1,nP))))
                sumV = numpy.concatenate((sumV, numpy.zeros(1)))
                sumF = numpy.concatenate((sumF, numpy.zeros(1)))
                
                L = L+1
                self.modelLo.append( {'type':type,'model':self.modelLo[L-1]['model'].getDuplicate()} )
                self.modelLo[L]['model'].level = L
                
                for l in range(0,L+1): 
                    sumVC += numpy.sqrt(Vl[l]*Cl[l]) 
                
                for l in range(0,L+1):
                    Cl[l] = pow(2.0,gamma*l) 
                    Ns[l] = numpy.ceil(2.0 * numpy.sqrt(Vl[l]/Cl[l]) * sumVC / (eps*eps)) 
                    dNl[l] = max(0, Ns[l]-Nl[l]) 
                
            it += 1 
            
        # finally, evaluate multilevel estimator
        Y = numpy.zeros(nP)
        P = numpy.zeros(L+1)
        V = numpy.zeros(L+1)
        tot = numpy.zeros((L+1,2))
        for l in range(0,L+1):
            Y += sumM[l,:]/Nl[l]
            P[l] = dX*linalg.norm(sumP[l,:])/Nl[l]
            V[l] = sumF[l]/Nl[l] - P[l]**2
            tot[l,0] = dX*linalg.norm(Y)
            tot[l,1] = tot[l-1,1] + Vl[l]/Nl[l]
        
        exectime = time.time() - exectime
        exectimeL = exectimeL*sum(Ns)
        rem_rel = rem/numpy.linalg.norm(Y)
        
        print "\033[92m"
        print "Computed mean with estimated absolute error %e and relative error %e." % (rem,rem_rel)
        print "Execution time %f seconds." % exectime 
        print "Estimated time for standard MC %f seconds." % exectimeL
        print "\033[0m"
        
        # Plot figures
        f,ax = plt.subplots(1,3,figsize=(10,5))
        levels = numpy.linspace(0,L,L+1).astype(int)

        ax[0].plot(levels,abs(P),'k--*',label='E[$P_l$]')
        ax[0].plot(levels,abs(tot[:,0]),'b-o',label='E[$Y$]')
        ax[0].set_xlabel('level l')
        ax[0].set_title('Mean')
        ax[0].legend(loc=3)
        
        ax[1].semilogy(levels,Vl,'k--*',label='V[$P_l-P_{l-1}$]')
        ax[1].semilogy(levels,V,'k-*',label='V[$P_l$]')
        ax[1].set_xlabel('level l')
        ax[1].set_title('Gain')
        ax[1].legend(loc=3)
        
        ax[2].semilogy(abs(ml)**2,'k-*',label='E[$P_l-P_{l-1}$]$^2$')
        ax[2].semilogy(levels,abs(tot[:,1]),'k-o',label='V[$Y$]')
        ax[2].semilogy(levels,numpy.ones(levels.shape)*eps**2,'r--',label='$\epsilon^2$')
        ax[2].set_xlabel('level l')
        ax[2].set_title('Errors')
        ax[2].legend(loc=0)
        
        plt.savefig('results/mlmc_convergence.png')
        plt.close()
        
        return Y


    #####################################################################################################
    # @brief Parse the input and run
    #####################################################################################################
    def run(self,input,N0):
        #	Read in method to use
        if len(input)>1 :
            method_name = str(input[1])
        else:
            method_name = "mlmc"

        #	Call solver once for debug
        if method_name == "run_fun_l":
            print("Doing a single run for debug.")
            (sum1,sum2) = self.run_fun_l(4,1)
            print sum1[0]
            print sum1[1]

        #   Estimate exponents
        elif method_name == "abc":
            print("Estimating MLMC exponents.")
            self.estimateExponents(1000)

        #    Call MLMC
        else:
            print("Performing multilevel Monte Carlo.")
            P = self.mlmc(N0)
            return P


#####################################################################################################
# End of class definition
#####################################################################################################


if __name__=='__main__':
    print "This is not a script. Please write a script that calls this class."


[GlobalParams]
  capacitance = 2.00   #microF/cm^2
  surface_to_volume = 1400.0 # cm
    ion_coeff = 2.0 #adimensional 
  integration_order = 1
[]

[Functions]
[./ElectrocardioForcing_function] type = ParsedFunction
vars = 'x0 t0'
vals = '0.2 1.9997'
 value ='if( x > 0.0  & x<x0 & y > 0.0  & y<x0 & t>0.0 & t < t0 ,  250000.0*exp(1/((t-1.0)*(t-1.0)-1.0)),0)'  # [Iapp]= microA/cm^3 
[../]

    [./KL_function]
		type = SolutionFunction
		solution = KL
    [../]

[]



[UserObjects]

	[./KL]
		type = SolutionUserObject
		system_variables = RandomField
		execute_on = timestep_end  #initial
		mesh = sub_0.e
	[../]
[]

###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################



[Mesh]
    
	file = dummy

[]



[Variables]
 [./potential]  order=FIRST  family=LAGRANGE
  [./InitialCondition]
   type = ConstantIC
   value = -90.3976264889   #mV
  [../]
 [../]
[]

[AuxVariables]
 [./nodal_previousAP_aux] order=FIRST family=LAGRANGE

   [./InitialCondition]
      type = FunctionIC
      function = '-90.3976264889'  
      [../] 
      [../]
[]


[Kernels]
 
[./euler]        type = ScaledTimeDerivative          variable = potential  [../]
[./ionicForcing] type = IonicForcingHigherOrder  variable = potential [../]
[./appliedStimulus] type = UserForcingFunctionHigherOrder variable = potential function = ElectrocardioForcing_function [../]
 [./diff]         type = MonodomainDiffusionHigherOrder variable = potential potential= nodal_previousAP_aux  [../]
[]


 [AuxVariables]
 [./activationTime] order = FIRST family = LAGRANGE [../]
  [./depolarizationTime] order = FIRST family = LAGRANGE [../]
  [./APD90] order = FIRST family = LAGRANGE [../]
 []
 
 
 [AuxKernels]
 [./nodal_AP_bdf3]
  type = Sbdf3Aux
  variable = nodal_previousAP_aux
  coupled = potential
  execute_on = 'timestep_end'
 [../]
 [./activationTimeAux]
 type = ActivationTimeAux variable = activationTime CurrentTime = activationTime Vmem = potential execute_on = 'timestep_end'
 [../]
  [./depolarizationTimeAux]
 type = DepolarizationTimeAux variable = depolarizationTime  Vmem = potential  activationTime=activationTime   depolarizationTime=depolarizationTime RMP=-90.0 APO=30.0  execute_on = 'timestep_end'
 [../]
 [./APD]
 type = APD90Aux variable = APD90 APD90 = APD90 activationTime=activationTime depolarizationTime=depolarizationTime   execute_on = 'timestep_end'
 [../]
 []
 
[Materials]
 [./CellType] type = CellType  cellTypeBlock=3 [../]
 [./fibers] type =FixedRotation E_fiber='1 0 0' E_sheet='0 1 0' [../]
 [./conductivity] type = MonodomainConductivity extraCellularConductivities ='6.2 2.4 2.4' intraCellularConductivities ='1.7 0.19 0.19' [../] # \muS / cm
 [./ionicMaterial_endo]    type = BernusHigherOrder  vmem = 'potential'   vmem_prev='nodal_previousAP_aux' [../]
[]


[Preconditioning]
[./SMP] type = SMP full = true [../]
[]



[Executioner] 
 type=Transient
 solve_type='Newton'
 start_time=   0.0
 end_time  = 1.0  #150
 dtmin     =   0.0125
 dtmax     =   0.0125
 
 petsc_options_iname = ' -pc_type -pc_hypre_type'
 petsc_options_value = ' hypre boomeramg'
line_search=none

#petsc_options_iname = ' -pc_type -pc_hypre_type'
#petsc_options_value = ' hypre boomeramg'

nl_rel_tol = 1e-8
nl_abs_tol = 1e-8

[]

[Outputs]
console = false
#file_base = APD90_2D_order1_IMEX_0125
csv=true
exodus = true 
#nemesis = true
#interval=40




[]

 


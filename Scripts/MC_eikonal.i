###############################################################
###############################################################

#			Mesh Configuration 

###############################################################
###############################################################


[Mesh]
    
	file = dummy.e

[]


###############################################################
###############################################################

#		Variables to solve equation for:
#		- potential 

###############################################################
###############################################################


[Variables]

	[./activationTime]
        	order = FIRST
        	family = LAGRANGE

     	[./InitialCondition]
      		type = FunctionIC
      		function =  1*y   #mV
	[../]

   [../]

[]

###############################################################
###############################################################

#    Functions: IC, stimulus and random field coming from KL 

###############################################################
###############################################################


[Functions]


    [./KL_function]
		type = SolutionFunction
		solution = KL
    [../]

[]



[UserObjects]

	[./KL]
		type = SolutionUserObject
		system_variables = RandomField
		execute_on = timestep_end  #initial
		mesh = sub_0.e
	[../]
[]


###############################################################
###############################################################

#			 BC and kernels 

###############################################################
###############################################################

[MeshModifiers]
 [./corner]
	type = AddExtraNodeset
	new_boundary = 'corner'
 	nodes = 0
 [../]
[]



[BCs]
[./potential_left] 
	type = DirichletBC 
	variable = activationTime 
	value = 0.0  
	boundary = 'corner' 
[../]
[]


[Kernels]
[./Eikonal] 
	type = Eikonal_KL 
	variable = activationTime 
	function = KL_function
    	c0 = 2.5 tau = 3.0 
[../]
[]

###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################


[AuxVariables]

	#[./KL_plot]

	#	order = FIRST #CONSTANT
	#	family = LAGRANGE #MONOMIAL

	#[../]

	[./potential_mean]

		order = FIRST
		family = LAGRANGE

	[../]

        [./potential_variance]

                order = FIRST
                family = LAGRANGE

        [../]
[]

[AuxKernels]
	#[./KL_plot_aux]

	#	type = FunctionAux
	#	variable = KL_plot
	#	function = KL

	#[../]

	[./MonteCarloMean]
		
		type = MonteCarloMeanAux 
		variable = potential_mean
		variable_to_integrate = activationTime
		num_samples = 50

	[../]

	[./Variance]

		type = MonteCarloVarianceAux
		variable = potential_variance
		variable_to_integrate = activationTime
		num_samples = 50
		mean = potential_mean

	[../]

[]



###############################################################
###############################################################

#    Fake Transient Executioner: Every time step is a sample

###############################################################
###############################################################

 [Preconditioning]
 [./prec]
	#type = FDP
 	type = SMP
 	full = true
 [../]
 []

[Executioner]

  	type=Transient
	solve_type=Newton
	line_search = none
 	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
 	petsc_options_value='   gmres   lu       NONZERO               superlu_dist'

	start_time = 0.0
	end_time = 50.0

	[./TimeStepper]
    		type = TransientOverSamples
    		next_sample = 1
    		dt = 1
  	[../]
  [../]
  
[]



###############################################################
###############################################################

#    			Postprocessors

###############################################################
###############################################################


[Postprocessors]
  [./dofs]
    type = NumDOFs
  [../]

 [./execution_time]
        type = PerformanceData
        event = active 
 [../]
  
  [./num_lin_it]
    type = NumLinearIterations
  [../]
  
  [./num_nonlin_it]
    type = NumNonlinearIterations
  [../]
[]

###############################################################
###############################################################

#    			Output options

###############################################################
###############################################################

[Problem]
    solve = true
[]


[Outputs]
    console = false
    execute_on = timestep_end
    exodus = true
    
[]

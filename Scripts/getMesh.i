[Mesh]
    #type = GeneratedMesh
    #dim = 2
    
    #nx = 10
    #ny = 60
    
    #xmin = 0.0  # in cm
    #xmax = 1.0
    
    #ymin = 0.0  # in ms
    #ymax = 6.0  # 0.8
 	type = GeneratedMesh
 	dim = 2
 	nx = 50
 	ny = 50
	nz = 50
 	xmin = 0.0  # in cm
 	xmax = 1.0
 	ymin = 0.0
 	ymax = 1.0
	zmin = 0.0
	zmax = 1.0 
 	parallel_type = replicated
[]


 
[Variables]
  [./RandomField]  
	order=FIRST  
	family=LAGRANGE
   [../]
[]


[Problem]
    kernel_coverage_check = false
[]


[Executioner]
  	type=Transient 
	solve_type=Newton
	line_search = none
 	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
 	petsc_options_value='   gmres   lu       NONZERO               superlu_dist'
	
	start_time = 0.0
	end_time = 50.0
[]



[Outputs]
	execute_on = 'timestep_end'
	console = false 
	exodus = true
	csv = true
[]


import numpy
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import pickle


# Save for plotting
file = open('output.pckl', 'rb')
xi     = pickle.load(file)
yi     = pickle.load(file)
x      = pickle.load(file)
y      = pickle.load(file)
xPlot  = pickle.load(file)
yPlot  = pickle.load(file)
sigma  = pickle.load(file)
file.close()

# Plot
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(x[0,:].T,x[1,:].T,y.T,c='r',depthshade=False,alpha=1.0)
ax.scatter(xi[0,:].T,xi[1,:].T,yi[:,0].T,c='b',depthshade=False,alpha=0.1)
ax.set_xlabel('1d')
ax.set_ylabel('surrogate')

#ax.set_xlim(0, 1)
#ax.set_ylim(0, 1)
#ax.set_zlim(0, 1)

plt.show()




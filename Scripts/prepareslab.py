import sys
from pyccmc import PyPropeiko
from pyccmc import igb_write
from pyccmc.pypropeiko import PacingSite
from angle2byte import angle2byte
#from templateap import template_AP
#from simpleexactecg import *

import numpy as np

def prepare_slab(nsub=1,nfact=10,
        prefix='slab',
        ang_alpha=0.0,ang_gamma=0.0,ang_phi=0.0,
        ecg_dt=1.0,ecg_length=500.0,
        leadfun=None,
        **kwargs):
    """
    Prepare a wall tissue specimen with axis
    oriented such that
    - x-axis is along endo to epi direction,
    - y-axis is along the counterclockwise
      direction of the LV, and
    - z-axis is parallel to and oriented as the
      base-to-apex ventricle axis.

    Dimension are fixed to 1.5 x 2.0 x 2.0 cm.
    """
    # size of the tissue slab [cm]
    lx,ly,lz = 2.0,2.0,2.0
    # center of the stimulus
    # FIXME this should be vertex coordinates!
    cx,cy,cz = 0.0,ly/2.0,lz/2.0
    # number of subdivisions in each direction
    nx,ny,nz = 4*nfact*nsub,4*nfact*nsub,4*nfact*nsub
    # prepare parameters
    params = PyPropeiko.default_parameters()
    params['dir_input'] = '.'
    params['dir_output'] = '.'
    # mesh-size
    hx,hy,hz = lx/nx,ly/ny,lz/nz
    params.update({'hx':hx,'hy':hy,'hz':hz})
    # coordinates
    x = np.linspace(0.0,lx,nx+1)
    y = np.linspace(0.0,ly,ny+1)
    z = np.linspace(0.0,lz,nz+1)
    # celltype
    celltype = PyPropeiko.CellType.LVENDO
    cell = np.empty(shape=(nz,ny,nx),dtype=np.uint8)
    cell.fill(celltype)
    params['substances'] = [PyPropeiko.default_Substance._replace(
        name="lv",cells=[celltype],**kwargs)]
    # write cells to file
    igb_write(cell,'{}-cell.igb'.format(prefix))
    params['fname_cell'] = '{}-cell'.format(prefix)
    # set the angles
    alpha = np.empty(shape=(nz,ny,nx),dtype=np.uint8)
    gamma = np.empty_like(alpha)
    phi = np.empty_like(alpha)
    # write to file the angles
    for label in ['alpha','gamma','phi']:
        data = locals()[label]
        ang = locals()['ang_{}'.format(label)]
        # convert to byte
        if isinstance(ang,tuple):
            # angles varies with X coordinate (cell-centered)
            _,_,xxc = np.meshgrid(z[:-1]+hz/2.0,y[:-1]+hy/2.0,x[:-1]+hx/2.0,
                                  indexing='ij',sparse=False)
            xnorm = xxc/lx;
            ang_byte,fact_a,zero_a = angle2byte(ang[0]+(ang[1]-ang[0])*xnorm)
            data[:] = ang_byte
        else:
            ang_byte,fact_a,zero_a = angle2byte(ang)
            data.fill(ang_byte)
        igb_write(data,'{}-{}.igb'.format(prefix,label),
                  facteur=fact_a,zero=zero_a)
        params['fname_{}'.format(label)] = '{}-{}'.format(prefix,label)
    # set output filename
    params['fname_acttimes'] = '{}_dtime'.format(prefix)
    # exact solution
    sigma_il = params['substances'][0].sigma_il
    sigma_el = params['substances'][0].sigma_el
    sigma_it = params['substances'][0].sigma_it
    sigma_et = params['substances'][0].sigma_et
    sigma_ic = params['substances'][0].sigma_ic
    sigma_ec = params['substances'][0].sigma_ec
    if sigma_ic < 0.0 : sigma_ic = sigma_it
    if sigma_ec < 0.0 : sigma_ec = sigma_et
    sigma_l = (sigma_il*sigma_el)/(sigma_il+sigma_el)
    sigma_t = (sigma_it*sigma_et)/(sigma_it+sigma_et)
    sigma_c = (sigma_ic*sigma_ec)/(sigma_ic+sigma_ec)
    rho2 = params['substances'][0].rho2
    if rho2 < 0.0: rho2 = params['substances'][0].alpha**2/params['substances'][0].beta
    zz,yy,xx = np.meshgrid(z,y,x,indexing='ij',sparse=False)
    dtime_exact = None
    if not isinstance(ang_alpha,tuple):
        # compute exact solution
        sc = 1.0/(rho2*sigma_c)
        sl = 1.0/(rho2*sigma_l)
        st = 1.0/(rho2*sigma_t)
        # local metric
        cosa,sina = np.cos(ang_alpha),np.sin(ang_alpha)
        cosg,sing = np.cos(ang_gamma),np.sin(ang_gamma)
        cosp,sinp = np.cos(ang_phi),np.sin(ang_phi)
        sxx = sc*(cosp*sing)**2 \
            + sl*(cosg*cosp*sina+cosa*sinp)**2 \
            + st*(cosa*cosg*cosp-sina*sinp)**2
        syy = sc*(sing*sinp)**2 \
            + sl*(cosa*cosp-cosg*sina*sinp)**2 \
            + st*(cosp*sina+cosa*cosg*sinp)**2
        szz = sc*cosg**2 \
            + sing**2*(sl*sina**2+st*cosa**2)
        sxy = sc*cosp*sing**2*sinp \
            + sl*(cosg*cosp*sina+cosa*sinp)*(cosg*sina*sinp-cosa*cosp) \
            + st*(cosp*sina+cosa*cosg*sinp)*(cosa*cosg*cosp-sina*sinp)
        sxz = sing*(sc*cosg*cosp \
                  - sl*sina*(cosg*cosp*sina+cosa*sinp) \
                  + st*cosa*(sina*sinp-cosa*cosg*cosp))
        syz = sing*(sc*cosg*sinp \
                  + sl*sina*(cosa*cosp-cosg*sina*sinp) \
                  - st*cosa*(sina*cosp+cosa*cosg*sinp))
        dtime_exact = np.sqrt(sxx*(xx-cx)**2+syy*(yy-cy)**2+szz*(zz-cz)**2
                             +2.0*sxy*(xx-cx)*(yy-cy)
                             +2.0*syz*(yy-cy)*(zz-cz)
                             +2.0*sxz*(xx-cx)*(zz-cz))
    else:
        dtime_exact = (z,y,x)
    # pacing-sites
    params['pacingsites'] = [PacingSite(int(cx/hx),int(cy/hy),int(cz/hz),0.0)]
    w_exact = None
    ecg_exact = None
    if leadfun is not None:
        # lead-field is Z(x,y,z) = x
        lead = leadfun(xx,yy,zz).astype(np.float32)
        igb_write(lead,'{}-LFV0.igb'.format(prefix))
        params['leadfields'] = [PyPropeiko.LeadField(
            lead="V0",
            fname_lead="{}-LFV0".format(prefix),
            fname_w="{}-LFV0_w".format(prefix),
            fname_ecg="{}-LFV0_ecg".format(prefix))]
        # AP
        if False:
            ap_fname = "../invmc/vm_tp06_endo"
            ap_dt = 0.5
        else:
            ap_fname = "{}-ap".format(prefix)
            ap_dt = ecg_dt
            # prepare the AP shape
            ndt = np.ceil(ecg_length/ecg_dt);
            ecg_t = ecg_dt*np.arange(ndt+1)
            lbU,ubU = template_AP(None)
            ecg_U = template_AP(ecg_t+lbU)
            # save to file
            ecg_U.astype(np.float32).tofile("{}.dat".format(ap_fname))

        params['aps'] = [PyPropeiko.ActionPotential(
            name="tp06endo",
            cells=[celltype],
            fname=ap_fname,
            dt=ap_dt)]
        params['ecg_length'] = ecg_length
        params['ecg_dt'] = ecg_dt
        # exact w and ecg
        if isinstance(dtime_exact,np.ndarray):
            sigmaxx = sigma_ic*(cosp*sing)**2 \
                + sigma_il*(cosg*cosp*sina+cosa*sinp)**2 \
                + sigma_it*(cosa*cosg*cosp-sina*sinp)**2
            ndt = np.ceil(ecg_length/ecg_dt);
            ecg_t = ecg_dt*np.arange(ndt)
            w_exact = simple_exact_w(ecg_t,
                sxx=sxx,syy=syy,szz=szz,
                xc=cx,yc=cy,zc=cz,
                xl=0.0,xr=lx,
                sigma=sigmaxx)
            ecg_exact = simple_exact_ecg(ecg_t,
                sxx=sxx,syy=syy,szz=szz,
                xc=cx,yc=cy,zc=cz,
                xl=0.0,xr=lx,
                sigma=sigmaxx)
    else:
        params['leadfields'] = []
        params['aps'] = []

    # init the solver
    return PyPropeiko(params),dtime_exact,w_exact,ecg_exact


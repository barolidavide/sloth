###############################################################
###############################################################

#			Fine Mesh Configuration 

###############################################################
###############################################################


[Mesh]
    
	file = dummy.e

[]


###############################################################
###############################################################

#
# 

###############################################################
###############################################################


[Variables]

	[./dummy]
        	order = FIRST
        	family = LAGRANGE
    	[../]

[]

###############################################################
###############################################################

#    Functions: IC, stimulus and random field coming from KL 

###############################################################
###############################################################


[Functions]

    [./MC_mean_on_fine_mesh]
		type = SolutionFunction
		solution = MC_mean_on_coarse_mesh
    [../]

[]



[UserObjects]

	[./MC_mean_on_coarse_mesh]
		type = SolutionUserObject
		system_variables = activationTime
		execute_on = timestep_end  #initial
		mesh = MC_out.e

	[../]
[]



###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################


[AuxVariables]

	[./mean_on_fine_mesh]

		order = FIRST 		
		family = LAGRANGE 

	[../]

[]

[AuxKernels]

	[./mean_fine_aux]

		type = FunctionAux
		variable = mean_on_fine_mesh
		function = MC_mean_on_fine_mesh

	[../]

[]


[Executioner]
   	type = Transient
    	solve_type = Newton # 'PJFNK' # Newton
	dt = 40
	start_time = 0.0
	end_time = 40.0
  [../]
[]

###############################################################
###############################################################

#    			Output options

###############################################################
###############################################################

[Problem]
    kernel_coverage_check = false
[]


[Outputs]
    console = false
    execute_on = timestep_end
    exodus = true
    csv = true
[]

###############################################################
###############################################################

#			Mesh Configuration 

###############################################################
###############################################################


[Mesh]
    
	file = dummy.e
[]


###############################################################
###############################################################

#		Variables to solve equation for:
#		- potential with IC to ease Newton conv. 

###############################################################
###############################################################


[Variables]

	[./potential]
        	order = FIRST
        	family = LAGRANGE

        [./InitialCondition]
            type = FunctionIC
            function = icfunc
        [../]

    [../]

[]

###############################################################
###############################################################

#    Functions: IC, stimulus and random field coming from KL 

###############################################################
###############################################################


[Functions]

    [./icfunc]
        type = ParsedFunction
        value =  'y >= 2.5*x +1'

    [../]

    [./ElectrocardioForcing_function]
    
	type = ParsedFunction
        value = 'exp( -(x*x) / (2*0.2*0.2)  ) * ( 1.0 <= y )* ( y <= 3.0 ) '

    [../]

    [./KL_function]
		type = SolutionFunction
		solution = KL
    [../]

[]



[UserObjects]

	[./KL]
		type = SolutionUserObject
		system_variables = RandomField
		execute_on = timestep_end  #initial
		mesh = sub_0.e
	[../]
[]


###############################################################
###############################################################

#			 BC and kernels 

###############################################################
###############################################################

[BCs]
  [./bottom]
    type = DirichletBC
    variable = potential
    boundary = 0
    value = 0
  [../]


[Kernels]
    [./timeDerivative]
        type = TimeAdvection1D
        coeff_time_derivative = 1.0
        variable = potential
    [../]

    [./Diffusion]
        type= SpaceTimeDiffusion1D
        variable = potential
        diffusion = 0.1
        time_stabilization = 0.0        # Change to another value
        function = KL_function
    [../]

	[./Ionic]
        type = IonicForcingFHN
        variable = potential
        C1 = 8
        C2 = 1
        Vrest = 0
        Vunstable = 0.1
        Vmax = 1
    [../]

    [./appliedStimulus]
        type = UserForcingFunction
        variable = potential
        function = ElectrocardioForcing_function
    [../]

[]

[Materials]
    [./recovery_variableFHN]
        type = IonicMaterialFHN
        vmem = '0'
        a = 0
        b = 0

    [../]
[]

###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################


[AuxVariables]

	#[./KL_plot]

	#	order = FIRST #CONSTANT
	#	family = LAGRANGE #MONOMIAL

	#[../]

	[./potential_mean]

		order = FIRST
		family = LAGRANGE

	[../]

        [./potential_variance]

                order = FIRST
                family = LAGRANGE

        [../]
[]

[AuxKernels]

	#[./KL_plot_aux]

	#	type = FunctionAux
	#	variable = KL_plot
	#	function = KL

	#[../]

	[./MonteCarloMean]
		
		type = MonteCarloMeanAux 
		variable = potential_mean
		variable_to_integrate = potential
		num_samples = 50

	[../]

	[./Variance]

		type = MonteCarloVarianceAux
		variable = potential_variance
		variable_to_integrate = potential
		num_samples = 50
		mean = potential_mean

	[../]

[]



###############################################################
###############################################################

#    Fake Transient Executioner: Every time step is a sample

###############################################################
###############################################################



[Executioner]
   	type = Transient
    	solve_type = Newton # 'PJFNK' # Newton

	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
	petsc_options_value='   gmres   lu       NONZERO               mumps'

	l_max_its = 50
	nl_max_its = 50

	start_time = 0.0
	end_time = 50.0

	[./TimeStepper]
    		type = TransientOverSamples
    		next_sample = 1
    		dt = 1
  	[../]
  [../]
  
[]



###############################################################
###############################################################

#    			Postprocessors

###############################################################
###############################################################


[Postprocessors]
  [./dofs]
    type = NumDOFs
  [../]

 [./execution_time]
        type = PerformanceData
        event = active 
 [../]
  
  [./num_lin_it]
    type = NumLinearIterations
  [../]
  
  [./num_nonlin_it]
    type = NumNonlinearIterations
  [../]
[]

###############################################################
###############################################################

#    			Output options

###############################################################
###############################################################

[Problem]
    solve = true
[]


[Outputs]
    console = false
    execute_on = timestep_end
    exodus = true
    
[]

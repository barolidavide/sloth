
[Mesh]
    
	file = dummy.e

[]


[Variables]

	[./potential]
        	order = FIRST
        	family = LAGRANGE
    	[../]
[]




[Problem]
    kernel_coverage_check = false
[]



[Executioner]

    type = PassoTransient
    solve_type = Newton 
	solver = passo_newton

	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package  '
	petsc_options_value='   gmres   lu       NONZERO               mumps'


    start_time = 0.0
    end_time = 1.0

    dtmin     =  1  
    dtmax     =  1  

[]

[Outputs]
    console = false
    execute_on = timestep_end
    exodus = true
[]

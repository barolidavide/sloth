
import sys
sys.path.append('../Distributions')
sys.path.append('../Functionals')
sys.path.append('../Inverse')
sys.path.append('../Models')
sys.path.append('../Samplers')
sys.path.append('../Solvers')
sys.path.append('../Surrogates')
sys.path.append('../Tests')

#whale_path = "/Users/seifbb/Projects/MooseWhale/whale/whale-opt"
whale_path = "/Users/alessio/projects/whale/whale-opt"

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import math as m
import numpy
import os
import pickle

from  MLMC import *
from  MCMC import *
from  MFMC import *
from BMFMC import *
from   SMC import *

from Vector2D import *
from Vector1D import *
from Energy import *
from Scalar import *
from Pointwise import *
from Sobol import *
from Variance import *
from Mean import *

from Dummy import *
from Laplacian1D import *
from Laplacian2D import *
from Ishigami import *
from Quintic import *
from Surrogate import *
from ShortColumn import *

from SampleDuplicate import *
from SampleFieldVoxel import *
from SampleNormal import *
from SampleHalton import *
from SampleUniform import *
from SampleVector import *

from GPR import *
from SVM import *
from Linear import *

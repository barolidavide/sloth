[Mesh]

    type = GeneratedMesh
    dim = 3
    nx = 50
    ny = 50
    nz = 50
    xmin = 0.0  # in cm
    xmax = 1.0
    ymin = 0.0
    ymax = 1.0
    zmin = 0.0
    zmax = 1.0
   parallel_type = replicated
	#file = dummy.e 
[]

[MeshModifiers]
 [./corner]
	type = AddExtraNodeset
	new_boundary = 'corner'
 	nodes = 0
 [../]
[]
 
[Variables]
  [./activationTime2D]  order=FIRST  family=LAGRANGE
     [./InitialCondition]
      type = FunctionIC
      function =  1*y   #mV
	[../]
   [../]
[]


[Kernels]
[./Eikonal] 
	type = Eikonal variable = activationTime2D
    c0 = 2.5 tau = 3.0 [../]
[]

[BCs]
[./potential_left] 
	type = DirichletBC 
	variable = activationTime2D 
	value = 0.0  
	boundary = 'corner' 
[../]
[]
 
 [Preconditioning]
 [./prec]
	#type = FDP
 	type = SMP
 	full = true
 [../]
 []
 
[Executioner]
  	type=Steady
	solve_type=Newton
	line_search = none
 	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
 	petsc_options_value='   gmres   lu       NONZERO               superlu_dist'
[]

[Outputs]
	execute_on = 'timestep_end'
	exodus = true
[]




import numpy as np

def angle2byte(rad_float):
    """
    Convert an angle from float to byte (compression).
    """
    zero_a  = -3.5
    fact_a  = 7.0/255
    ifact_a = 1.0/fact_a
    rad_byte = ((rad_float-zero_a)*ifact_a)+0.5
    if isinstance(rad_float,np.ndarray):
        rad_byte = rad_byte.astype('int')
    else:
        rad_byte = int(rad_byte)

    return rad_byte,fact_a,zero_a


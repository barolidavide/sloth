[GlobalParams]
  capacitance = 1.00   #microF/cm^2
  surface_to_volume = 1400.0 # cm
  ion_coeff = 1.0 #adimensional 
  integration_order = 1
[]


[Functions]
[./ElectrocardioForcing_function] type = ParsedFunction
vars = 'x0 t0'
vals = '0.15 2.0'
value ='if( x > 0.0  & x<x0 & y > 0.0  & y<x0 & z > 0.0  & z<x0 & t>0.0 & t < t0 ,  50000.0,0)'  # [Iapp]= microA/cm^3 
[../]

# [./KL_function]
#  type = ParsedFunction
#  value = 0
#  [../]

#[]

[./KL_function]
  type = SolutionFunction
  solution = KL
  [../]

[]

[Mesh]
    
    type = GeneratedMesh
    dim = 3
    nx = 10
    ny = 10
    nz = 10
    xmin = 0.0  # in cm
    xmax = 1.0
    ymin = 0.0
    ymax = 1.0
    zmin = 0.0
    zmax = 1.0
   parallel_type = replicated

	#file = dummy.e
	#parallel_type = replicated
[]



[Variables]
 [./potential]  order=FIRST  family=LAGRANGE
  #[./InitialCondition]
   #type = ConstantIC
   #value = -85.0   #mV
  #[../]
 [../]
[]



[ICs]
active = 'bounding_IC'
 [./bounding_IC]
    type = BoundingBoxIC
    x1 = 0.0
    x2 = 0.1
    y1 = 0.0
    y2 = 0.1
    z1= 0.0
    z2= 0.1
    inside = 30
    outside = -85
    variable = potential
  [../]
[]


[AuxVariables]
 [./nodal_previousAP_aux] order=FIRST family=LAGRANGE

   [./InitialCondition]
      type = FunctionIC
      function = '-85.0'  
      [../] 
      [../]
[]



[Kernels]
[./euler]  type =   ScaledTimeBDF  variable = potential potential= nodal_previousAP_aux [../] # 
[./ionicForcing] type = IonicForcingImplicit        variable = potential [../]
[./diff]         type = MonodomainDiffusionImplicit variable = potential  [../]
#[./appliedStimulus] type = UserForcingFunction variable = potential function = ElectrocardioForcing_function [../]
[]


 [AuxVariables]
 [./activationTime] order = FIRST family = LAGRANGE [../]
  [./depolarizationTime] order = FIRST family = LAGRANGE [../]
  [./APD90] order = FIRST family = LAGRANGE [../]
  [./distance_outer]    order = FIRST family = LAGRANGE [../]
  [./distance_LV_inner] order = FIRST family = LAGRANGE [../]
  [./thickness_parameter] order = FIRST family = LAGRANGE [../]
  [./fiber_x] order = CONSTANT family = MONOMIAL [../]
  [./fiber_y] order = CONSTANT family = MONOMIAL [../]
  [./fiber_z] order = CONSTANT family = MONOMIAL [../]
[]



 []
 
 
 [AuxKernels]
 [./nodal_AP_bdf3]
  type = Sbdf3Aux
  variable = nodal_previousAP_aux
  coupled = potential
  execute_on = 'timestep_end'
 [../]
 [./activationTimeAux]
 type = ActivationTimeAux variable = activationTime CurrentTime = activationTime Vmem = potential execute_on = 'timestep_end'
 [../]
  [./depolarizationTimeAux]
 type = DepolarizationTimeAux variable = depolarizationTime  Vmem = potential  activationTime=activationTime   depolarizationTime=depolarizationTime RMP=-90.0 APO=30.0  execute_on = 'timestep_end'
 [../]
 [./APD]
 type = APD90Aux variable = APD90 APD90 = APD90 activationTime=activationTime depolarizationTime=depolarizationTime   execute_on = 'timestep_end'
 [../]
 [./auxdistance_outer]    type=VolumeNearestNodeDistanceAux  variable=distance_outer   block=0   paired_boundary=left    execute_on = initial [../]
[./auxdistance_LV_inner] type=VolumeNearestNodeDistanceAux  variable=distance_LV_inner  block=0 paired_boundary=right execute_on = initial [../]
[./auxthickness] type=CardiacThicknessParameterAuxLV variable=thickness_parameter   distance_LV_inner=distance_LV_inner distance_outer=distance_outer execute_on = initial [../]
[./auxfiber_x] type = FibersAux variable = fiber_x component = 0 execute_on = timestep_begin [../]
[./auxfiber_y] type = FibersAux variable = fiber_y component = 1 execute_on = timestep_begin [../]
[./auxfiber_z] type = FibersAux variable = fiber_z component = 2 execute_on = timestep_begin [../]
[]


 
[Materials]
[./CellType] type = CellType  cellTypeBlock=3 [../]
[./fibers] type =FibersGeometryLV  thickness_parameter = thickness_parameter  noise_function = KL_function standardDeviation = 50.0 [../]
[./conductivity] type = MonodomainConductivity extraCellularConductivities ='6.2 2.4 2.4' intraCellularConductivities ='1.7 0.19 0.19'
[../] # \muS / cm

[./FitzHughNagumoLinearized]
   type = FitzHughNagumo 
    vmem = 'potential'
    block = 0
    Vrest = -85.0
    Vmax = 30.0
    Vunstable = -57.6 
    mu1 =  1.4e-3
    mu2 = 0.0
    mu3 = 0.013
    mu4 = 1.0
    [../]
[]





[UserObjects]

  [./KL]
    type = SolutionUserObject
    system_variables = RandomField
    execute_on = timestep_begin  #initial
    mesh = sub_0.e
  [../]
[]







[Preconditioning]
[./SMP] type = SMP full = true [../]
[]



[Executioner] 
 type=Transient
 start_time=   0.0
 end_time  = 40.0
 dtmin     =   0.0125
 dtmax     =   0.05
 solve_type='Newton'
 line_search=none
 
 petsc_options_iname = ' -pc_type -pc_hypre_type'
 petsc_options_value = ' hypre boomeramg'


#petsc_options_iname = ' -pc_type -pc_hypre_type'
#petsc_options_value = ' hypre boomeramg'

nl_rel_tol = 1e-8
nl_abs_tol = 1e-8

[]

[Outputs]
console = false
exodus=true
#interval=40
#file_base = original



[]




 



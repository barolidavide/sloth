###############################################################
###############################################################

#			Mesh Configuration 

###############################################################
###############################################################


[Mesh]

    #type = GeneratedMesh
    #dim = 3
    #nx = 10
    #ny = 10
    #nz = 10
    #xmin = 0.0  # in cm
    #xmax = 1.0
    #ymin = 0.0
    #ymax = 1.0
    #zmin = 0.0
    #zmax = 1.0
    #parallel_type = replicated
	
	file = ../Scripts/cube_encomplete.e 

	block_id = '0'
	boundary_id = '101 102 103 104 105'
	#boundary_name = 
	parallel_type = replicated

[]

[MeshModifiers]
[./corner] type = AddExtraNodeset new_boundary = 'corner' 
nodes = '0 1 2 3 4 5 6 7'
[../]

[./origin] type = AddExtraNodeset new_boundary = 'origin' 
nodes = 0
[../]

[./corner_x] type = AddExtraNodeset new_boundary = 'corner_x' 
nodes = 1
[../]

[./corner_y] type = AddExtraNodeset new_boundary = 'corner_y' 
nodes = 3
[../]

[./corner_z] type = AddExtraNodeset new_boundary = 'corner_z' 
nodes = 4
[../]


[./corner_xy] type = AddExtraNodeset new_boundary = 'corner_xy' 
nodes = 2
[../]

[./corner_xz] type = AddExtraNodeset new_boundary = 'corner_xz' 
nodes = 5
[../]

[./corner_yz] type = AddExtraNodeset new_boundary = 'corner_yz' 
nodes = 7
[../]

[./corner_xyz] type = AddExtraNodeset new_boundary = 'corner_xyz' 
nodes = 6
[../]

[]

#[MeshModifiers]
#[./corner] type = SubdomainBoundingBox block_name = 'corner' 
#bottom_left ='0 0 0'
#top_right ='0.1 0.1 0.1'
#block_id = 1
#[../]
#[]

###############################################################
###############################################################

#		Variables to solve equation for:
#		- activationTime 

###############################################################
###############################################################


[Variables]
[./activationTime]  order=FIRST  family=LAGRANGE
 [./InitialCondition] type = FunctionIC function = 1*y #mV [../]
[../]
[]

[Functions]
[./KL_function] type = SolutionFunction solution = KL [../]
[]

[UserObjects]
[./KL] type = SolutionUserObject system_variables = RandomField execute_on = initial mesh = sub_0.e [../]
[]


###############################################################
###############################################################

#			 BC and kernels 

###############################################################
###############################################################

[BCs]
#[./potential_left] 
#	type = DirichletBC 
#	variable = activationTime 
#	value = '2.5 + 50*(x+y+z)' 
#	boundary = 'corner' 
#[../]

active = 'potential_box'
[./potential_origin] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #2.2 
	boundary = 'origin' 
[../]

[./potential_corner_x] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #7.5 
	boundary = 'corner_x' 
[../]


[./potential_corner_y] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #4.5 
	boundary = 'corner_y' 
[../]

[./potential_corner_z] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #4.5 
	boundary = 'corner_z' 
[../]

[./potential_corner_xy] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #7 
	boundary = 'corner_xy' 
[../]

[./potential_corner_xz] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #7 
	boundary = 'corner_xz' 
[../]

[./potential_corner_yz] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #5 
	boundary = 'corner_yz' 
[../]

[./potential_corner_xyz] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #7  
	boundary = 'corner_xyz' 
[../]

[./potential_box] 
	type = DirichletBC 
	variable = activationTime 
	value = 0 #7  
	boundary = '101 102 103' 
[../]

[]


[Kernels]
[./Eikonal] 
type = Eikonal variable = activationTime  c0 = 10 tau = 70 #c0 = 40 tau = 400  #c0 = 2.5 tau = 3.0 
[../]
[]

###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################


[AuxVariables]
 [./distance_outer]    order = FIRST family = LAGRANGE [../]
 [./distance_LV_inner] order = FIRST family = LAGRANGE [../]
 [./thickness_parameter] order = FIRST family = LAGRANGE [../]
 [./fiber_x] order = CONSTANT family = MONOMIAL [../]
 [./fiber_y] order = CONSTANT family = MONOMIAL [../]
 [./fiber_z] order = CONSTANT family = MONOMIAL [../]
[]
 
[AuxKernels]   # 104 = left 105 = right
[./auxdistance_outer] type=VolumeNearestNodeDistanceAux variable=distance_outer block=1 paired_boundary=104 execute_on = initial [../]
[./auxdistance_LV_inner] type=VolumeNearestNodeDistanceAux variable=distance_LV_inner block=1 paired_boundary=105 execute_on = initial [../]
[./auxthickness] type=CardiacThicknessParameterAuxLV variable=thickness_parameter   distance_LV_inner=distance_LV_inner distance_outer=distance_outer execute_on = initial [../]
[./auxfiber_x] type = FibersAux variable = fiber_x component = 0 execute_on = timestep_begin [../]
[./auxfiber_y] type = FibersAux variable = fiber_y component = 1 execute_on = timestep_begin [../]
[./auxfiber_z] type = FibersAux variable = fiber_z component = 2 execute_on = timestep_begin [../]
[]

[Materials]
[./fibers] type =FibersGeometryLV  thickness_parameter = thickness_parameter  noise_function = KL_function standardDeviation = 50.0 [../]
[./conductivity] type = MonodomainConductivity extraCellularConductivities ='6.2 2.4 2.4' intraCellularConductivities ='1.7 0.19 0.19'    #6.2 1.7
[../] # \muS / cm
[]

###############################################################
###############################################################

#   Auxiliary variables for MC mean and variance visualization

###############################################################
###############################################################


[AuxVariables]

	[./potential_mean]

		order = FIRST
		family = LAGRANGE

	[../]

        [./potential_variance]

                order = FIRST
                family = LAGRANGE

        [../]
[]

[AuxKernels]

	[./MonteCarloMean]
		
		type = MonteCarloMeanAux 
		variable = potential_mean
		variable_to_integrate = activationTime
		num_samples = 50

	[../]

	[./Variance]

		type = MonteCarloVarianceAux
		variable = potential_variance
		variable_to_integrate = activationTime
		num_samples = 50
		mean = potential_mean

	[../]

[]

###############################################################
###############################################################

#    Fake Transient Executioner: Every time step is a sample

###############################################################
###############################################################

 [Preconditioning]
 [./prec]
	#type = FDP
 	type = SMP
 	full = true
 [../]
 []

[Executioner]
   	type = Transient
    solve_type = Newton # 'PJFNK' # Newton
    line_search = 'none'
	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
	petsc_options_value='   gmres   lu       NONZERO               mumps'

	#petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
 	#petsc_options_value='   gmres   lu       NONZERO               superlu_dist'

	start_time = 0.0
	end_time = 50.0  
[]



###############################################################
###############################################################

#    			Postprocessors

###############################################################
###############################################################

###############################################################
###############################################################

#    			Output options

###############################################################
###############################################################

[Outputs]
#console = false
#file_base = originalEikonal
exodus = true
[]



import numpy
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import pickle


# Save for plotting
file = open('/Users/alessio/Desktop/plotting.pckl', 'rb')
f = pickle.load(file)
X = pickle.load(file)
path = pickle.load(file)
file.close()

# Create mask
mask = f==100000;
f = numpy.ma.MaskedArray(f, mask)

# Plot
fig = plt.figure()
ax = fig.gca(projection='3d')
step = 3
ax.scatter(X[0][1::step,1::step,1::step],
           X[1][1::step,1::step,1::step],
           X[2][1::step,1::step,1::step],c=f[1::step,1::step,1::step], depthshade=False, alpha=0.1),

print f[1::step,1::step,1::step].shape

# Plot
for i in range(path.shape[0]):
    ax.scatter(X[0][path[i,0],path[i,1],path[i,2]],
               X[1][path[i,0],path[i,1],path[i,2]],
               X[2][path[i,0],path[i,1],path[i,2]])

ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_zlim(0, 1)

plt.show()




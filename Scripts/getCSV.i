[Mesh]

	file = dummy

[]

[Variables]
  [./RandomField]  order=FIRST  family=LAGRANGE
   [../]
[]

 
[Problem]
	kernel_coverage_check = false
[]


[Executioner]
  	type=Steady  
	solve_type=Newton
	line_search = none
 	petsc_options_iname=' -ksp_type -pc_type -pc_factor_shift_type -pc_factor_mat_solver_package '
 	petsc_options_value='   gmres   lu       NONZERO               superlu_dist'
	

[]

[VectorPostprocessors]
	[./nodal]
		type = NodalValueSampler
		variable = RandomField
		sort_by = id
	[../]
[]


[Outputs]
	execute_on = 'timestep_end'
	console = false
	#exodus = true
	csv = true
[]

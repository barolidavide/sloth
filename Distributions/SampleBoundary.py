
# @file	EikonalEAS.py
# @brief	This file contains the model implementation to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	12 October 2016

import sys
sys.path.append('/scratch/eikonal/repo/ccmc-inverse/pypropeiko')
from pypropeiko import *
from collections import namedtuple

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class Distribution:
    
    def __init__(self):
        self.computeBoundary()
        self.n = 3

    def sample(self):
        bi = numpy.random.choice(self.boundary_list.shape[1])
        return self.boundary_list[:,bi]

    def computeBoundary(self):
        
        pacings = [(24,46,104,0.0)] # RV free-wall
        exp = 'crt001s01'
        self.boundary_list = self.run_experiment(exp,pacings)

    def run_experiment(self,prefix,pacings,
            alphalvfast=10.0,alpharvfast=10.0,sitratio=0.1,
            leadfun=None):
        
        ExpResult = namedtuple('ExpResult','qrsd,tst,tat,teqrsd,w,ecg')
        PacingSite = PyPropeiko.PacingSite
        Substance  = PyPropeiko.Substance
        CT = PyPropeiko.CellType
        # initialize parameters
        params = PyPropeiko.default_parameters()
        datadir = '/scratch/eikonal/anatomy/crt001'
        params['dir_input'] = datadir
        params['dir_output'] = '/scratch/eikonal/exp/mf_eas/'
        # files
        params['fname_cell']  = 'crt001-18-heart1mm-c'
        params['fname_alpha'] = 'crt001-18-heart1mm-a'
        params['fname_gamma'] = 'crt001-18-heart1mm-g'
        params['fname_phi']   = 'crt001-18-heart1mm-p'
        params['fname_acttimes'] = '{}_dtime'.format(prefix)
        # pacing-sites from external list
        params['pacingsites'] = [PacingSite(*p) for p in pacings]
        params['aps'] = []
        params['leadfields'] = []
        params['propeiko_exe'] = '/scratch/eikonal/repo/ccmc-inverse/propeiko/propeiko_cuda'
        params['propeiko_ecg_exe'] = '/scratch/eikonal/repo/ccmc-inverse/propeiko/propeiko_ecg_mc_cuda'
        params['logfile'] = 'logfile_{}'.format(prefix)
        propeiko = PyPropeiko(params)
        
        # Computation of boundary points
        cmask = propeiko.get_cellmask()
        # boundary mask of the points
        bpmask = computeboundary(cmask)
        # points of the LV endocardium
        lvcells  = [CT.LVFASTENDO,CT.LVFASTENDOSEPT]
        lvcells += [CT.RVFASTENDO,CT.RVFASTENDOSEPT]
        lvcmask = propeiko.get_cellmask(include_only=lvcells)
        lvpmask = pointmaskfromcell(lvcmask)
        # boundary points of the domain that are on LV endocardium
        lvbmask = np.logical_and(bpmask,lvpmask)
        numpy.set_printoptions(threshold=numpy.nan)
        boundary_list = np.asarray(np.where(lvbmask))[::-1,:]
        return boundary_list





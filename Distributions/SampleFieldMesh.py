
# @file	createRandomField.cxx
# @brief	This file takes samples from a correlated random field in 2D
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import numpy
import scipy
from scipy import linalg
from LowRank import *
import sys
sys.path.append('../Models')


class SampleFieldMesh:
    
    def __init__(self,sigma,model,kernel_dim,mMax=2000):
        self.sigma = sigma
        
        # Get list of nodes
        (c,self.X) = model.getCSV()
        
        # Find out dimension
        self.N = c.shape[0]
        
        # Set the space dimension for the correlation kernel
        self.kernel_dim = kernel_dim
        
        # Precompute field
        self.createRandomField(model.getMassMatrix(),mMax)
    
    
    # Computes geodesic distance using fast marching method
    def computeGeoDistance(self,idx):
        
        # Run fast marching

#        r = (self.X[:,0]-self.X[idx,0])*(self.X[:,0]-self.X[idx,0]) +\
#            (self.X[:,1]-self.X[idx,1])*(self.X[:,1]-self.X[idx,1]) +\
#            (self.X[:,2]-self.X[idx,2])*(self.X[:,2]-self.X[idx,2])

        if self.kernel_dim==1 :
            r = (self.X[:,0]-self.X[idx,0])*(self.X[:,0]-self.X[idx,0])

        if self.kernel_dim==2 :
            r = (self.X[:,0]-self.X[idx,0])*(self.X[:,0]-self.X[idx,0]) +\
                (self.X[:,1]-self.X[idx,1])*(self.X[:,1]-self.X[idx,1])

        if self.kernel_dim==3 :
            r = (self.X[:,0]-self.X[idx,0])*(self.X[:,0]-self.X[idx,0]) +\
                (self.X[:,1]-self.X[idx,1])*(self.X[:,1]-self.X[idx,1]) +\
                (self.X[:,2]-self.X[idx,2])*(self.X[:,2]-self.X[idx,2])

        c = numpy.exp(-r/self.sigma)
        
        #return c[:,0]
        return c[:]


    # Creates the KL expansion of the random field
    def createRandomField(self,MassMatrix,mMax):
        
        print "Number of unknowns: %d" % self.N
        
        # Compute Cholesky
        (L,self.m) = lowRankCholesky(self.N,mMax,self.computeGeoDistance)
        
        # Create matrices
        #Id = numpy.eye(self.N)
        Id = numpy.eye(MassMatrix.shape[0])
        print Id
        MassMatrix = MassMatrix*Id     # Unsparsing the mass matrix
        
#        print "What is taking lot of time?"
#        B = numpy.dot(L.T,MassMatrix)
#        print "B?"
#        A = numpy.dot(B,L)    # LT*MassMatrix*L
#        print "A?"
        A = numpy.dot(L.T,L)
        
        flag = scipy.sparse.issparse(A)
        print flag
        
        # Compute eigenvalues
        (w,vv) = linalg.eigh(A)
        self.v = numpy.dot(L,vv)
        self.w = w
        
        print "Eigenvalues ratio: %f" % (max(w)/min(w))


    # Returns samples from the random field
    def sample(self, Yi):
        
        # Generate iid samples
        Yi = numpy.atleast_2d(Yi).T
        
        # Generate random field
        self.S = numpy.dot(self.v,Yi)
        
        # Create function to plot
        self.phi = self.S[:,0]

        # Return field
        return (self.X,self.phi)
        


    # Test correlation between selected points
    def testSamples(self,p1,p2,x):

        Yi = numpy.random.randn(self.m,10000).T
        self.sample(Yi)
        Ndim = p1.shape[0]
    
        i1 = numpy.zeros(Ndim).astype(int)
        i2 = numpy.zeros(Ndim).astype(int)
        truth1 = numpy.ones(self.xi.shape[1]).astype(bool)
        truth2 = numpy.ones(self.xi.shape[1]).astype(bool)
        dx = numpy.zeros(Ndim)
    
        for i in range(Ndim):
            i1[i] = numpy.argmin(numpy.abs(x[i,:]-p1[i]))
            i2[i] = numpy.argmin(numpy.abs(x[i,:]-p2[i]))
            truth1 = numpy.logical_and(truth1,self.xi[i,:]==i1[i])
            truth2 = numpy.logical_and(truth2,self.xi[i,:]==i2[i])
    
        idx1 = numpy.where(truth1)[0][0]
        idx2 = numpy.where(truth2)[0][0]
    
        for i in range(Ndim):
            dx[i] = (self.X[i][tuple(i1)]-self.X[i][tuple(i2)])**2
    
        euclCov = numpy.exp(-numpy.sum(dx)/self.sigma)
        compCov = numpy.cov(self.S[idx1,:],self.S[idx2,:])
        geodCov = numpy.dot(self.v[idx1,:],self.v[idx2,:])

        print "Computed covariance (Geodesic): %f" % compCov[0,1]
        print "Expected covariance (Geodesic): %f" % geodCov
        print "Expected covariance (Euclidean): %f" % euclCov
        print "Absolute error in covariance %f" % (abs(compCov[0,1])-abs(geodCov))








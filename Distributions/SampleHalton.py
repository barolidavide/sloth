
# @file	EikonalEAS.py
# @brief	This file contains the model implementation to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	12 October 2016

from math import log,floor,ceil,fmod
import numpy

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class SampleHalton:
    
    # Preallocates all Halton points
    def __init__(self,dim=1,nbpts=100):
        h = numpy.empty(nbpts * dim)
        h.fill(numpy.nan)
        p = numpy.empty(nbpts)
        p.fill(numpy.nan)
        P = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,\
             47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, \
             107, 109, 113, 127, 131, 137, 139, 149, 151]
        lognbpts = log(nbpts + 1)

        for i in range(dim):
            b = P[i]
            n = int(ceil(lognbpts / log(b)))
            
            for t in range(n):
                p[t] = pow(b, -(t + 1) )
    
            for j in range(nbpts):
                d = j + 1
                sum_ = fmod(d, b) * p[0]
                
                for t in range(1, n):
                    d = floor(d / b)
                    sum_ += fmod(d, b) * p[t]
            
                h[j*dim + i] = sum_

        self.points = h.reshape(nbpts, dim)
        self.i = 0
        self.n = dim
        self.nS = nbpts
        self.sigma = 4

    # Returns points one by one
    def sample(self):
            if(self.i == self.nS):  self.__init__(self.n,self.nS)
            if(self.i == 200):      self.sigma = 4
            self.i = self.i+1
            return self.sigma*(self.points[self.i-1,:]-0.5)







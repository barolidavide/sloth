
# @file	SampleVector.py
# @brief	This file contains a sampler from standard distributions
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	23 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class SampleVector:
    
    def __init__(self,distributions):
        self.n = len(distributions)
        self.Y = distributions

    def sample(self):
        z = numpy.zeros(self.n)
        
        for i,y in enumerate(self.Y):
            if y[0]=='Uniform':
                z[i] = numpy.random.uniform(y[1],y[2])
            elif y[0]=='Lognormal':
                z[i] = numpy.random.lognormal(y[1],y[2])
            elif y[0]=='Normal':
                z[i] = numpy.random.normal(y[1],y[2])

        return z








# @file	createRandomField.cxx
# @brief	This file takes samples from a correlated random field in 2D
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import numpy
from scipy import linalg
from LowRank import *


class SampleFieldVoxel:
    
    def __init__(self,sigma,model,compr,X,h,mask,mMax=2000):
        self.sigma = sigma
        self.compr = compr
        self.dist = model
    
        # Create N x N grid
        self.X = numpy.asarray(X)
        self.h = h
    
        # Create mask
        self.mask = mask

        # Precompute field
        self.createRandomField(mMax)
    
    
    # Computes geodesic distance using fast marching method
    def computeGeoDistance(self,idx):
        
        # Set initial point
        self.phi[tuple(self.xi[:,idx])] = 0
        
        # Run fast marching
        d = self.dist.findSolution(self.phi,self.h).flatten()
        r = d*d
        #X = numpy.reshape(self.X,self.xi.shape)
        #r = (X[0][:]-X[0][idx])**2 + (X[1][:]-X[1][idx])**2
        #print ((r-r2)**2).sum()
        #exit()
        c = numpy.exp(-r/self.sigma)
        
        # Restore initial point
        self.phi[tuple(self.xi[:,idx])] = 1

        # Compress if hole is present
        if self.compr   : return c.compressed()
        else            : return c


    # Creates the KL expansion of the random field
    def createRandomField(self,mMax):
        
        # Create level set
        phi = numpy.ones_like(self.X[0])
        
        # Apply mask
        self.phi = numpy.ma.MaskedArray(phi, self.mask)
        
        # Create indices
        self.xi = numpy.asarray(numpy.where(self.phi.mask==False))
        N = numpy.ma.count(self.phi.flatten())
        
        print "Number of unknowns: %d" % N
        
        # Compute Cholesky
        (L,self.m) = lowRankCholesky(N,mMax,self.computeGeoDistance)
        
        # Create matrices (assuming M=Id)
        A = numpy.dot(L.T,L)
        
        # Compute eigenvalues
        (w,vv) = linalg.eigh(A)
        self.v = numpy.dot(L,vv)
        self.w = w
        
        print "Eigenvalues ratio: %f" % (max(w)/min(w))


    # Returns samples from the random field
    def sample(self, Yi):
        
        # Generate iid samples
        Yi = numpy.atleast_2d(Yi).T
        
        # Generate random field
        self.S = numpy.dot(self.v,Yi)
        
        # Create function to plot
        self.phi[tuple(self.xi[:,:])] = self.S[:,0]

        # Return field
        return self.phi


    # Test correlation between selected points
    def testSamples(self,p1,p2,x):

        Yi = numpy.random.randn(self.m,10000).T
        self.sample(Yi)
        Ndim = p1.shape[0]
    
        i1 = numpy.zeros(Ndim).astype(int)
        i2 = numpy.zeros(Ndim).astype(int)
        truth1 = numpy.ones(self.xi.shape[1]).astype(bool)
        truth2 = numpy.ones(self.xi.shape[1]).astype(bool)
        dx = numpy.zeros(Ndim)
    
        for i in range(Ndim):
            i1[i] = numpy.argmin(numpy.abs(x[i,:]-p1[i]))
            i2[i] = numpy.argmin(numpy.abs(x[i,:]-p2[i]))
            truth1 = numpy.logical_and(truth1,self.xi[i,:]==i1[i])
            truth2 = numpy.logical_and(truth2,self.xi[i,:]==i2[i])
    
        idx1 = numpy.where(truth1)[0][0]
        idx2 = numpy.where(truth2)[0][0]
    
        for i in range(Ndim):
            dx[i] = (self.X[i][tuple(i1)]-self.X[i][tuple(i2)])**2
    
        euclCov = numpy.exp(-numpy.sum(dx)/self.sigma)
        compCov = numpy.cov(self.S[idx1,:],self.S[idx2,:])
        geodCov = numpy.dot(self.v[idx1,:],self.v[idx2,:])

        print "Computed covariance (Geodesic): %f" % compCov[0,1]
        print "Expected covariance (Geodesic): %f" % geodCov
        print "Expected covariance (Euclidean): %f" % euclCov
        print "Absolute error in covariance %f" % (abs(compCov[0,1])-abs(geodCov))









# @file	EikonalEAS.py
# @brief	This file contains the model implementation to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	12 October 2016

import numpy

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class SampleNormal:
    
    def __init__(self,mu,sigma,n=1):
        self.mu = mu
        self.sigma = sigma
        self.n = n

    def sample(self):
        if self.n == 1:
            return numpy.random.normal(self.mu,self.sigma)
        else:
            return numpy.random.multivariate_normal(self.mu,self.sigma)

    def mean(self): return self.mu








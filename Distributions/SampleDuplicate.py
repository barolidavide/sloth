
# @file	SampleVector.py
# @brief	This file draws two realizations of the variable per sample
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	24 May 2018

import sys
sys.path.append('../Scripts')
from config import *


###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class SampleDuplicate:
    
    def __init__(self,sampler):
        self.n = sampler.n*2
        self.Y = sampler

    def sample(self):
        z = self.Y.sample()
        w = self.Y.sample()
                
        return numpy.concatenate((z,w),axis=0)








# @file	LowRank.cxx
# @brief	This file generates a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import numpy
from scipy import linalg



# Compute low-rank pivoted Cholesky factorization
def lowRankCholesky(N,mMax,computeGeoDistance):
    
    # Guess reduced rank
    M = min(N,2000)
    
    # Allocate space for efficiency
    L = numpy.zeros((M,N))

    # Precompute vectors
    d = numpy.ones(N)
    e = numpy.linalg.norm(d,ord=1)
    p = numpy.arange(0,N)
    
    # Loop over reduced rank
    for m in range(0,M):
        
        # Pick highest pivot
        i = numpy.argmax(d[p[m:N]]) + m
        
        # Swap pi_m and pi_i
        p[[m,i]] = p[[i,m]]
    
        # Set diagonal element
        L[m,p[m]] = numpy.sqrt(d[p[m]])
        
        # Compute p[m]-th row
        a = computeGeoDistance(p[m])
        
        # Extract row of L
        l = L[0:m,p[m]]
        
        # Set off-diagonals
        t = L[0:m,:]
        r = L[0:m,p[m+1:N]]
        s = numpy.dot(l,r)
        L[m,p[m+1:N]] = (a[p[m+1:N]] - s)/L[m,p[m]]
        d[p[m+1:N]] -= L[m,p[m+1:N]]*L[m,p[m+1:N]]
    
        # Compute error
        e = numpy.sum(d[p[m+1:N]])
        
        print (m,e)
        
        # Stop if converged
        if e<-1e-1 or m+1==mMax:
            print "Converged in %d iterations" % m
            LT = numpy.delete(L,range(m+1,M),axis=0).T
            M = m+1
            break
        
    return (LT,M)











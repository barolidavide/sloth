
# @file	EikonalEAS.py
# @brief	This file contains the model implementation to perform MC
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	12 October 2016

import numpy

###############################################################################
# This is the class taking care of the physics
# (or of interfacing with external solvers)
###############################################################################
class SampleUniform:
    
    def __init__(self,a,b,n=1):
        self.a = a
        self.b = b
        self.n = n

    def sample(self):
        return numpy.random.uniform(self.a,self.b,self.n)








# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a scalar output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Running mlmc on 1D Laplacian with scalar QoI"

# Set maximum resolution
L = 6

# Create model
functional = Energy()
modelHi = Laplacian1D(functional,level=L);

# Create solver
solver = MFMC(tol=1e-2,nSigma=1000)

# Set random variables
sampler = SampleNormal(1.0,1.0e-1)
solver.addDistribution(sampler)

# Set high-fidelity model
solver.addHiFidelityModel('fine',modelHi)

# Set all other models
for l in range(3):
    modelLo = Laplacian1D(functional,level=l,type='noisy',noise=(2.**L)*2.e-3);
    solver.addLoFidelityModel('coarse',modelLo)

# Run solver
P = solver.run(sys.argv,1)

print "The computed mean is %f" % P
print "Test completed"

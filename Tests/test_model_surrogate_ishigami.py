
# @file	test_model_surrogate_ishigami.py
# @brief	This file tests the implementation of a surrogate model for the Ishigami function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Computing GPR surrogate of Ishigami function"

###############################################################################
# Create models and surrogate
###############################################################################

# Create model
#functional = Sobol([0],'total')
functional = Mean(1)
modelHi = Quintic(functional,level=0);

# Set random variables
sampler = SampleUniform(-1.1*m.pi,1.1*m.pi,3)
samplerS = SampleDuplicate(sampler)

# Create regression
regression = [GPR()]

# Create low-fidelity model
modelLo = Quintic(functional,level=2)

# Create surrogate
surrogate1 = Surrogate(modelLo,modelHi,regression,sampler,100)


###############################################################################
# Do scatter plot to compare plain low fidelity and surrogate
###############################################################################

# Create samples
sampler = SampleUniform(-m.pi,m.pi,3)
samplerS = SampleDuplicate(sampler)
k = numpy.zeros((400,samplerS.n))
for i in range(400): k[i,:] = samplerS.sample()

# Eval surrogate
surrogate1.findSolution(k,True)

# Plot results
idx = numpy.argsort(surrogate1.x[:,0])
xPlot = surrogate1.x[idx,0]
yPlot = surrogate1.f[idx,0]
sPlot = surrogate1.sigma[idx]

plt.scatter(surrogate1.x0,surrogate1.y0,c='b',label='Training')
#plt.scatter(surrogate1.x,surrogate1.y,c='r',label='Posterior')
plt.scatter(surrogate1.x,surrogate1.f,c='g',label='Prediction')
plt.fill(numpy.concatenate([xPlot[:], xPlot[::-1]]),
         numpy.concatenate([yPlot[:] - 1.9600 * sPlot,
                           (yPlot[:] + 1.9600 * sPlot)[::-1]]),
                       alpha=.5, fc='0.75', ec='None', label='95%')
plt.legend(loc='upper left')
plt.xlabel('Low-fidelity output')
plt.ylabel('High-fidelity output')
plt.savefig('results/model_surrogate_ishigami.png')
plt.close()

rhof = numpy.corrcoef(surrogate1.f,modelHi.findSolution(k),rowvar=False)
rhoy = numpy.corrcoef(surrogate1.y,modelHi.findSolution(k),rowvar=False)
rhox = numpy.corrcoef(surrogate1.x,modelHi.findSolution(k),rowvar=False)

#plt.scatter(surrogate1.y,modelHi.findSolution(k),c='r',label='posterior vs highF r={}'.format(rhoy[0,1]))
plt.scatter(surrogate1.x,modelHi.findSolution(k),c='b',label='LowF r={}'.format(rhox[0,1]))
plt.scatter(surrogate1.f,modelHi.findSolution(k),c='g',label='Surrogate r={}'.format(rhof[0,1]))
plt.legend(loc='upper left')
plt.xlabel('Low-fidelity output')
plt.ylabel('High-fidelity output')
plt.savefig('results/model_surrogate_ishigami_correlation.png')
plt.close()

print "Test completed"

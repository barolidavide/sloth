
import sys
sys.path.append('../Scripts')
from config import *

print "Running SMC on 1D Laplacian"

# Mesh
x = numpy.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);

# Starting value
startValue = numpy.array([0.0]);

# Parameters
it = 10
nObs = 40
nSamples = 10
k_exa = 1.0

# Create model
functional = Vector1D(x)
model = Laplacian1D(functional)

# Create solver
solver = SMC(model,startValue)

# Set random variables
sampler = SampleNormal(0.0,1.0e-1)
solver.addDistribution(sampler)

# Create synthetic data
k = numpy.random.normal(k_exa,1.0e-1,nObs)
data = model.findSolution(k)
solver.setData(data)

# Run solver
(k_est,u) = solver.run(sys.argv,it,nSamples)
err = abs(k_exa-numpy.mean(k_est,0))*100

print "Exact k is {0}".format(k_exa)
print "Estimated k is {0}".format(numpy.mean(k_est))
print "Error is {0}%".format(err[0])

# Plot histogram
plt.hist(k_est,bins=30)
plt.savefig('results/sampler_smc_laplacian1d.png')
plt.close()

print "Test completed"


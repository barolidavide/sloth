
import sys
sys.path.append('../Scripts')
from config import *

print "Running MCMC on 1D Laplacian"

# Mesh
x = numpy.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);

# Starting value
startValue = numpy.array([0.0]);

# Parameters
it = 8000
burnIn = it/10
nObs = 40
nSamples = 5

# Create model
functional = Vector1D(x)
model = Laplacian1D(functional,4)

# Create solver
solver = MCMC(model,startValue)

# Set random variables
solver.addDistribution('normal',[1.0,1.0e-1])

# Create synthetic data
k = numpy.random.normal(1.0,1e-1,nObs)
data = model.findSolution(k)
solver.setData(data)

# Run solver
chain = solver.run(sys.argv,it,nSamples)
postBurnChain = chain[burnIn:it,0]

# Check acceptance 
uniqueChain = numpy.unique(postBurnChain)
acceptanceRatio = numpy.true_divide(uniqueChain.size,it-burnIn)*100
print "Acceptance ratio: %f %%" % acceptanceRatio

# Plot histogram
plt.hist(uniqueChain,bins=30)
plt.savefig('results/sampler_mcmc_laplacian1d.png')
plt.close()

print "Test completed"



# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *
from Linear import LR
import matplotlib.pyplot as plt
import numpy



print "Running GPR fit test"

# Generate data
N = 100
x = numpy.linspace(0,10,N)
y = numpy.sqrt(x) + numpy.random.uniform(-2,2,N)
xi = numpy.random.normal(5.0,1,10000)
xPlot = numpy.linspace(0,10,100)
xSample = numpy.random.normal(5.0,1,1000)

# Fit using Gaussian Process Regression
gpReg = GPR()
gpReg.fit(x,y)
(yi,sigma) = gpReg.eval(xi)
(yPlot,sPlot) = gpReg.eval(xPlot)
yVec = gpReg.samplePosterior(xPlot,3)
ySample = gpReg.samplePosterior(xSample)

# Plot regression
f, (ax1,ax2) = plt.subplots(1,2)
ax1.plot(x,y,'r.')
ax1.plot(xPlot,yPlot,'k-')
ax1.plot(xPlot,yVec[:,0],'b')
ax1.plot(xPlot,yVec[:,1],'g')
ax1.plot(xPlot,yVec[:,2],'r')
ax1.set_title('GPR fit')
ax1.legend(('Data','mu','f1','f2','f3'),loc=0)

# Plot histogram
ax2.hist(ySample.flatten(),50)
ax2.set_title('Estimated distribution')
ax2.axvline(numpy.mean(yi),linewidth=4, color='k')
ax2.axvline(numpy.mean(ySample[:,0]),linewidth=2, color='b')
ax2.axvline(numpy.mean(ySample[:,1]),linewidth=2, color='g')
ax2.axvline(numpy.mean(ySample[:,2]),linewidth=2, color='r')

# Show fit
plt.show()

print "Test completed"



# @file	test_sampler_mfmc_quintic.py
# @brief	This file tests the implementation of mfmc using a Quintic function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	1st June 2018

import sys
sys.path.append('../Scripts')
from config import *

###############################################################################
# Function running single experiment
###############################################################################
def runQuintic(functional,tolerance,reference,training,sampler,sampler2,nonlinear=False):
    
    # Create model
    modelHi = Quintic(functional,level=0);

    # Create solver
    solver = MFMC(tol=tolerance,nSigma=100)

    # Set random variables
    solver.addDistribution(sampler)

    # Set high-fidelity model
    solver.addHiFidelityModel('fine',modelHi)

    # Set all other models
    model2 = Quintic(functional,level=2);
    model1 = Quintic(functional,level=1);

    # Create Surrogate model
    if nonlinear:
        regression1 = [GPR() for i in range(len(reference))]
        regression2 = [GPR() for i in range(len(reference))]
        surrogate2 = Surrogate(model2,modelHi,regression2,sampler2,100)
        surrogate1 = Surrogate(model1,modelHi,regression1,sampler2,100)
        solver.addLoFidelityModel('coarse',surrogate2)
        solver.addLoFidelityModel('coarse',surrogate1)
    else:
        solver.addLoFidelityModel('coarse',model2)
        solver.addLoFidelityModel('coarse',model1)

    # Add mid fidelity

    # Estimate error
    M = 100
    E = numpy.zeros((M,len(reference)))
    m = numpy.zeros((M,len(reference),3))
    a = numpy.zeros((M,len(reference),3))
    r = numpy.zeros((M,len(reference),3))
    omega = [1.0,0.05,0.001]
    
    for i in range(M):

        # Estimate correlations
        #if i>0 : solver.nLo = 3
        solver.estimateSigma()

        #if nonlinear:
        #    plt.scatter(surrogate2.x,solver.h,c='b')
        #    plt.scatter(surrogate2.f,solver.h,c='g')
        #    plt.savefig('results/model_surrogate_ishigami_correlation.png')
        #    plt.close()

        print solver.order

        # Set times
        if solver.order.shape[1] == 3:
            solver.omega[0,0] = omega[solver.order[0,0]]
            solver.omega[0,1] = omega[solver.order[0,1]]
            solver.omega[0,2] = omega[solver.order[0,2]]
        else:
            solver.omega[0,0] = omega[0]
            solver.omega[0,1] = omega[2]

        # Solve optimization problem
        #solver.parametersGivenTolerance(1)
        solver.parametersGivenBudget(numpy.ones(solver.nP)*training)
        solver.parameters = True
        #solver.sample = i # Just for saving figures

        # Run solver
        (P,V,Pl) = solver.run(sys.argv,1)

        for ri,ref in enumerate(reference):
            E[i,ri] = (P[ri]-ref)**2.0
            for o in solver.order[0,:]:
                m[i,ri,o] = solver.m[ri][o]
                a[i,ri,o] = solver.a[ri][o]
                r[i,ri,o] = solver.rho[ri,o]

    print "The computed estimator is {} with error {}" .format(P[0],E.mean())
    print "The estimated error is {} and {}".format(V[-1,0],solver.e[-1,0])
    print "The averages are m={} a={} r={}".format(m.mean(axis=0),a.mean(axis=0),r.mean(axis=0))

    return E.mean(axis=0)


###############################################################################
# Function running without then with nonlinear map
###############################################################################
def runTwice(functional,tolerance,reference,name,sampler,sampler2):
    training = [40,80,160,320]
    #training = [40]
    e = numpy.zeros((len(training),len(reference)))
    e_nl = numpy.zeros((len(training),len(reference)))
    
    for i in range(len(training)):
        e[i,:] = runQuintic(functional,tolerance,reference,training[i],sampler,[])
        e_nl[i,:] = runQuintic(functional,tolerance,reference,training[i],sampler,sampler2,True)

    print e
    print e_nl
    
    for i in range(len(reference)):
        plt.plot(training,e[:,i],training,e_nl[:,i])
        plt.savefig('results/sampler_mfmc_ishigami_{}_{}.png'.format(name,i))
        plt.close()


###############################################################################
# Run experiments
###############################################################################
print "Running mlmc on Quintic function with scalar QoI"

tolM = 0.046
tolV = numpy.sqrt(0.078)
exM = 0.5
exV = 85.75

sampler = SampleUniform(-m.pi,m.pi,3)
samplerS = SampleDuplicate(sampler)
sampler2 = SampleUniform(-1.1*m.pi,1.1*m.pi,3)
sampler2S = SampleDuplicate(sampler2)

#runTwice(Mean(1),tolM,[exM],'convergenceM',sampler,sampler2)
runTwice(Variance(1),tolV,[exV],'convergenceV',sampler,sampler2)
#runTwice(Sobol([0,1,2],'main'), 0.046,exSm,'convergenceSm',samplerS,sampler2S)
#runTwice(Sobol([0,1,2],'total'),0.046,exSt,'convergenceSt',samplerS,sampler2S)

print "Test completed"


# @file	createRandomField.cxx
# @brief	This file generated a correlated random field in 3D
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from FastMarching import *


print "Running random field 3D test"

sigma = 0.2
hole = True
        
# Create N x N x N grid
Nx = 19
x = numpy.zeros((3,Nx))
x[0,:] = numpy.linspace(0,1,Nx)
x[1,:] = numpy.linspace(0,1,Nx)
x[2,:] = numpy.linspace(0,1,Nx)
X = numpy.meshgrid(x[0,:],x[1,:],x[2,:],indexing='ij')
h = 1./(Nx-1)

# Create mask
mask = numpy.logical_and(abs(X[0]-0.5)<0.45,
            numpy.logical_and(abs(X[1]-0.5)<0.45,abs(X[2]-0.5)<0.45))
if hole==False : mask = numpy.zeros_like(mask)

# Create random field
model = FastMarching()
field = SampleFieldVoxel(sigma,model,hole,X,h,mask)

# Take sample
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
phi = field.sample(sampler.sample())

# Test
p1 = numpy.asarray([0.01,0.5,0.5])
p2 = numpy.asarray([0.99,0.5,0.5])
field.testSamples(p1,p2,x)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(X[0],X[1],X[2],c=phi)
plt.savefig('results/field3d.png')
plt.close()

print "Test completed"



# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *


print "Running BMFMC convergence test with 1D Laplacian"

# Parameters
nMC = 50000
nRE = [12,24,48,96,192]
err = []

# Create model
functional = Energy()
model = Laplacian1D(functional,4)
regression = GPR()
surrogate = GPR()
samplerN = SampleNormal(1.0,1.0e-1)
samplerU = SampleUniform(0.1,1.9)

# Create solver
solver = BMFMC(nMC)

# Set random variables
solver.addLoDistribution(samplerN)
solver.addHiDistribution(samplerU)

# Add models
solver.addHiFidelityModel('fine',model)
#solver.addLoFidelityModel('coarse',model)
solver.addLoFidelityModel('noisy',model)

# Add regressions
solver.addRegression(regression)

# Main loop
for nS in nRE :
    solver.nRE = nS
    (mu,sigma) = solver.run(sys.argv)
    err = numpy.append(err,sigma)
    print "The computed mean is %f" % mu
    print "The computed sigma is %f" % sigma

# Convergence rate
for i in range(1,err.size):
    rate = numpy.log(err[i-1]/err[i])/numpy.log(2.0)
    print 'Convergence rate: %f' % rate

# Plot
fig = plt.figure()
plt.plot(nRE,err)
plt.savefig('results/convergence.png')
plt.close()

print "Test completed"



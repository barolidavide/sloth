
# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
from FastMarching import *


print "Running random field 2D test"

sigma = 0.05
hole = False
        
# Create N x N grid
Nx = 101
x = numpy.zeros((2,Nx))
x[0,:] = numpy.linspace(0,1,Nx)
x[1,:] = numpy.linspace(0,1,Nx)
X = numpy.meshgrid(x[0,:],x[1,:],indexing='ij')
h = 1./(Nx-1.)
        
# Create mask
mask = numpy.logical_and(abs(X[0]-0.5) < 0.05, abs(X[1]-0.5) < 0.45)
if hole==False : mask = numpy.zeros_like(mask)

# Create random field
dist = FastMarching()
field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)

# Take sample
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
phi = field.sample(sampler.sample())

# Test
p1 = numpy.asarray([0.35,0.5])
p2 = numpy.asarray([0.65,0.5])
field.testSamples(p1,p2,x)

plt.contourf(X[0],X[1],phi)
plt.savefig('results/field2d.png')
plt.close()

print "Test completed"

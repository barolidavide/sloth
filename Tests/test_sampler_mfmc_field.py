
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a field output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Running mlmc on 1D Laplacian with vector QoI"

# Mesh
x = numpy.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);
#x = numpy.array([0.1,0.5]);

# Set maximum resolution
L = 6

# Create model
functional = Vector1D(x)
modelHi = Laplacian1D(functional,level=L)

# Create solver
solver = MFMC(tol=1e-2,nSigma=1000)

# Set random variables
sampler = SampleNormal(1.0,1.0e-1)
solver.addDistribution(sampler)

# Set high-fidelity model
solver.addHiFidelityModel('fine',modelHi)

# Set all other models
for l in range(2):
    modelLo = Laplacian1D(functional,level=l,type='noisy',noise=(2.**L)*2.e-3)
    solver.addLoFidelityModel('coarse',modelLo)

# Run solver
u = solver.run(sys.argv,1)

# Plot solution
modelHi = Laplacian1D(functional,10)
solver.addHiFidelityModel('fine',modelHi)
uMean = solver.meanSolution()
functional.plotSolution(uMean,u)

# Get model evaluations
(a,m,e) = solver.parametersGivenTolerance(1)

# Plot model evaluations in the same figure
for i in range(m.shape[1]): plt.semilogy(x,m[:,i],label='model '+str(i))
plt.legend(loc=1)
plt.savefig('results/mfmc_evaluations.png')
plt.close()

print "Test completed"


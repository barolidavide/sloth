
import sys
sys.path.append('../Scripts')
from config import *


print "Running sklearn convergence test"

# Parameters
nRE = [12, 25, 50, 100, 200, 400]
err = []
x = numpy.linspace(0, 10, 100)
sigma = 3.0e-1

# Create model
regression = GPR()

# Main loop
for nS in nRE :
    print ""

    # Data points
    xS = numpy.linspace(0, 10, nS)
    xi = xS.T
    yi = xS.T + numpy.random.normal(0.,sigma,nS)

    # Fit
    regression.fit(xi,yi)
    (mu,sigma_h) = regression.eval(x.T)
    
    #var_h = regression.evalVariance(x)
    var_h = sigma_h
    
    # Errors
    err_i = numpy.linalg.norm(mu-x)/numpy.linalg.norm(x)
    std_i = numpy.mean(sigma_h)
    var_i = numpy.mean(var_h)
    err = numpy.append(err,var_i)
    print "Data points = %d" % nS
    print "Sigma = %f" % std_i
    print "Variance = %f" % var_i
    print "Relative mu error = %e" % err_i

# Convergence rate
for i in range(1,err.size):
    rate = numpy.log(err[i-1]/err[i])/numpy.log(2.0)
    print 'Convergence rate: %f' % rate

# Plot
f,ax = plt.subplots(1,2)
ax[0].plot(x,mu)
ax[0].plot(xi,yi,'.')
ax[0].fill_between(x,mu[:,0]-1.96*sigma_h,mu[:,0]+1.96*sigma_h,alpha=0.5)
ax[1].plot(nRE,err)
f.savefig('results/convergence_sklearn.png')
plt.close()

print "Test completed"



# @file	test_sampler.py
# @brief	This file tests the implementation of mfmc using the Ishigami function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	11th May 2018

import sys
sys.path.append('../Scripts')
from config import *

###############################################################################
# Function creating models and solver
###############################################################################
def touchIshigami(functional,tolerance,reference,nonlinear):
    
    # Create model
    modelHi = Ishigami(functional,level=0);

    # Create solver
    solver = MFMC(tol=tolerance,nSigma=20)

    # Set random variables
    solver.addDistribution(sampler)

    # Set high-fidelity model
    solver.addHiFidelityModel('fine',modelHi)

    # Set all other models
    model2 = Ishigami(functional,level=2);
    model1 = Ishigami(functional,level=1);

    # Create Surrogate model
    if nonlinear:
        regression = [GPR() for i in range(len(reference))]
        surrogate2 = Surrogate(model2,modelHi,regression,sampler2,100)
        solver.addLoFidelityModel('coarse',surrogate2)
    else:
        solver.addLoFidelityModel('coarse',model2)

    # Add models to solver
    solver.addLoFidelityModel('coarse',model1)

    return solver


###############################################################################
# Function doing preprocessing
###############################################################################
def preIshigami(axs,color,functional,tolerance,reference,training,sampler,sampler2,nonlinear=False):

    solver = touchIshigami(functional,tolerance,reference,nonlinear)
    
    # Estimate m and a
    M = 5000
    m = numpy.zeros((M,len(reference),3))
    a = numpy.zeros((M,len(reference),3))
    
    for i in range(M):
        
        # Estimate correlations
        solver.nSigma = training
        solver.estimateSigma()

        # Set times
        solver.omega[0,0] = 1.0
        solver.omega[0,1] = 0.05
        solver.omega[0,2] = 0.001

        # Solve optimization problem
        #solver.parametersGivenTolerance(1)
        solver.parametersGivenBudget(numpy.ones(solver.nP)*40)
        solver.parameters = True

        for ri,r in enumerate(reference):
            m[i,ri,:] = solver.m[ri]
            a[i,ri,:] = solver.a[ri]

    print "The average m is {}".format(m.mean(axis=0))
    print "The average a is {}".format(a.mean(axis=0))
    print "The std dev of m is {}".format(m.std(axis=0))
    print "The std dev of a is {}".format(a.std(axis=0))

    for i in range(3):
        axs[i].hist(m[:,0,i],20,histtype='stepfilled',color=color,alpha=0.5,label='{}'.format(training))



###############################################################################
# Function running single experiment
###############################################################################
def runIshigami(axs,color,functional,tolerance,reference,training,budget,sampler,sampler2,nonlinear=False):
    
    solver = touchIshigami(functional,tolerance,reference,nonlinear)

    # Estimate error
    M = 1000
    E = numpy.zeros((M,len(reference)))
    y = numpy.zeros((M,len(reference)))
    m = numpy.zeros((M,len(reference),3))
    a = numpy.zeros((M,len(reference),3))
    
    for i in range(M):

        # Estimate correlations
        solver.nSigma = training
        solver.estimateSigma()

        # Set times
        solver.omega[0,0] = 1.0
        solver.omega[0,1] = 0.05
        solver.omega[0,2] = 0.001

        # Solve optimization problem
        #solver.parametersGivenTolerance(1)
        solver.parametersGivenBudget(numpy.ones(solver.nP)*budget)
        solver.parameters = True
        
        # Run solver
        (P,V,Pl) = solver.run(sys.argv,1)
        
        for ri,r in enumerate(reference):
            y[i,ri] = P[ri]
            E[i,ri] = (P[ri]-r)**2.0
            m[i,ri,:] = solver.m[ri]
            a[i,ri,:] = solver.a[ri]

    print "The computed estimator is {} with error {}" .format(P,E.mean(axis=0))
    print "The estimated error is {} and {}".format(V[-1,:],solver.e[-1,:])
    print "The average m is {} and a is {}".format(m.mean(axis=0),a.mean(axis=0))
    
    axs.hist(y[:,0],16,histtype='stepfilled',color=color,alpha=0.5,label='{}'.format(training))
    axs.set_xlim(2.2,2.8)

    return E.mean(axis=0)


###############################################################################
# Function running preprocessing
###############################################################################
def preTwice(functional,tolerance,reference,name,sampler,sampler2):
    training = [20,100]
    color = ['b','r']
    
    fig,axs = plt.subplots(1,3,sharey=True,squeeze=True,figsize=(12,4))
    plt.locator_params(axis='x',nbins=6)
    
    for i in range(len(training)):
        preIshigami(axs,color[i],functional,tolerance,reference,training[i],sampler,[])

    plt.legend()
    fig.tight_layout()
    plt.savefig('results/sampler_mfmc_ishigami_hist_m.png')
    plt.close()


###############################################################################
# Function running without then with nonlinear map
###############################################################################
def runTwice(functional,tolerance,reference,name,sampler,sampler2):
    #budget = [40,80,160,320]
    training = [20,100]
    color = ['b','r']
    budget = [40,80,160]
    
    fig,axs = plt.subplots(1,3,sharey=True,squeeze=True,figsize=(12,4))
    plt.locator_params(axis='x',nbins=6)
    
    e = numpy.zeros((len(budget),len(reference)))
    
    for i in range(len(budget)):
        for j in range(len(training)):
            e[i,:] = runIshigami(axs[i],color[j],functional,tolerance,reference,training[j],budget[i],sampler,[])

    print "e: {}".format(e.T)

    plt.legend()
    fig.tight_layout()
    plt.savefig('results/sampler_mfmc_ishigami_hist_y.png')
    plt.close()


###############################################################################
# Run experiments
###############################################################################
print "Running mlmc on Ishigami function with scalar QoI"

tolM = 0.046
tolV = numpy.sqrt(0.078)
exV = 10.8445879407
exSm = [0.401*exV,0.288*exV,0.000*exV]
exSt = [0.712*exV,0.288*exV,0.311*exV]

sampler = SampleUniform(-m.pi,m.pi,3)
samplerS = SampleDuplicate(sampler)
sampler2 = SampleUniform(-1.5*m.pi,1.5*m.pi,3)
sampler2S = SampleDuplicate(sampler2)

runTwice(Mean(1),tolM,[2.5],'convergenceM',sampler,sampler2)
#runTwice(Variance(1),tolV,[exV],'convergenceV',sampler,sampler2)
#runTwice(Sobol([0,1,2],'main'), 0.046,exSm,'convergenceSm',samplerS,sampler2S)
#runTwice(Sobol([0,1,2],'total'),0.046,exSt,'convergenceSt',samplerS,sampler2S)

print "Test completed"

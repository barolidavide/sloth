
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *


print "Running BMFMC on 1D Laplacian"

# Create model
functional = Energy()
modelHi = Laplacian1D(functional,level=5)
modelLo = Laplacian1D(functional,level=1)
regression = GPR()
surrogate = GPR()
samplerN = SampleNormal(1.0,1.0e-1)
samplerU = SampleUniform(0.1,1.9)

# Create solver
solver = BMFMC()

# Set random variables
solver.addDistribution(samplerN)

# Create surrogate
lowfidelity = Surrogate(modelLo,modelHi,regression,samplerU,100)

# Add models
solver.addHiFidelityModel('level',modelHi)
#solver.addLoFidelityModel('coarse',modelLo)
solver.addLoFidelityModel('coarse',lowfidelity)
#solver.addLoFidelityModel('surrogate',surrogate)
#solver.addLoFidelityModel('noisy',model)

# Run solver
(mu,sigma) = solver.run(sys.argv)
err = (mu/8.3333333333333-1.0)

print "The computed mean is %f" % mu
print "Relative error = %e" % err
print "Test completed"


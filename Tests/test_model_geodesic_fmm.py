
# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
from FastMarching import *
from Geodesic import *


###############################################################################
# Main routine
###############################################################################
hole = True
        
# Create N x N grid
N = 99
x = numpy.zeros((2,N))
x[0,:] = numpy.linspace(0,1,N)
x[1,:] = numpy.linspace(0,1,N)
X = numpy.meshgrid(x[0,:],x[1,:],indexing='ij')
h = 1./(N-1)
        
# Create mask
mask = numpy.logical_and(abs(X[0]-0.5) < 0.05, abs(X[1]-0.5) < 0.45)
if hole==False : mask = numpy.zeros_like(mask)

# Create level set
phi = numpy.ones_like(X[0])
phi = numpy.ma.MaskedArray(phi, mask)

# Set starting point
x0 = [0,30]
phi[tuple(x0)] = 0

# Compute distance
model = FastMarching()
f = model.findSolution(phi,h)

# Compute gradient
g = numpy.gradient(f)

# Plot
plt.contourf(X[0],X[1],f,20)
plt.plot(X[0][tuple(x0)],X[1][tuple(x0)],'ro')
plt.quiver(X[0],X[1],-g[0],-g[1])

# Compute path
f[mask] = 100000
geodesic = Geodesic(h,2)
path = geodesic.computePath(f)
L = geodesic.findSolution()

# Plot
for i in range(path.shape[0]):
    plt.plot(X[0][tuple(path[i,:])],X[1][tuple(path[i,:])],'bo')

# Do integral
print L
print f[tuple(path[0,:])]
print "Error %2.2f%%" % (100.*(L/f[tuple(path[0,:])]-1.))

# Show figure
plt.savefig('results/model_geodesic_fmm.png')
plt.close()




import sys
import numpy
sys.path.append('../Surrogates')
from SklearnPyMC import NPR
import matplotlib
import matplotlib.pyplot as plt


###############################################################################
# Main routine running MCMC
###############################################################################

# Parameters
nRE = [12, 25, 50, 100, 200]
err = []
x = numpy.linspace(0, 1, 100)
sigma = 1.0e-1

# Create model
regression = NPR()

f, ax = plt.subplots(1,5)
i = 0

# Main loop
for nS in nRE :
    print ""

    # Data points
    xS = numpy.linspace(0, 1, nS)
    xi = xS.T
    yi = xS.T + numpy.random.normal(0.,sigma,nS)

    # Fit
    (mu,sigma_h) = regression.fit(xi,yi,x)
    
    var_h = sigma_h
    
    # Errors
    err_i = numpy.linalg.norm(mu-x)/numpy.linalg.norm(x)
    std_i = numpy.mean(sigma_h)
    var_i = numpy.mean(var_h)
    err = numpy.append(err,err_i)
    print "Data points = %d" % nS
    print "Sigma = %f" % std_i
    print "Variance = %f" % var_i
    print "Relative mu error = %e" % err_i

    # Plot
    ax[i].plot(x,mu)
    ax[i].plot(xi,yi,'.')
    ax[i].plot(x,x,'r--')
    ax[i].fill_between(x,mu-1.96*sigma_h,mu+1.96*sigma_h,alpha=0.5)
    i = i+1

# Convergence rate
for i in range(1,err.size):
    rate = numpy.log(err[i-1]/err[i])/numpy.log(2.0)
    print 'Convergence rate: %f' % rate

# Plot
f.savefig('results/surrogate_convergence_pymc.png')
plt.close()
fig = plt.figure()
plt.plot(nRE,err)
plt.savefig('results/surrogates_convergence_pymc_error.png')
plt.close()



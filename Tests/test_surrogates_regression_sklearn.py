
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *
from Linear import LR

print "Running sklearn regression test"

# Generate data
N = 10
x = numpy.linspace(0,10,N)
y = x + numpy.random.normal(0.0,3e-1,N)

f, (ax1,ax2) = plt.subplots(1,2)

# Fit using Gaussian Process Regression
gpReg = GPR()
gpReg.fit(x,y)
(yFit,sigma) = gpReg.eval(x)

ax1.plot(x,y,'r.')
ax1.plot(x,yFit,'g-')
ax1.fill(numpy.concatenate([x, x[::-1]]),
         numpy.concatenate([yFit - 1.9600 * sigma,
                           (yFit + 1.9600 * sigma)[::-1]]),
    alpha=.5, fc='b', ec='None', label='95%')

# Fit using Bayesian Linear Regression
linReg = LR()
(yFit,sigma) = linReg.fit(x,y,x)

ax2.plot(x,y,'r.')
ax2.plot(x,yFit,'g-')
ax2.fill(numpy.concatenate([x, x[::-1]]),
         numpy.concatenate([yFit - 1.9600 * sigma,
                           (yFit + 1.9600 * sigma)[::-1]]),
    alpha=.5, fc='b', ec='None', label='95%')

# Show fit
ax2.set_title('Linear')
ax1.set_title('Gaussian Process')
ax2.legend(('Data','Linear','95%'),loc=0)
ax1.legend(('Data','Nonpar','95%'),loc=0)
plt.savefig('results/regression_sklearn.png')
plt.close()

print "Test completed"


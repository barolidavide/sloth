import sys
sys.path.append('../Scripts')
from config import *
from scipy import stats
import matplotlib.pyplot as plt
import scipy.io
import netCDF4
import cmath
import numpy as np

###############################################################################
# Calculate Pearson correlation factor
###############################################################################

num_samples = 20
nc_for_size = netCDF4.Dataset('results/Eikonal1.e','r+')
tmp = numpy.asarray(nc_for_size.variables["vals_nod_var2"])
mesh_size = tmp[0].shape[0]

vals_monodomain = np.zeros((num_samples, mesh_size))
vals_eikonal = np.zeros((num_samples, mesh_size))


for i in range(num_samples):
    monodomain_file =  'results/Monodomain'+ str(i+1) + '_out.e'
    eikonal_file =  'results/Eikonal'+ str(i+1) + '.e'   ###### ATTENTION ICI !!
    
    nc_monodomain = netCDF4.Dataset(monodomain_file,'r+')
    nc_eikonal = netCDF4.Dataset(eikonal_file,'r+')
    
    vals_monodomain[i,:] = numpy.asarray(nc_monodomain.variables["vals_nod_var2"])
    vals_eikonal[i,:] = numpy.asarray(nc_eikonal.variables["vals_nod_var2"])


sample_mean_monodomain = vals_monodomain.mean(axis=0)
sample_mean_eikonal = vals_eikonal.mean(axis=0)

tmp_monodomain = vals_monodomain - sample_mean_monodomain
tmp_eikonal = vals_eikonal - sample_mean_eikonal

variance_monodomain = np.multiply(tmp_monodomain,tmp_monodomain).sum(axis=0)
variance_eikonal = np.multiply(tmp_eikonal,tmp_eikonal).sum(axis=0)
covariance = np.multiply(tmp_monodomain,tmp_eikonal).sum(axis=0)

Pearson_corr = np.divide( covariance ,  np.multiply( np.sqrt(variance_monodomain) , np.sqrt(variance_eikonal) ) )

squared_mean_monodomain = np.multiply(tmp_monodomain,tmp_monodomain).sum(axis=0)
squared_mean_eikonal = np.multiply(tmp_eikonal,tmp_eikonal).sum(axis=0)


###############################################################################
# Calculate Pearson correlation factor
###############################################################################

output_correlation = netCDF4.Dataset('results/ActivationTime/Pearson.e','r+')
output_correlation.variables["vals_nod_var1"][0]=Pearson_corr
output_correlation.close()

output_monodomain_variance = netCDF4.Dataset('results/ActivationTime/Variance_Monodomain.e','r+')
output_monodomain_variance.variables["vals_nod_var1"][0]=variance_monodomain
output_monodomain_variance.close()

output_eikonal_variance = netCDF4.Dataset('results/ActivationTime/Variance_Eikonal.e','r+')
output_eikonal_variance.variables["vals_nod_var1"][0]=variance_eikonal
output_eikonal_variance.close()

output_covariance = netCDF4.Dataset('results/ActivationTime/Covariance.e','r+')
output_covariance.variables["vals_nod_var1"][0]=covariance
output_covariance.close()


################################################################################
## Scatter plots per mesh positions
################################################################################
#
#negative_correlation_monodomain = vals_monodomain[:,17]  #17  # 260
#negative_correlation_eikonal = vals_eikonal[:,17]
#
#area1 = 0.25*np.ones((1, num_samples))
##x = np.arange(6.7, 7.0, 0.1)
##y = -x + 9
##plt.plot(x,y)
#plt.scatter(negative_correlation_eikonal,negative_correlation_monodomain,c=area1,s=100,marker='s')
#plt.savefig('results/negative_correlation.png')
#plt.close()
#
#
#negative_correlation_monodomain = vals_monodomain[:,67323]   # 67323
#negative_correlation_eikonal = vals_eikonal[:,67323]
#
#area1 = 0.25*np.ones((1, num_samples))
##x = np.arange(6.7, 7.0, 0.1)
##y = -x + 9
##plt.plot(x,y)
#plt.scatter(negative_correlation_eikonal,negative_correlation_monodomain,c=area1,s=100,marker='s')
#plt.savefig('results/positive_correlation.png')
#plt.close()

###############################################################################
# Scatter plot per sample
###############################################################################


###############################################################################
#for i in range(num_samples):
#    sample_to_plot = i
#    sample_monodomain = vals_monodomain[sample_to_plot,:]
#    sample_eikonal = vals_eikonal[sample_to_plot,:]
#
#    area1 = 0.25*np.ones((1, mesh_size))
#
#    plt.scatter(sample_eikonal,sample_monodomain,c=area1,s=1,marker='s')
#    file =  'results/sample_plot'+ str(i+1) + '.png'
#    plt.savefig('results/sample_plot.png')
#    plt.close()
#
#    slope, intercept , r_value, p_value, std_err = stats.linregress(sample_eikonal,sample_monodomain)
#    print ('Sample ', i, ', slope = ', slope)
#    print ('Sample ', i, ', intercept = ', intercept)

###############################################################################

#sample_monodomain = np.var(vals_monodomain, axis=0) #np.sqrt(variance_monodomain)
#sample_eikonal = np.var(vals_eikonal, axis=0)  #np.sqrt(variance_eikonal)

#sample_monodomain = np.var(vals_monodomain, axis=0)
#sample_eikonal = np.var(vals_eikonal, axis=0)
#
#area1 = 0.25*np.ones((1, mesh_size))
#    
#plt.scatter(sample_eikonal,sample_monodomain,c=area1,s=1,marker='s')
#file =  'results/sample_plot.png'
#plt.savefig(file)
#plt.close()
#slope, intercept , r_value, p_value, std_err = stats.linregress(sample_eikonal,sample_monodomain)
#print slope
#print intercept

###############################################################################


#mesh_position = 67323
#sample_monodomain = vals_monodomain[:,mesh_position]
#sample_eikonal = vals_eikonal[:,mesh_position]
#
#area1 = 0.25*np.ones((1, num_samples))
#
#plt.scatter(sample_eikonal,sample_monodomain,c=area1,s=1,marker='s')
#plt.savefig('results/sample_plot.png')
#plt.close()
#
#slope, intercept , r_value, p_value, std_err = stats.linregress(sample_eikonal,sample_monodomain)
#print slope
#print intercept



#################################################################################
#################################################################################
#################################################################################
#
################################################################################
## Calculate Eikonal Variance only
################################################################################

#num_samples = 20
#nc_for_size = netCDF4.Dataset('results/ActivationTime/Eikonal1a.e','r+')
#tmp = numpy.asarray(nc_for_size.variables["vals_nod_var2"])
#mesh_size = tmp[0].shape[0]
#
#vals_eikonal = np.zeros((num_samples, mesh_size))
#
#
#for i in range(num_samples):
#    eikonal_file =  'results/ActivationTime/Eikonal'+ str(i+1) + 'a.e'
#    nc_eikonal = netCDF4.Dataset(eikonal_file,'r+')
#    vals_eikonal[i,:] = numpy.asarray(nc_eikonal.variables["vals_nod_var2"])
#
#
#sample_mean_eikonal = vals_eikonal.mean(axis=0)
#tmp_eikonal = vals_eikonal - sample_mean_eikonal
#variance_eikonal = np.multiply(tmp_eikonal,tmp_eikonal).sum(axis=0)
#
#
################################################################################
## Output
################################################################################
#
#output_eikonal_variance = netCDF4.Dataset('results/ActivationTime/Variance_Eikonal_second.e','r+')
#output_eikonal_variance.variables["vals_nod_var1"][0]=np.sqrt(variance_eikonal)
#output_eikonal_variance.close()


#################################################################################
#################################################################################
#################################################################################


################################################################################
################################################################################
################################################################################

###############################################################################
# Calculate Monodomain Variance only
###############################################################################
#
#num_samples = 40
#nc_for_size = netCDF4.Dataset('results/ActivationTime/Monodomain1.e','r+')
#tmp = numpy.asarray(nc_for_size.variables["vals_nod_var2"])
#mesh_size = tmp[0].shape[0]
#
#vals_monodomain = np.zeros((num_samples, mesh_size))
#
#
#for i in range(num_samples):
#    monodomain_file =  'results/ActivationTime/Monodomain'+ str(i+1) + '.e'
#    nc_monodomain = netCDF4.Dataset(monodomain_file,'r+')
#    vals_monodomain[i,:] = numpy.asarray(nc_monodomain.variables["vals_nod_var2"])
#
#
#sample_mean_monodomain = vals_monodomain.mean(axis=0)
#tmp_monodomain = vals_monodomain - sample_mean_monodomain
#variance_monodomain = np.multiply(tmp_monodomain,tmp_monodomain).sum(axis=0)
#
#
################################################################################
## Output
################################################################################
#
#output_monodomain_variance = netCDF4.Dataset('results/ActivationTime/Variance_Monodomain_second.e','r+')
#output_monodomain_variance.variables["vals_nod_var1"][0]=np.sqrt(variance_monodomain)
#output_monodomain_variance.close()


################################################################################
################################################################################
################################################################################




################################################################################
## Calculate Pearson correlation factor
################################################################################
#
#nc_monodomain = netCDF4.Dataset('../Scripts/ActivationTime/Monodomain18.e','r+')
#nc_eikonal = netCDF4.Dataset('../Scripts/ActivationTime/Eikonal18.e','r+')
#
#vals_monodomain = numpy.asarray(nc_monodomain.variables["vals_nod_var2"])
#vals_eikonal = numpy.asarray(nc_eikonal.variables["vals_nod_var2"])
#
#mean_monodomain = 0
#mean_eikonal = 0
#var_monodomain = 0
#var_eikonal = 0
#correlation = 0
#
##print vals_monodomain[0].shape[0]
#
#mesh_size = vals_monodomain[0].shape[0]
#
#for i in range(mesh_size):
#    mean_monodomain += vals_monodomain[0][i]
#    mean_eikonal += vals_eikonal[0][i]
#
#mean_monodomain /= mesh_size
#mean_eikonal /= mesh_size
#
#print mean_monodomain
#print mean_eikonal
#
#for i in range(mesh_size):
#    var_monodomain += (vals_monodomain[0][i] - mean_monodomain)*(vals_monodomain[0][i] - mean_monodomain)
#    var_eikonal += (vals_eikonal[0][i] - mean_eikonal)*(vals_eikonal[0][i] - mean_eikonal)
#    correlation += (vals_monodomain[0][i] - mean_monodomain)*(vals_eikonal[0][i] - mean_eikonal)
#
#
#
#
#corr_factor = correlation/(cmath.sqrt(var_monodomain*var_eikonal))
#
#print("Correlation factor between the two samples is: ", correlation)
#print("Variance for monodomaion is: ", cmath.sqrt(var_monodomain))
#print("Variance for eikonal is: ", cmath.sqrt(var_eikonal))
#print("The pearson correlation factor is: ", corr_factor)

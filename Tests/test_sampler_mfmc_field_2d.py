
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a field output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Running mlmc on 1D Laplacian with vector QoI"

# Mesh
x = numpy.linspace(0.1, 0.9, 9);
y = numpy.linspace(0.1, 0.9, 9);

# Set maximum resolution
L = 4

# Create model
functional = Vector2D(x,y)
modelHi = Laplacian2D(functional,level=L)

# Create solver
solver = MFMC()

# Set random variables
sampler = SampleNormal(1.0,1.0e-1)
solver.addDistribution(sampler)

# Set high-fidelity model
solver.addHiFidelityModel('fine',modelHi)

# Set all other models
for l in range(L):
    modelLo = Laplacian2D(functional,level=l);
    solver.addLoFidelityModel('coarse',modelLo)

# Run solver
u = solver.run(sys.argv,1)

# Plot solution
modelHi = Laplacian2D(functional,5)
solver.addHiFidelityModel('fine',modelHi)
uMean = solver.meanSolution()
functional.plotSolution(uMean,u)

print "Test completed"


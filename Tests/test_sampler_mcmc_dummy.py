
import sys
sys.path.append('../Scripts')
from config import *

print "Running MCMC on dummy model"

# Starting value
startValue = numpy.array([4,2.3]);

# Parameters
it = 100000
burnIn = 5000
nObs = 100
nPar = 2

# Create model
functional = Scalar()
model = Dummy(functional);

# Create solver
solver = MCMC(model,startValue)

# Set random variables
solver.addDistribution('normal',[1.0,1.0e-1])

# Create synthetic data
exactValue = numpy.array([5.3,2.7])
k = numpy.random.normal(exactValue[0],exactValue[1],nObs)
data = model.findSolution(k)
solver.setData(data)

# Run solver
chain = solver.run(sys.argv,it)

# Plot histogram
plt.hist(chain[burnIn:it,0],bins=50)
plt.savefig('results/inverse_abc_dummy_k0.png')
plt.close()

plt.hist(chain[burnIn:it,1],bins=50)
plt.savefig('results/inverse_abc_dummy_k1.png')
plt.close()

print "Test completed"


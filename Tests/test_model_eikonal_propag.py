
# @file	test_propag_eikonal_propag.cxx
# @brief	This file contains a test of Propag
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	13 February 2018

import sys
sys.path.append('../Scripts')
from config import *
from mpl_toolkits.mplot3d import axes3d
from Propag import *
from FastMarching import *

print "Running propag with angle input with cube geometry"

sigma = 1
hole = False

# Create propag
propag = Propag('propag',functional='full',reuse=True)

# Initialize propag
propag.loadPatient('cube')
alpha0 = propag.getMean()

# Create geometry
X = propag.getMesh()
h = propag.getSpacing()

# Create mask
mask = propag.getMask()

# Create random field
dist = FastMarching()
field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)
propag.setRandomField(field)

# Run propag
f = propag.findSolution(numpy.zeros([2,field.m]),'full')

# Plot
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(X[0],X[1],X[2],c=f[0], depthshade=False, alpha=0.05)

# Show figure
plt.savefig('results/test_eikonal_propag.png')
plt.close()

print "Test completed"


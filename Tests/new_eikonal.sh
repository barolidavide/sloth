#!/bin/bash


for i in {1..20}
do
mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i UserObjects/KL/mesh=results/RandomFields/random${i}.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/tau=70 Kernels/Eikonal/c0=10 #Outputs/file_base=../Scripts/Eikonal${i} #Mesh/file=results/lev_1.e

mpirun -n 1 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/getSolution.i Mesh/file=results/RandomFields/random1.e UserObjects/MC_mean_on_coarse_mesh/mesh=../Scripts/MC_eikonal_random_out.e Executioner/end_time=1 Outputs/file_base=results/Eikonal${i}
#


#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_monodomain_random.i UserObjects/KL/mesh=results/RandomFields/random${i}.e Outputs/file_base=results/Monodomain${i}

#mpirun -n 1 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/getSolution.i Mesh/file=results/RandomFields/random1.e UserObjects/MC_mean_on_coarse_mesh/mesh=results/Monodomain${i}.e Executioner/end_time=40 Outputs/file_base=results/Monodomain${i}_out

done

#!/bin/bash



mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=0.5 Outputs/file_base=results/Eikonal/c0=0.5



mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.0 Outputs/file_base=results/Eikonal/c0=1.0



mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Outputs/file_base=results/Eikonal/c0=1.5



mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=2.0 Outputs/file_base=results/Eikonal/c0=2.0




mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=2.5 Outputs/file_base=results/Eikonal/c0=2.5




mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=3.0 Outputs/file_base=results/Eikonal/c0=3.0



mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=3.5 Outputs/file_base=results/Eikonal/c0=3.5


mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=4.0 Outputs/file_base=results/Eikonal/c0=4.0


mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=4.5 Outputs/file_base=results/Eikonal/c0=4.5


mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=5.0- Outputs/file_base=results/Eikonal/c0=5.0

mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/MC_eikonal_random.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=5.5 Outputs/file_base=results/Eikonal/c0=5.5

#######################################################################################################################################################################



#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=0.5 Outputs/file_base=results/Eikonal/tau=0.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=1.0 Outputs/file_base=results/Eikonal/tau=1.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=1.5 Outputs/file_base=results/Eikonal/tau=1.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/ssub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=2.0 Outputs/file_base=results/Eikonal/tau=2.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=2.5 Outputs/file_base=results/Eikonal/tau=2.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=3.0 Outputs/file_base=results/Eikonal/tau=3.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=3.5 Outputs/file_base=results/Eikonal/tau=3.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=4.0 Outputs/file_base=results/Eikonal/tau=4.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e UserObjects/KL/mesh=results/random_field_copy.e AuxKernels/MonteCarloMean/num_samples=1 AuxKernels/Variance/num_samples=1 Executioner/end_time=1 Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=4.5 Outputs/file_base=results/Eikonal/tau=4.5

#####################################################################################################################################################################

#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=0.5 Outputs/file_base=results/Eikonal/tau=0.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=1.0 Outputs/file_base=results/Eikonal/tau=1.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=1.5 Outputs/file_base=results/Eikonal/tau=1.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=2.0 Outputs/file_base=results/Eikonal/tau=2.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=2.5 Outputs/file_base=results/Eikonal/tau=2.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=3.0 Outputs/file_base=results/Eikonal/tau=3.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=3.5 Outputs/file_base=results/Eikonal/tau=3.5
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=4.0 Outputs/file_base=results/Eikonal/tau=4.0
#
#mpirun -n 4 /Users/seifbb/Projects/MooseWhale/whale/whale-opt -i ../Scripts/sub.i Mesh/file=results/lev_1.e  Kernels/Eikonal/c0=1.5 Kernels/Eikonal/tau=4.5 Outputs/file_base=results/Eikonal/tau=4.5

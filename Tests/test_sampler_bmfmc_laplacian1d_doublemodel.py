
# @file	test_2d_regression.cxx
# @brief	This file tests 2 low-fidelity models
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *


print "Running BMFMC on 1D Laplacian with two low-fidelity models"

# Create model
functional = Energy()
model = Laplacian1D(functional,4)
regression = GPR()
surrogate = SVM()
samplerN = SampleNormal(1.0,1.0e-1)
samplerU = SampleUniform(0.1,1.9)

# Create solver
solver = BMFMC()

# Set random variables
solver.addLoDistribution(samplerN)
solver.addHiDistribution(samplerU)

# Add models
solver.addHiFidelityModel('fine',model)
solver.addLoFidelityModel('surrogate',surrogate)
solver.addLoFidelityModel('noisy',model)

# Add regressions
solver.addRegression(regression)

# Run solver
(mu,sigma) = solver.run(sys.argv)
err = (mu/8.3333333333333-1.0)

print "The computed mean is %f" % mu
print "Relative error = %e" % err
print "Test completed"


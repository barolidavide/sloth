
# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal MOOSE model
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import sys
sys.path.append('../Scripts')
from config import *
from SampleFieldMesh import *
from MOOSE import *



###############################################################################
# Properties
###############################################################################
sigma = 0.15
N = numpy.array([5,5,5]) # 3 18
domain = numpy.array([[0,1],[0,1],[0,1]])

###############################################################################
# Create KL
###############################################################################
modelKL = MOOSE('none',3,)
modelKL.createMesh(N,domain,modelKL.fine_mesh)

# Create functional for output
Ndof = modelKL.getNumberOfNodes()
functional = Pointwise(Ndof)

# Create random field
kernel_dim = 3
field = SampleFieldMesh(sigma,modelKL,kernel_dim)

# Take one sample from distribution
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
Y = numpy.zeros((1,field.m))
Y[0,:] = sampler.sample()


###############################################################################
# Run monodomain space-time
###############################################################################
#model = MOOSE(functional,1,"../Scripts/MC_monodomain_ST")
#model.createMesh(N,domain,"lev_1")
#
#model.setFakeTimeStepping()
#
#
## Create random field
#model.setRandomField(field)
#
## Run one sample
#f = model.findSolution(Y)
#
#plt.scatter(field.X[:,0],field.X[:,1],c=f,s=100,marker='s')
#plt.savefig('results/model_moose_monodomain_ST.png')
#plt.close()


###############################################################################
# Run monodomain Sonia
###############################################################################
#model = MOOSE(functional,1,"../Scripts/MC_monodomain_random")
#model.createMesh(N,domain,"lev_1")
#
## Create random field
#model.setRandomField(field)
#
## Run one sample
#f = model.findSolution(Y)
#
#plt.scatter(field.X[:,0],field.X[:,1],c=f,s=100,marker='s')
#plt.savefig('results/model_moose_monodomain.png')
#plt.close()


###############################################################################
# Run eikonal
###############################################################################
#model = MOOSE(functional,1,"../Scripts/MC_eikonal_random")
#model.createMesh(N,domain,"lev_1")
#
#model.setFakeTimeStepping()
#
## Create random field
#model.setRandomField(field)
#
## Run one sample
#f = model.findSolution(Y)
#
#plt.scatter(field.X[:,0],field.X[:,1],c=f,s=100,marker='s')
#plt.savefig('results/model_moose_eikonal.png')
#plt.close()


# @file	test_model_surrogate_ShortColumn.py
# @brief	This file tests the implementation of a surrogate model for the ShortColumn function
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Computing GPR surrogate of ShortColumn function"

###############################################################################
# Create models and surrogate
###############################################################################

# Create model
functional = Pointwise(1)
modelHi = ShortColumn(functional,level=0);

# Set random variables
sampler = SampleVector([['Uniform',1,20],['Uniform',10,30],['Lognormal',5,1],
                        ['Normal',2000,800],['Normal',500,200]])

# Create regression
regression = GPR()

# Create low-fidelity model
modelLo = ShortColumn(functional,level=1)

# Create surrogate
surrogate1 = Surrogate(modelLo,modelHi,regression,sampler,300)


###############################################################################
# Do scatter plot to compare plain low fidelity and surrogate
###############################################################################

# Create samples
sampler = SampleVector([['Uniform',5,15],['Uniform',15,25],['Lognormal',5,0.5],
                        ['Normal',2000,400],['Normal',500,100]])
k = numpy.zeros((200,sampler.n))
for i in range(200): k[i,:] = sampler.sample()

# Eval surrogate
surrogate1.findSolution(k,True)

# Plot results
idx = numpy.argsort(surrogate1.x[:,0])
xPlot = surrogate1.x[idx,0]
yPlot = surrogate1.f[idx,0]
sPlot = surrogate1.sigma[idx]

plt.scatter(surrogate1.x0,surrogate1.y0,c='b',label='Training')
plt.scatter(surrogate1.x,surrogate1.y,c='r',label='Posterior')
plt.scatter(surrogate1.x,surrogate1.f,c='g',label='Prediction')
#plt.fill(numpy.concatenate([xPlot[:], xPlot[::-1]]),
#         numpy.concatenate([yPlot[:] - 1.9600 * sPlot,
#                           (yPlot[:] + 1.9600 * sPlot)[::-1]]),
#                       alpha=.5, fc='0.75', ec='None', label='95%')
plt.legend(loc='upper left')
plt.xlabel('Low-fidelity output')
plt.ylabel('High-fidelity output')
plt.savefig('results/model_surrogate_ShortColumn.png')
plt.close()

rhof = numpy.corrcoef(surrogate1.f,modelHi.findSolution(k),rowvar=False)
rhoy = numpy.corrcoef(surrogate1.y,modelHi.findSolution(k),rowvar=False)
rhox = numpy.corrcoef(surrogate1.x,modelHi.findSolution(k),rowvar=False)

plt.scatter(surrogate1.y,modelHi.findSolution(k),c='r',label='posterior vs highF r={}'.format(rhoy[0,1]))
plt.scatter(surrogate1.x,modelHi.findSolution(k),c='b',label='identity vs highF r={}'.format(rhox[0,1]))
plt.scatter(surrogate1.f,modelHi.findSolution(k),c='g',label='prediction vs highF r={}'.format(rhof[0,1]))
plt.legend(loc='upper left')
plt.xlabel('Low-fidelity output')
plt.ylabel('Surrogate output')
plt.gca().set_xlim([numpy.amin(surrogate1.f),numpy.amax(surrogate1.f)])
plt.gca().set_ylim([numpy.amin(surrogate1.f),numpy.amax(surrogate1.f)])
plt.savefig('results/model_surrogate_ShortColumn_correlation.png')
plt.close()

print "Test completed"

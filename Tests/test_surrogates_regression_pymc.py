
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Surrogates')
from LinearPyMC import LR
from SklearnPyMC import NPR
import matplotlib.pyplot as plt
import numpy



###############################################################################
# Main routine
###############################################################################

# Generate data
x = numpy.linspace(0,1,50)
y = x + numpy.random.normal(0.0,1e-1,50)

f, (ax1,ax2) = plt.subplots(1,2)

# Fit using Linear Regression computed via MC
linReg = LR()
(yFit,sigma) = linReg.fit(x,y,x)

ax1.plot(x,y,'r.')
ax1.plot(x,x,'r--')
ax1.plot(x,yFit,'g-')
ax1.fill(numpy.concatenate([x, x[::-1]]),
         numpy.concatenate([yFit - 1.9600 * sigma,
                           (yFit + 1.9600 * sigma)[::-1]]),
         alpha=.5, fc='b', ec='None', label='95%')

# Fit using Nonparametric Regression
npReg = NPR()
(yFit,sigma) = npReg.fit(x,y,x)

ax2.plot(x,y,'r.')
ax2.plot(x,x,'r--')
ax2.plot(x,yFit,'g-')
ax2.fill(numpy.concatenate([x, x[::-1]]),
         numpy.concatenate([yFit - 1.9600 * sigma,
                           (yFit + 1.9600 * sigma)[::-1]]),
         alpha=.5, fc='b', ec='None', label='95%')

# Show fit
ax1.set_title('Linear')
ax2.set_title('Gaussian Process')
plt.savefig('results/surrogates_regression_pymc.png')
plt.close()

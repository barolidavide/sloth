
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a field output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *
import scipy.interpolate
from ECG import Model

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Functional:
    
    def __init__(self):
        # Locations where to compute the output
        self.x = numpy.linspace(0.1, 0.9, 9);
        self.y = numpy.linspace(0.1, 0.9, 9);
        self.nP = self.x.size * self.y.size
    
    def getNumberOutputs(self): return self.nP
				
    def eval(self,xi,yi,uh):

        # Use nodal values
        z = numpy.reshape(uh,(xi.size,yi.size))
        F = scipy.interpolate.interp2d(xi,yi,z);
        
        self.x = xi
        self.y = yi
        
        # Return result
        return F(self.x,self.y)

    def plotSolution(self,u,ecg):
        
        xi,yi = numpy.meshgrid(self.x,self.y)

        # Plot solution
        f,(ax1,ax2) = plt.subplots(1,2)
        ax1.axis([0,1,0,1])
        ax1.contourf(xi,yi,u)
        ax2.plot(ecg)
        plt.ion()
        plt.savefig('results/model_eikonal_ecg.png')
        plt.close()


        

print "Computing the ECG"


# Create model
functional = Functional()
model = Model(functional)

# Define time steps
N = 1
ecg = []
activation = numpy.append(numpy.linspace(0.39,0.61,N),numpy.linspace(0.39,0.61,N))

# Define geometry
l = 5
nEx = 2 * pow(2,l)
nEy = 2 * pow(2,l)
nx = nEx-1
ny = nEy-1
nV = nx * ny
nHx = nx / 4
nHy = ny / 4
h = 1.0/nx;
xi = numpy.linspace(h, 1-h, nx);
yi = numpy.linspace(h, 1-h, ny);

# Loop over time steps
for xA in activation:
    
    i0 = 0
    i1 = 1*nHx/2+0*nHy/2
    i2 = 3*nHx/2+2*nHy/2
    i3 = 4*nHx/2+4*nHy/2
    
    # Numbering in full domain
    xL,yT = (nx-nHx)/2 , (ny-nHy)/2
    xR,yB = (nx+nHx)/2 , (ny+nHy)/2
    
    v1 = xL + yT * nx
    v2 = xR + yT * nx
    v3 = xR + yB * nx
    v4 = xL + yB * nx
    
    # Numbering in heart domain
    vH0 = 0
    vH1 = 1*nHx+0*nHy
    vH2 = 1*nHy+1*nHx
    vH3 = 2*nHx+1*nHy
    vH4 = 2*nHx+2*nHy
    
    # Define potential
    acP = numpy.zeros(nx*ny)
    bcH = numpy.arange(0,2*(nHx+nHy),1)
    
    bcH[vH0:vH1] = numpy.arange(v1,v2,  1)
    bcH[vH1:vH2] = numpy.arange(v2,v3, nx)
    bcH[vH2:vH3] = numpy.arange(v3,v4, -1)
    bcH[vH3:vH4] = numpy.arange(v4,v1,-nx)
    
    for i in range(xL,xR):
        if( xi[i ]>xA ):  acP[i+yT*nx] = -pow(h,2)*100
        else:             acP[i+yT*nx] =  pow(h,2)*100
    
    for i in range(yT,yB+1):
        if( xi[xR]>xA ):  acP[xR+i*nx] = -pow(h,2)*100
        else:             acP[xR+i*nx] =  pow(h,2)*100

    for i in range(xL,xR):
        if( xi[i ]>xA ):  acP[i+yB*nx] = -pow(h,2)*100
        else:             acP[i+yB*nx] =  pow(h,2)*100

    for i in range(yT,yB):
        if( xi[xL]>xA ):  acP[xL+i*nx] = -pow(h,2)*100
        else:             acP[xL+i*nx] =  pow(h,2)*100

    model.setPotential(acP,bcH)

    # Run model
    uMean = model.findSolution(l,1.0)

    # Get solution at given point
    ecg.append(uMean[ny/2,nx-1])

    # Plot solution
    functional.plotSolution(uMean,ecg)


print "Test completed"


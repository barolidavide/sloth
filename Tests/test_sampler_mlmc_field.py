
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a field output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Running mlmc on 1D Laplacian with vector QoI"

# Mesh
x = numpy.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);

# Initial number of samples
N0 = numpy.array([64,16,4]);

# MLMC parameters
(alpha,beta,gamma) = (-2,-4,+1)
tol = 1e-2

# Create solver
solver = MLMC(tol)

# Set exponents
solver.setExponents(alpha,beta,gamma)

# Set random variables
sampler = SampleNormal(1.0,1.0e-1)
solver.addDistribution(sampler)

# Initial number of models
functional = Vector1D(x)

for l in range(3):
    modelLo = Laplacian1D(functional,l);
    solver.addLoFidelityModel('coarse',modelLo)

# Run solver
u = solver.run(sys.argv,N0)

# Plot solution
modelHi = Laplacian1D(functional,10)
solver.addHiFidelityModel('fine',modelHi)
uMean = solver.meanSolution()
functional.plotSolution(u,uMean)

print "Test completed"


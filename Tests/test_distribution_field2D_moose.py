
# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
from SampleFieldMesh import *
from MOOSE import *


###############################################################################
# Properties
###############################################################################
sigma = 0.15 # correlation
N = numpy.array([5,5,5]) # 3 18
domain = numpy.array([[0,1],[0,1],[0,1]])

###############################################################################
# Create KL
###############################################################################
modelKL = MOOSE('none',3,N,domain)
(c,X) = modelKL.getMesh()
MassMatrix = modelKL.getMassMatrix()

# Create functional for output
Ndof = modelKL.getNumberOfNodes()
functional = Pointwise(int(Ndof))

# Create random field
field = SampleFieldMesh(sigma,c,X,MassMatrix)

# Take one sample from distribution
sampler = SampleNormal(numpy.zeros(field.m),numpy.identity(field.m),field.m)
(X,phi) = field.sample(sampler.sample())
phi = 0.01*phi # for now, just to avoid negative diffusion values


# Write fibers to solution
nc = netCDF4.Dataset('results/sub_0.e','r+')
nc.variables["vals_nod_var1"][0]=phi
nc.close()
            
            
        
        



# Test
#p1 = numpy.asarray([0.35,0.5])
#p2 = numpy.asarray([0.65,0.5])
#field.testSamples(p1,p2,x)

plt.scatter(X[:,0],X[:,1],c=phi,s=200,marker='s')
plt.savefig('results/distribution_field2D_moose.png')
plt.close()


# @file	test_scalar.cxx
# @brief	This file contains a test of the Eikonal model with random EAS
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 October 2016

import sys
sys.path.append('../Scripts')
from config import *
from EikonalEAS import Model
from SampleBoundary import Distribution



###############################################################################
# Main routine running MLMC
###############################################################################
'''Runs models for debugging.'''

# Create model
functional = Scalar()
model = Model(functional)
sampler = Distribution()

# Run model
uMean = model.findSolution(numpy.array([sampler.sample()]))

print "Solution is {} seconds".format(uMean)


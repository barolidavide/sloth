
# @file	run_all_tests.py
# @brief	This file run all tests of SLOTH
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	5 October 2017

import argparse
import os

if not os.path.exists('results'): os.makedirs('results')

parser = argparse.ArgumentParser(description='Run with one of the following options: \n std, models, surrogates, samplers, moose, eikonal, pymc')
parser.add_argument("type")
args = parser.parse_args()

if args.type=="models":
    execfile("test_distribution_field2D.py")
    execfile("test_distribution_field3D.py")
    execfile("test_model_laplace2d.py")
    execfile("test_model_geodesic_fmm.py")
    execfile("test_model_ecg.py")
    
if args.type=="surrogates":
    execfile("test_surrogates_regression_sklearn.py")
    execfile("test_surrogates_convergence_sklearn.py")
    execfile("test_surrogates_gpr.py")

if args.type=="samplers":
    execfile("test_sampler_mlmc_field.py")
    execfile("test_sampler_mlmc_scalar.py")
    execfile("test_sampler_bmfmc_scalar.py")
    execfile("test_sampler_bmfmc_laplacian1d_doublemodel.py")
    execfile("test_sampler_bmfmc_laplacian1d_convergence.py")
    execfile("test_sampler_mcmc_laplacian1d.py")
    execfile("test_sampler_mcmc_dummy.py")

if args.type=="moose":
    execfile("test_model_moose.py")
    execfile("test_distribution_field2D_moose.py")

if args.type=="eikonal":
    execfile("test_model_eikonal_eas.py")
    execfile("test_model_eikonal_angle.py")
    execfile("test_model_eikonal_realanatomy.py")

if args.type=="pymc":
    execfile("test_solvers_laplacian.py")
    execfile("test_surrogates_regression_pymc.py")
    execfile("test_surrogates_convergence_pymc.py")


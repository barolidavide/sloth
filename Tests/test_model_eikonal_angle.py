# @file	test_angle.cxx
# @brief	This file contains a test of the Eikonal model with random fiber angle
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	27 February 2017

import sys
sys.path.append('../Scripts')
from config import *
from mpl_toolkits.mplot3d import axes3d
from EikonalCUDA import *
from Geodesic import *
from FastMarching import *
from Reduced1D import *
from ActivationSite import *

print "Running eikonal model with angle input with slab geometry"

sigma = 2
hole = False
x0 = [0,20,20]

# Create model
model = EikonalCUDA('alpha',functional='none',reuse=False)

# Initialize angles
alphaMinMax = (m.pi/3.0,m.pi/3.0)
alpha0 = model.preprocess(alphaMinMax)

# Create geometry
X = model.getMesh()
h = model.getSpacing()

# Create mask
hole = False
mask = numpy.logical_and(abs(X[0]-0.5)<0.45,
       numpy.logical_and(abs(X[1]-0.5)<0.45,abs(X[2]-0.5)<0.45))
if hole==False : mask = numpy.zeros_like(mask)

# Create random field
dist = FastMarching()
field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)
model.setRandomField(field)

# Add QoI
site = ActivationSite(mask,1,1,37)
model.functional = site

# Run model
f = model.findSolution(numpy.zeros([1,field.m]),'full')
uMean = model.findSolution(numpy.zeros([1,field.m]))

print "Solution is {} seconds".format(uMean)
print "Test completed"

# Plot
x0 = [19,19,0]
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(X[0],X[1],X[2],c=f, depthshade=False, alpha=0.01)
ax.scatter(X[0][x0[0],x0[1],x0[2]],
           X[1][x0[0],x0[1],x0[2]],
           X[2][x0[0],x0[1],x0[2]],c='r')

# Compute path
geodesic = Geodesic(h,3,[1,1,37],model)

# Create reduced model
redModel = Reduced1D(geodesic,1)
redModel.fit(numpy.zeros([1,field.m]),f)
L = redModel.eval(numpy.zeros([1,field.m]))
path = geodesic.getPath()

# Plot
for i in range(path.shape[0]):
    ax.scatter(X[0][path[i,0],path[i,1],path[i,2]],
               X[1][path[i,0],path[i,1],path[i,2]],
               X[2][path[i,0],path[i,1],path[i,2]])

ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_zlim(0, 1)

# Do integral
print "Time computed from geodesic integration: %f" % L
print "Time computed from 3D activation map: %f" % uMean
print "Error %2.2f%%" % (100.*(L/uMean-1.))

# Show figure
plt.savefig('results/test_geodesic_slab.png')
plt.close()

print "Test completed"




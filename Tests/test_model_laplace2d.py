
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a field output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../scripts')
from config import *

print "Solving the 2D Laplace equation"

# Create model
x = numpy.linspace(0.0, 1.0, 10);
y = numpy.linspace(0.0, 1.0, 10);
functional = Vector2D(x,y)
model = Laplacian2D(functional,5)

# Run model
uMean = model.findSolution([1.0])
print uMean

# Plot solution
xi,yi = numpy.meshgrid(functional.x,functional.y)
    
# Plot solution
plt.contourf(xi,yi,uMean.reshape(10,10))
plt.savefig('results/laplace2d.png')
plt.close()

print "Test completed"


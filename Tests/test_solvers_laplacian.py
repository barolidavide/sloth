
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multifidelity
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../scripts')
import config
from LaplacianPyMC2 import NPR
from Laplacian1D import Model
from Vector1D import *
import matplotlib.pyplot as plt
import numpy


print "Running stochastic 1D Laplacian solver"

# Mesh
x = numpy.array([0.001,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.999]);

# Generate data
N = 10
x = numpy.linspace(0,1,N)
y = numpy.zeros(N)

# Create model
functional = Vector1D(x)
model = Model(functional)

# Plot reference
refinement = 10
yRef = model.findSolution(refinement,[0.01])
plt.plot(functional.x,yRef.T,'o',label='Reference')

# Solve Laplacian
npReg = NPR()
(res,yFit) = npReg.fit(x,y,x)

plt.plot(x,res,'-',label='Residual')
plt.plot(x,yFit,'--',label='Solution')

# Show fit
plt.legend()
plt.savefig('results/solvers_laplacian.png')
plt.close()

print "Test completed"



# @file	createRandomField.cxx
# @brief	This file generated a correlated random field
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	25 January 2017

import sys
sys.path.append('../Scripts')
from config import *
from mpl_toolkits.mplot3d import axes3d
from EikonalCUDA import *
from Geodesic import *
from FastMarching import *
from Reduced1D import *
from ActivationSite import *

print "Running eikonal model with angle input with patient geometry"

sigma = 40
hole = True
x0 = [21,22,106]

# Create model
model = EikonalCUDA('alpha')

# Initialize model
model.loadPatient('../../input/CRT001j01.par.pickle')
model.setActivationSite(*x0)
alpha0 = model.getMean()

# Create geometry
X = model.getMesh()
h = model.getSpacing()

# Create mask
hole = True
mask = model.getMask()

# Create random field
dist = FastMarching()
field = SampleFieldVoxel(sigma,dist,hole,X,h,mask)
model.setRandomField(field)

# Add QoI
site = ActivationSite(mask,104,35,103)
model.functional = site

# Run model
f = model.findSolution(numpy.zeros([1,field.m]),'full')
uMean = model.findSolution(numpy.zeros([1,field.m]))

print "Solution is {} seconds".format(uMean)

# Plot
x0 = [105,22,20]
fig = plt.figure()
ax = fig.gca(projection='3d')
#ax.scatter(X[0],X[1],X[2],c=f, depthshade=False, alpha=0.005)
ax.scatter(X[0][x0[0],x0[1],x0[2]],
           X[1][x0[0],x0[1],x0[2]],
           X[2][x0[0],x0[1],x0[2]],c='r')

# Compute path
geodesic = Geodesic(h,3,[104,35,103],model)

# Create reduced model
redModel = Reduced1D(geodesic,1)
redModel.fit(numpy.zeros([1,field.m]),f)
L = redModel.eval(numpy.zeros([1,field.m]))
path = geodesic.getPath()

# Plot
for i in range(path.shape[0]):
    ax.scatter(X[0][path[i,0],path[i,1],path[i,2]],
               X[1][path[i,0],path[i,1],path[i,2]],
               X[2][path[i,0],path[i,1],path[i,2]])

ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_zlim(0, 1)

# Do integral
functional = ActivationSite(mask,104,35,103)
print "Time computed from geodesic integration: %f" % L
print "Time computed from 3D activation map: %f" % uMean
print "Error %2.2f%%" % (100.*(L/uMean-1.))

# Show figure
plt.savefig('results/test_geodesic_realanatomy.png')
plt.close()

print "Test completed"




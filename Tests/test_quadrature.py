
# @file	test_scalar.cxx
# @brief	This file contains an implementation of quadrature
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	24 May 2018

import sys
sys.path.append('../Scripts')
from config import *
from numpy.linalg import norm
from scipy.optimize import bisect

print "Doing quadrature"


###############################################################################
# Sign functions
###############################################################################
def sgn(m,s,S,sigma):
    if m==sigma or S==True : return sigma*m
    else : return 0

def sgnL(m,s,S): return sgn(m,s,S,-1)
def sgnU(m,s,S): return sgn(m,s,S,+1)


###############################################################################
# Integrand evaluation
###############################################################################
def F(x,f,list,R,q,k):
    x1 = R[0]
    x2 = R[1]
    wq = numpy.asarray([5.,8.,5.])/18.
    xq = [-m.sqrt(3./5.),0,m.sqrt(3./5.)]
    
    for i,l in enumerate(list):
        p = lambda y: l[0](xx(x,k,y))
        try:
            y = bisect(p,x1,x2)
            R = numpy.append(R,y)
        except: pass
    
    R = numpy.sort(R)
    I = 0

    for j in range(R.shape[0]-1):
        L = R[j+1]-R[j]
        xc = xx(x,k,0.5*(R[j+1]+R[j]))
        update = True

        for i,l in enumerate(list):
            p = l[0]
            s = l[1]
            update = update and s*p(xc) >= 0.0

        if update:
            for i in range(q):
                I = I + L*wq[i]*f(xx(x,k,numpy.array(R[j]+L*xq[i])))

    return I


###############################################################################
# Integral evaluation
###############################################################################
def I(f,list,U,S,q):
    d = len(U)
    xC = numpy.mean(U,axis=1)
    id = []
    
    for i,l in enumerate(list):
        p = l[0](xC)
        s = l[1]
        if abs(p) >= 10.0 :
            if s*p >= 0.0 : id.append(i)
     
    for i in sorted(id, reverse=True): del list[i]

    if d==1: return F(numpy.array([]),f,list,U[0,:],q,0)

    k = 1
    xL = U[k,0]
    xU = U[k,1]
    phi = []

    for l in list:
        p = l[0]
        s = l[1]
        g = numpy.zeros(xC.shape)
        
        for i,xi in enumerate(xC):
            e = numpy.zeros(xC.shape)
            e[i] = 1e-8
            g[i] = (p(xC+e)-p(xC-e))*1e8
        
        phiL = lambda x: p(xx(x,k,xL))
        phiU = lambda x: p(xx(x,k,xU))
        
        sL = sgnL(numpy.sign(g[k]),s,S)
        sU = sgnU(numpy.sign(g[k]),s,S)
    
        phi.append([phiL,sL])
        phi.append([phiU,sU])
    
        ft = lambda x: F(x,f,list,U[k,:],q,k)

    return I(ft,phi,numpy.delete(U,k,axis=0),False,q)


###############################################################################
# Run test
###############################################################################

def f(x): return 1
def r(x,xC): return x-xC
def h(x,xC): return norm(r(x,xC))-1.0
def xx(x,k,y): return numpy.insert(x,k,y)

distance2D = [[lambda x: h(x,[-1,-1]),-1]]
distance3D = [[lambda x: h(x,[-1,-1,-1]),-1]]
distance4D = [[lambda x: h(x,[-1,-1,-1,-1]),-1]]
rect2D = numpy.array([[-1,1],[-1,1]])
rect3D = numpy.array([[-1,1],[-1,1],[-1,1]])
rect4D = numpy.array([[-1,1],[-1,1],[-1,1],[-1,1]])
volume = True
order = 3

print "Computed volume 2D: {}".format(I(f,distance2D,rect2D,volume,order))
print "Computed volume 3D: {}".format(I(f,distance3D,rect3D,volume,order))
print "Computed volume 4D: {}".format(I(f,distance4D,rect4D,volume,order))
print "Exact volume 2D: {}".format(m.pi/4.0)
print "Exact volume 3D: {}".format(4.0*m.pi/24.0)
print "Exact volume 4D: {}".format(m.pi**2.0/16.0)


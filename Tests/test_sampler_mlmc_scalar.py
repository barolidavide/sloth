
# @file	test_scalar.cxx
# @brief	This file contains an implementation of multilevel Monte Carlo with a scalar output functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 July 2016

import sys
sys.path.append('../Scripts')
from config import *

print "Running mlmc on 1D Laplacian with scalar QoI"

# Initial number of samples
N0 = numpy.array([640,160,40]);

# MLMC parameters
(alpha,beta,gamma) = (-2,-4,+1)
tol = 1e-2

# Create solver
solver = MLMC(tol)

# Set exponents
solver.setExponents(alpha,beta,gamma)

# Set random variables
sampler = SampleNormal(1.0,1.0e-1)
solver.addDistribution(sampler)

# Initial number of models
functional = Energy()

for l in range(3):
    modelLo = Laplacian1D(functional,l);
    solver.addLoFidelityModel('coarse',modelLo)

# Run solver
P = solver.run(sys.argv,N0)

print "The computed mean is %f" % P
print "Test completed"

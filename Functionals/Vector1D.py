
# @file	Scalar.py
# @brief	This file contains a dummy functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	28 October 2016

import sys
sys.path.append('../Scripts')
from config import *
import scipy.interpolate

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Vector1D:
    
    def __init__(self,x):
        # Locations where to compute the output
        self.x = x
        self.nP = self.x.size
        self.pointwise = True
    
    def getNumberOutputs(self): return self.nP
    
    def eval(self,uh,nx):
        x = self.x
        
        # Create coarse mesh
        xi = numpy.linspace(0,1,nx+1);
        
        # Extend solution to the boundary
        ui = numpy.zeros(nx+1);
        ui[1:nx] = uh;
        
        # Uses either nodal values or integrated over dual cells
        if( self.pointwise ):
            F = numpy.interp(x,xi,ui);
        
        else:
            # Compute integral of P1 function
            Fi = scipy.integrate.cumtrapz(ui,xi,initial=0);
            
            # Create fine meshes
            xl = numpy.linspace(0,1,nP+1)
            xu = numpy.linspace(0,1,nP+1)
            
            xl[1:nP+1] = x
            xu[0:nP+0] = x
            
            # Create dual fine mesh
            xd = numpy.zeros(3*nP);
            
            xd[0:3*nP:3] = x - numpy.diff(xl)/2;
            xd[1:3*nP:3] = x;
            xd[2:3*nP:3] = x + numpy.diff(xu)/2;
            
            # Get solution at fine nodes
            Fd = numpy.interp(xd,xi,Fi);
            
            # Compute integral
            F = numpy.zeros(nP)
            
            # Evaluate integral over each dual cell
            for i in range(0,nP):
                F[i] = Fd[3*i+2]-Fd[3*i]
        
        # Return result
        return F

    def plotSolution(self,u,uMean):
        nP = self.nP
        x = self.x
        
        # Create new vectors
        xp = numpy.linspace(0,1,nP+2)
        up = numpy.zeros(nP+2)
        um = numpy.zeros(nP+2)
        
        xp[1:nP+1] = x
        up[1:nP+1] = -u
        um[1:nP+1] = -uMean
        
        # Plot solution
        plt.plot(xp,up,'-o',xp,um,'--s')
        plt.legend(('f(mean)','mean(f)'))
        plt.savefig('results/vector1d_output.png')
        plt.close()



# @file	Scalar.py
# @brief	This file contains a dummy functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	28 October 2016

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Scalar:
    
    def __init__(self): self.nP = 1
    
    def getNumberOutputs(self): return self.nP
				
    def eval(self,u): return u

    def plotSolution(self): pass


# @file	Squared.py
# @brief	This file computes Sobol indices
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 October 2018

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Sobol:
    
    def __init__(self,d,type='total'):
        self.d = d
        self.nP = len(d)
        self.type = type
    
    def getNumberOutputs(self): return self.nP
    
    # Create the model input and eval the model
    def eval(self,model,k):
        z,w = numpy.split(numpy.asarray(k),2,axis=1)
        N,D = z.shape
        
        S = numpy.zeros((N,self.nP))
        
        # Evaluate model with 2 fixed inputs
        fw = model.evalModel(w)
        fz = model.evalModel(z)
        
        # Evaluate other d inputs
        for di,d in enumerate(self.d):
            y = numpy.copy(w)
            y[:,d] = z[:,d]
            fy = model.evalModel(y)
            S[:,di] = self.evalIndex(fz,fw,fy)[:,0]
        
        return S
    
    # Eval the index given all model results
    def evalIndex(self,fz,fw,fy):
        n = fz.shape[0]
        if self.type=='main':
            Ez = numpy.mean(fz,axis=0)
            Ew = numpy.mean(fw,axis=0)
            Vz = numpy.var(fz,axis=0,ddof=1.0)
            Vw = numpy.var(fw,axis=0,ddof=1.0)
            return (2.*n/(2.*n-1.)) * ( fz*fy - ((Ez+Ew)/2.)**2. + (Vz+Vw)/(4.*n) )
        elif self.type=='total':
            return 0.5*(fw - fy)**2.0
        else:
            print "Sobol index {} not supported!".format(type)
            exit()

    def plotSolution(self): pass

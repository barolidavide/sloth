
# @file	Squared.py
# @brief	This file is useful to compute second moments
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 October 2018

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Mean:
    
    def __init__(self,n):
        self.nP = n
    
    def getNumberOutputs(self): return self.nP
				
    def eval(self,model,z): return model.evalModel(z)

    def plotSolution(self): pass

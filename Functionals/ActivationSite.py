
# @file	Scalar.py
# @brief	This file contains a dummy functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	28 October 2016

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class ActivationSite:
    
    def __init__(self,mask,ei=20,ej=20,ek=40):
        self.nP = 1
        self.ei = ei
        self.ej = ej
        self.ek = ek
        xi = numpy.asarray(numpy.where(mask==False))

        truth = numpy.ones(xi.shape[1]).astype(bool)
        truth = numpy.logical_and(truth,xi[0,:]==self.ei)
        truth = numpy.logical_and(truth,xi[1,:]==self.ej)
        truth = numpy.logical_and(truth,xi[2,:]==self.ek)
    
        self.idx = numpy.where(truth)[0][0]


    def getNumberOutputs(self): return self.nP
				
                
    def eval(self,u):
        try:
            W = u[self.ei,self.ej,self.ek]
        except:
            #uvec = u.reshape(self.N[0],self.N[1],self.N[2])[self.ei,self.ej,self.ek]
            W = u[self.idx]
        return W


    def plotSolution(self): pass


# @file	Squared.py
# @brief	This file is useful to compute second moments
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	21 October 2018

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Variance:
    
    def __init__(self,n):
        self.nP = n
    
    def getNumberOutputs(self): return self.nP
				
    def eval(self,model,z):
        u = model.evalModel(z)
        n = u.shape[0]
        mu = numpy.mean(u,axis=0)
        return ((u-mu)**2.0) * (n/(n-1.))

    def plotSolution(self): pass

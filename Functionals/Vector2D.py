
# @file	Scalar.py
# @brief	This file contains a dummy functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	28 October 2016

import numpy
import scipy.interpolate

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Vector2D:
    
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.nP = self.x.size * self.y.size
    
    def getNumberOutputs(self): return self.nP
				
    def eval(self,xi,yi,uh):
        z = numpy.reshape(uh,(xi.size,yi.size))
        F = scipy.interpolate.interp2d(xi,yi,z);
        return F(self.x,self.y).flatten()

    def plotSolution(self,u,uMean): pass



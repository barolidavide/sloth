
# @file	Energy.py
# @brief	This file contains a dummy functional
# @details TBD
# @author	Alessio Quaglino
# @version	1
# @date	28 October 2016

import numpy

###############################################################################
# This is the class taking care of the output
# It does not need to know how the solution is computed
# Only solution and geometry are required as input
###############################################################################
class Energy:
    
    def __init__(self): self.nP = 1
    
    def getNumberOutputs(self): return 1
    
    def eval(self,u,nx):
    
        h = 1.0/nx
        
        # Get energy value
        E = -sum(u)*h
        
        # Return result
        return E
